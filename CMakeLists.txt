cmake_minimum_required(VERSION 3.1)
project("solerogue")

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED on)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${PROJECT_SOURCE_DIR}/cmake)

# Coverage
# cmake .. -DCMAKE_BUILD_TYPE=Coverage
if (CMAKE_BUILD_TYPE STREQUAL "Coverage")
  SET(CMAKE_CXX_FLAGS "-g -O0 -fprofile-arcs -ftest-coverage")
  SET(CMAKE_C_FLAGS "-g -O0 -fprofile-arcs -ftest-coverage")

  include(CodeCoverage)
  setup_target_for_coverage_gcovr_html(
    NAME ${PROJECT_NAME}_coverage
    EXECUTABLE tests
    BASE_DIRECTORY "${PROJECT_SOURCE_DIR}/src"
    EXCLUDE "${PROJECT_SOURCE_DIR}/ext/" "src/*_Test.cpp" "src/examples/*"
    DEPENDENCIES tests ${PROJECT_NAME}
    )
else()
  set(CMAKE_BUILD_TYPE Debug)
endif() #CMAKE_BUILD_TYPE STREQUAL "Coverage"

set(PROJECT  "solerogue")
set(VERSION 0.0.10)
set(REVISION 30)

configure_file(
  "${PROJECT_SOURCE_DIR}/Doxyfile.in"
  "${PROJECT_BINARY_DIR}/Doxyfile"
  )

configure_file(
  "${PROJECT_SOURCE_DIR}/config.h.in"
  "${PROJECT_BINARY_DIR}/config.h"
  )

INCLUDE(FindGTest)

find_package(ZLIB REQUIRED)
find_package(SFML 2 COMPONENTS system window graphics audio REQUIRED)
find_package(cereal REQUIRED)
find_package(Boost REQUIRED COMPONENTS program_options REQUIRED )

add_library(rltk SHARED
  ext/rltk/rltk/rltk.cpp
  ext/rltk/rltk/texture_resources.cpp
  ext/rltk/rltk/color_t.cpp
  ext/rltk/rltk/virtual_terminal.cpp
  ext/rltk/rltk/rng.cpp
  ext/rltk/rltk/geometry.cpp
  ext/rltk/rltk/input_handler.cpp
  ext/rltk/rltk/font_manager.cpp
  ext/rltk/rltk/gui.cpp
  ext/rltk/rltk/layer_t.cpp
  ext/rltk/rltk/gui_control_t.cpp
  ext/rltk/rltk/virtual_terminal_sparse.cpp
  ext/rltk/rltk/ecs.cpp
  ext/rltk/rltk/xml.cpp
  ext/rltk/rltk/perlin_noise.cpp
  ext/rltk/rltk/rexspeeder.cpp
  ext/rltk/rltk/scaling.cpp)

add_library(fantasyname ext/fantasyname/c++/namegen.cc)

add_subdirectory(ext/md4c)

set_target_properties(fantasyname
    PROPERTIES
        CXX_STANDARD 11
        CXX_STANDARD_REQUIRED YES
        CXX_EXTENSIONS NO
)

target_include_directories(rltk PUBLIC
		"$<BUILD_INTERFACE:${SFML_INCLUDE_DIR}>"
		"$<BUILD_INTERFACE:${CEREAL_INCLUDE_DIR}>"
		"$<BUILD_INTERFACE:${ZLIB_INCLUDE_DIRS}>"
		)
target_link_libraries(rltk PUBLIC ${ZLIB_LIBRARIES} ${SFML_LIBRARIES})
target_compile_options(rltk PUBLIC -O3 -Wall -Wpedantic -march=native -mtune=native -g)

# API library
add_library(solerogue-api SHARED
  src/api/Attributes.cpp
  src/api/Entity.cpp
  src/api/EntityList.cpp
  src/api/File.cpp
  src/api/IdxOutOfBoundsException.cpp
  src/api/Loot.cpp
  src/api/Level.cpp
  src/api/Logger.cpp
  src/api/Map.cpp
  src/api/PlayerId.cpp
  src/api/Rng.cpp
  src/api/VcharBuffer.cpp
  src/api/Weapon.cpp
  )


# Game
set(SOURCES
  src/main.cpp

  src/NavigatorHelper.cpp
  src/Position.cpp
  src/i18n.cpp
  src/Layers.cpp
  src/MapManager.cpp
  src/MdParser.cpp
  src/Morgue.cpp
  src/PlayedTime.cpp
  src/Renderable.cpp
  src/Score.cpp
  src/Script.cpp 
  src/ScriptManager.cpp
  src/HighScoresEntry.cpp

  src/formatters/highscores/Json.cpp
  src/formatters/highscores/Text.cpp
  
  src/exceptions/MapOutOfBounds.cpp

  src/components/Character.cpp
  src/components/Inventory.cpp
  
  src/layers/Layer.cpp 
  src/layers/TitleLayer.cpp

  src/scripts/So.cpp

  src/ui/Character.cpp
  src/ui/Form.cpp
  src/ui/HelpViewer.cpp
  src/ui/Info.cpp
  src/ui/Log.cpp
  src/ui/Highscores.cpp
  
  src/systems/ActorRender.cpp
  src/systems/Camera.cpp
  src/systems/EnemyInfo.cpp
  src/systems/Gui.cpp
  src/systems/HighScores.cpp
  src/systems/Hud.cpp
  src/systems/Inventory.cpp
  src/systems/Metrics.cpp
  src/systems/Menu.cpp
  src/systems/NodePointing.cpp
  src/systems/PickupMenu.cpp
  src/systems/Player.cpp
  src/systems/SoundManager.cpp
  src/systems/Visibility.cpp
  src/systems/Zoom.cpp

  src/messages/ActorMoved.cpp
  src/messages/AddLogEntry.cpp
  src/messages/AttributeModifier.cpp
  src/messages/ChangeNodeInfo.cpp
  src/messages/ChangeObjectsInfo.cpp
  src/messages/ChangeMainFont.cpp
  src/messages/OutlineTile.cpp
  src/messages/PlayerMoved.cpp
  src/messages/PlaySfx.cpp
  src/messages/ToggleVisibility.cpp
  )

add_executable(solerogue ${SOURCES})

target_link_libraries(solerogue rltk fantasyname dl solerogue-api md4c
  ${Boost_LIBRARIES})
include_directories(ext/rltk/rltk)
include_directories(ext/fantasyname/c++)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/src/)
include_directories(${PROJECT_BINARY_DIR}/) # Mainly for config.h
include_directories(${Boost_INCLUDE_DIRS})

# Example
add_executable(example-serialization src/examples/serialization/main.cpp)
target_link_libraries(example-serialization rltk)

add_executable(example-i18n src/examples/i18n/main.cpp src/i18n.cpp
  src/api/Logger.cpp)
target_link_libraries(example-i18n rltk)

add_executable(example-map src/examples/map/main.cpp
  src/MapManager.cpp
  src/i18n.cpp
  src/ScriptManager.cpp
  src/Script.cpp
  src/scripts/So.cpp
  src/exceptions/MapOutOfBounds.cpp
  src/MdParser.cpp
  src/Position.cpp
  src/api/IdxOutOfBoundsException.cpp
  src/ui/HelpViewer.cpp
  src/messages/PlaySfx.cpp
  )
target_link_libraries(example-map solerogue-api rltk dl md4c)

add_executable(example-singleton src/examples/singleton/main.cpp)

set_target_properties(example-singleton
    PROPERTIES
        CXX_STANDARD 14
        CXX_STANDARD_REQUIRED YES
        CXX_EXTENSIONS NO
)

add_executable(example-scriptmanager
  src/examples/scriptmanager/main.cpp
  src/ScriptManager.cpp
  src/Script.cpp
  src/i18n.cpp
  src/ui/HelpViewer.cpp
  src/MdParser.cpp
  
  src/scripts/So.cpp

  src/MapManager.cpp
  src/Position.cpp
  src/exceptions/MapOutOfBounds.cpp

  src/messages/PlaySfx.cpp
  )
target_link_libraries(example-scriptmanager solerogue-api rltk dl md4c)

add_executable(example-actionverb
  src/examples/action-verb/main.cpp)

add_executable(example-autocpy src/examples/autocpy/main.cpp)
set_target_properties(example-autocpy
    PROPERTIES
        CXX_STANDARD 14
        CXX_STANDARD_REQUIRED YES
        CXX_EXTENSIONS NO
)

add_executable(example-md src/examples/md/main.cpp)
include_directories(ext/md4c/src)
target_link_libraries(example-md md4c)


# Mods
add_library(01-so MODULE
  src/mods/01-so/main.cpp)
target_link_libraries(01-so solerogue-api)
set_target_properties(01-so  PROPERTIES
  LIBRARY_OUTPUT_DIRECTORY "mods/01-so/")

add_library(torch MODULE
  src/mods/torch/Torch.cpp
  src/mods/torch/main.cpp)
target_link_libraries(torch solerogue-api)
set_target_properties(torch  PROPERTIES
  LIBRARY_OUTPUT_DIRECTORY "mods/torch/")

add_library(club MODULE
  src/mods/club/main.cpp)
target_link_libraries(club solerogue-api)
set_target_properties(club  PROPERTIES
  LIBRARY_OUTPUT_DIRECTORY "mods/club/")

add_library(standard-food MODULE
  src/mods/standard-food/main.cpp)
target_link_libraries(standard-food solerogue-api)
set_target_properties(standard-food  PROPERTIES
  LIBRARY_OUTPUT_DIRECTORY "mods/standard-food/")

add_library(cactus MODULE
  src/mods/cactus/main.cpp)
target_link_libraries(cactus solerogue-api)
set_target_properties(cactus  PROPERTIES
  LIBRARY_OUTPUT_DIRECTORY "mods/cactus/")

add_library(cat MODULE src/mods/cat/main.cpp)
target_link_libraries(cat solerogue-api)
set_target_properties(cat  PROPERTIES
  LIBRARY_OUTPUT_DIRECTORY "mods/cat/")

add_library(dog MODULE  src/mods/dog/main.cpp)
target_link_libraries(dog solerogue-api)
set_target_properties(dog  PROPERTIES  LIBRARY_OUTPUT_DIRECTORY
  "mods/dog/")

add_library(saturate MODULE  src/mods/saturate/main.cpp)
target_link_libraries(saturate solerogue-api)
set_target_properties(saturate  PROPERTIES  LIBRARY_OUTPUT_DIRECTORY
  "mods/saturate/")

add_library(ambient MODULE  src/mods/ambient/main.cpp)
target_link_libraries(ambient solerogue-api)
set_target_properties(ambient  PROPERTIES  LIBRARY_OUTPUT_DIRECTORY
  "mods/ambient/")

add_library(multiwalls MODULE  src/mods/multiwalls/main.cpp)
target_link_libraries(multiwalls solerogue-api)
set_target_properties(multiwalls  PROPERTIES
  LIBRARY_OUTPUT_DIRECTORY "mods/multiwalls/")

add_library(mushroom MODULE  src/mods/mushroom/main.cpp)
target_link_libraries(mushroom solerogue-api)
set_target_properties(mushroom  PROPERTIES  LIBRARY_OUTPUT_DIRECTORY
  "mods/mushroom/")

# Unit tests
enable_testing()
# All files with the given pattern will be use as unit tests.
# No main function/file needed.
file(GLOB tests-src src/*.cpp src/*/*.cpp src/formatters/*/*.cpp)
list(APPEND tests-src src/tests.cpp)

# Remove main.cpp file
get_filename_component(full_path_main_cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/src/main.cpp ABSOLUTE)
list(REMOVE_ITEM tests-src ${full_path_main_cpp})

# Add mods' tested classes
list(APPEND tests-src src/mods/torch/Torch.cpp)
list(APPEND tests-src src/mods/torch/Torch_Test.cpp)

add_executable(tests ${tests-src})
target_link_libraries(tests
  rltk md4c
  fantasyname
  ${GTEST_LIBRARIES}
  pthread dl gcov
  )
add_custom_target(check COMMAND tests) # Add the `make check` command


# Translation
find_package(Gettext)
add_custom_target(xgtext)
ADD_CUSTOM_COMMAND(
  TARGET xgtext POST_BUILD
  COMMAND xgettext --keyword=_ --language=C --add-comments --sort-output -o po/en.pot ${SOURCES}
  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
  )

# This is a fix for the GNU C __FILE__ thing :
#  __FILE__ shows the full path of the file, so the logger text output
#  is weird.
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -D__FILENAME__='\"$(subst \
  ${CMAKE_SOURCE_DIR}/,,$(abspath $<))\"'")
