# solerogue

A C++ rogue project based on rltk library.

[[_TOC_]]

# Screenshot

![Screenshot](./media/screenshot/screen01.png "A partially explored map")

# Dependencies

## Debian

	sudo apt install zlib1g-dev libsfml-dev libcereal-dev \
	  libboost-program-options-dev gettext imagemagick

## Manjaro

	sudo pacman -S cereal

# Building

	git submodule init
	git submodule update

	mkdir build
	cd build/
	cmake ..
	make

Before running, you need to generate some assets :

	cd media/assets/
	./resize.sh

# Key bindings

| Key      |     Action                    |
|----------|-------------------------------|
| Arrows   | Move player                   |
| Enter    | Use a stair/hatch             |
| Space    | Pick up object                |
| F1       | Show/Hide system performances |
| F6       | Show/Hide highscores          |
| I        | Show/Hide player's inventory  |
| C        | Show/Hide character's form    |
| A/Q      | Up/Down in dialog menu        |
|          | Change current hand           |
| W/X      | Left/Right in action menu     |
| -/+      | Zoom in/out                   |

# Translation/i18n

## Game translation
To extract translatable messages :

	make xgtext
	cd ../po/
	poedit fr.po
	make

To handle a new language (for example fr):

	mkdir po/fr/
	msginit --input=po/solerogue.pot --locale=fr --output=po/fr.po

Edit the po/fr.po file then compile it :

	msgfmt --output-file=mo/fr/solerogue.mo fr.po

## Mod translation

Mod strings translation is based on *.ini* files found in mod root directory.
To provide translation in french for example, the mod must a `.ini` file per
language in this format :

	[glyphs]
	chicken=c
	
	[messages]
	Chicken = Poulet

Glyphs are translated using a unique string-based identifier also found in this
`.ini` file, so the mod must provide a `en.ini` file containing only the
glyphs definitions.

# Mod names and loading

`cmake` forbids usage of the same name for multiple module targets.
So the shared library must have the same name as its parent dirctory :

	mods/
	├── 01-so
	│   ├── lib01-so.so
	│   └── main.cpp
	└── standard-food
		├── libstandard-food.so
		└── main.cpp

To achieve that, here is a `cmake` code example :

	add_library(standard-food MODULE
	    src/mods/standard-food/main.cpp)
	target_link_libraries(standard-food solerogue-api)
	set_target_properties(standard-food  PROPERTIES
		LIBRARY_OUTPUT_DIRECTORY "mods/standard-food/")

# Unit tests coverage

To be able to report coverage :

	mkdir build-coverage/
	cd build-coverage/
	cmake .. -DCMAKE_BUILD_TYPE=Coverage	
	make -j4
	make solerogue_coverage -j4

If you got a *Truncated object file* related error, remove the object file 
and -rebuild or, better, simply issue a `make clean` and rebuild.

Then open *solerogue_coverage/index.html* with your favorite browser.

For the release **ChangeLog number**, we take *Lines Coverage %*.

# Help and Help viewer

This game features a in-game help viewer supporting a little subset of
the *markdown* format :

	em
	strong
	hr
	headers

These files are translatable and can basically found in `/help/$LANG/` 
directory where *$LANG* is a 2 characters language code.

# std::string vs std::wstring

We can use standard strings everywhere but when printing to string using
*sfml*. To help with conversion, you can use `get_wstring()` function from
*src/i18n.cpp* to easy conversion from string.

Everywhere else, especially in log messages, you can simply use string 
directly from gettext, even with multibyte characters and accidentals.

# Music

You can you own music files with rebuilding project in `media/music` subdirs.
We only search for '.ogg' files and don't support MP3 format.
