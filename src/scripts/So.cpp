#include "So.hpp"

#include "api/Logger.hpp"
#include "api/VcharBuffer.hpp"

/** Log error message once for all */
void
error_impl(const char* msg)
{
  LOG(LogLevel::Error, "So script loading error : %s", msg);
}

void
error_handle()
{
#ifndef _WIN32
  char *error;
 if ((error = dlerror()) != NULL)
    error_impl(error);
#endif
}

SoScript::SoScript(const string& vPath):
  Script(vPath),
  on_mov(nullptr),
  on_frame(nullptr)
{
  handle = DLOPEN(vPath.c_str());
  if (!handle)
    {
#ifdef _WIN32
      LOG(LogLevel::Error, "So dlopen error opening: %s", vPath.c_str());
#else
      error_impl(dlerror());
#endif
    }
  on_init = (on_initptr)DLSYM(handle, "on_init");
  error_handle();

  on_gen = (on_genptr)DLSYM(handle, "on_generate");
  error_handle();
  
  on_mov = (on_movptr)DLSYM(handle, "on_move");
  error_handle();

  on_frame = (on_frameptr)DLSYM(handle, "on_frame");
  error_handle();

  on_gettile = (on_gettileptr)DLSYM(handle, "on_getTile");
  error_handle();
}

SoScript::~SoScript()
{
  DLCLOSE(handle);

}

void
SoScript::call_OnInit(void)
{
  if (on_init)
    (*on_init)();
}

/** Call on_generate function on all registered scripts 
  *
  */
void
SoScript::call_OnGenerate(Map* m)
{
  if (on_gen)
    (*on_gen)(m);
}

void
SoScript::call_OnMove(Map* m, rltk::entity_t* pl)
{
  if (on_mov)
    (*on_mov)(m, pl);
}

void
SoScript::call_OnFrame(double duration, VcharBuffer* vb)
{
  if (on_frame)
    (*on_frame)(duration, vb);
  
}

int
SoScript::call_OnGetTile(int mapLevel, int x, int y, bool isWall)
{
  if (on_gettile)
    return (*on_gettile)(mapLevel, x, y, isWall);
  else
    return -1;
  
}
