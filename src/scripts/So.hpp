#ifndef __SO_SCRIPT_HPP__
#define __SO_SCRIPT_HPP__

#include "rltk.hpp"   // USES entity_t*
#include "Script.hpp"

// Forward declarations
class Map;
class VcharBuffer;
// End of forward declarations

#ifdef _WIN32
#  include <windows.h> 
   typedef HINSTANCE LibHandleType;
#  define DLOPEN(FILENAME)   LoadLibrary(TEXT(FILENAME))
#  define DLSYM(HANDLE, SYM) GetProcAddress(HANDLE, SYM)
#  define DLCLOSE(HANDLE)   FreeLibrary(HANDLE)
#else
#  include <dlfcn.h>
   typedef void* LibHandleType;
#  define DLOPEN(FILENAME)   dlopen(FILENAME, RTLD_LAZY)
#  define DLSYM(HANDLE, SYM) dlsym(HANDLE, SYM)
#  define DLCLOSE(HANDLE)   dlclose(HANDLE)
#endif

// Can't use std::function because of dlsym
typedef void (*on_initptr)(void);
typedef void (*on_genptr)(Map*);
typedef void (*on_movptr)(Map*, rltk::entity_t*);
typedef void (*on_frameptr)(double, VcharBuffer*);
typedef int (*on_gettileptr)(int, int, int, bool);


class SoScript : public Script
{
public:
  SoScript(const string&);
  ~SoScript();

  virtual void call_OnInit(void) override;
  virtual void call_OnGenerate(Map*) override;
  virtual void call_OnMove(Map*, rltk::entity_t*) override;
  virtual void call_OnFrame(double, VcharBuffer*) override;
  virtual int  call_OnGetTile(int, int, int, bool);

private:
  LibHandleType handle;    // Handle to the library object

  on_initptr    on_init; // Function pointer to the on_init function;
  on_genptr     on_gen;
  on_movptr     on_mov;  // Makes the Entities move
  on_gettileptr on_gettile;
  
  on_frameptr on_frame;
};

#endif // !__SO_SCRIPT_HPP__
