#ifndef __MD_PARSER_HPP__
#define __MD_PARSER_HPP__

#include <string>

#include "md4c.h"

extern int layerx;
extern int layery;

/** A markdown parser based on md4c */
class MdParser
{
public:
  MdParser();

  int parse(const std::string&);

private:
  MD_RENDERER rn; // Can't be a member (segfault)
};

#endif // ! __MD_PARSER_HPP__
