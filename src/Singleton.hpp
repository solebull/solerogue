#ifndef __SINGLETON_HPP__
#define __SINGLETON_HPP__

/** \file Singleton.hpp
  * The Singleton header file 
  *
  */

/** A basic singleton class
  *
  * Note that the subclass **must** implement a default contructor with token
  * parameter. It is needed to avoid the use of `friend class` statement.
  *
  * \code
  * class Aze final : public Singleton<Aze>
  * {
  * public:
  *   Aze(token){};
  *  ~Aze(){};
  * };
  *
  *
  * auto& Aze::getInstance;
  * \endcode
  *
  */
template<typename T>
class Singleton {
public:
    static T& getInstance();

    Singleton(const Singleton&) = delete;
    Singleton& operator= (const Singleton) = delete;

protected:
    struct token {};
    Singleton() {}
};

/** Get the only instance of this class
  *
  */
template<typename T>
T& Singleton<T>::getInstance()
{
  static T instance{token{}};
  return instance;
}

#endif // !__SINGLETON_HPP__

