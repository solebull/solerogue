#ifndef _LAYER_HPP_
#define _LAYER_HPP_

/** Basically a rltk System without message/event management 
  * Used by every layers
  *
  */
class Layer
{
public:
  Layer(int);
  virtual ~Layer();
  
  virtual void update(double duration_ms) = 0;
protected:
  /** The internal RLTK identifier used to create the layer */
  int layerId;
};


#endif // !_LAYER_HPP_
