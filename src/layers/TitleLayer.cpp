#include "TitleLayer.hpp"

#include "rltk.hpp"

using namespace rltk;
using namespace rltk::colors;

// This is called when the screen resizes, to allow the GUI to redefine itself.
void resize_title(layer_t * l, int w, int h) {
  // Simply set the width to the whole window width
  l->w = w;
  l->h = 16; // Not really necessary - here for clarity
}

TitleLayer::TitleLayer(int layerId, const string& vTitle):
  Layer(layerId),
  title(vTitle)
{
  gui->add_layer(layerId, 0, 0, 1024, 32, "8x16", resize_title);
  
}

TitleLayer::~TitleLayer()
{

}
  
void
TitleLayer::update(double duration_ms)
{
  term(layerId)->clear(vchar{' ', YELLOW, BLUE});
  term(layerId)->print_center(0, "SoleRogue", YELLOW, BLUE);


}
