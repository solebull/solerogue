#ifndef _TITLE_LAYER_HPP_
#define _TITLE_LAYER_HPP_

#include <string>

#include "Layer.hpp"

using namespace std;

class TitleLayer : public Layer
{
public:
  TitleLayer(int, const string&);
  virtual ~TitleLayer();
  
  virtual void update(double duration_ms) override;

private:
  string title;
};

#endif // !_TITLE_HPP_
