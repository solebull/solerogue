#include <Score.hpp>
#include <gtest/gtest.h>

TEST( Score, bonus_get )
{
  Score s;
  ASSERT_EQ( s.getBonuses().size(),  0);
}

TEST( Score, bonus_add )
{
  Score s;
  s.addBonus("Test", 10);
  ASSERT_EQ( s.getBonuses().size(),  1);

  auto b = s.getBonuses();
  ASSERT_EQ(b[0].first,  "Test");
  ASSERT_EQ(b[0].second, 10);
}

TEST( Score, bonus_compute )
{
  Score s;
  ASSERT_EQ( s.compute(),  0);

  s.addBonus("Test", 10);
  ASSERT_EQ( s.compute(), 10);
}

TEST( Score, get_kill_0 )
{
  Score s;
  int i = s.getKill("rat");
  ASSERT_EQ( i,  0);
}

TEST( Score, add_kills_0 )
{
  Score s;
  s.addKill("rat");
  int i = s.getKill("rat");
  ASSERT_EQ( i,  1);
}

// Test that kills are sorted by kills number
TEST( Score, kills_sorted )
{
  Score s;

  s.addKill("dog");

  s.addKill("rat");
  s.addKill("rat");
  s.addKill("rat");

  // As map is sorted on key, in a standard map, dog should be first

  auto k = s.getKills();
  ASSERT_EQ( k[0].first,  "rat");
  ASSERT_EQ( k[1].first,  "dog");
}

// Test that kills are sorted by kills number
TEST( Score, kills_add_empty_species )
{
  Score s;
  s.addKill("");
  ASSERT_EQ( s.getKills().size(),  1);
}
