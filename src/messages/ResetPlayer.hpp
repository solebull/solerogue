#ifndef __RESET_PLAYER_HPP__
#define __RESET_PLAYER_HPP__

#include "rltk.hpp"

/** A message used to reset all player's atributes between two games.
  *
  */
class ResetPlayerMessage : rltk::base_message_t
{

};


#endif // !__RESET_PLAYER_HPP__
