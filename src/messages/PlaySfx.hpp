#ifndef _PLAY_SFX_MESSAGE_HPP_
#define _PLAY_SFX_MESSAGE_HPP_

#include "rltk.hpp"

#include <string>

using namespace rltk;

class PlaySfxMessage : base_message_t
{
public:
  PlaySfxMessage();
  PlaySfxMessage(const std::string&);
  PlaySfxMessage(const std::string&, float, float);

  std::string filename;
  float pitch;
  float volume;
};

#endif // !_PLAY_SFX_MESSAGE_HPP_
