#include "AddLogEntry.hpp"

#include "systems/Player.hpp" // USES nb_moves
#include "PlayedTime.hpp"

AddLogEntryMessage::AddLogEntryMessage():
  nbMoves(nb_moves),
  time(playedTime.str())
{

}

AddLogEntryMessage::AddLogEntryMessage(const std::string& m):
  AddLogEntryMessage()
{
  msg = m;
}

