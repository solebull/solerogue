#include "ChangeMainFont.hpp"

using namespace std;

ChangeMainFontMessage::ChangeMainFontMessage():
  fontWidth(0),
  fontHeight(0)
{
  
}

/** Use to change the font of the Main layer */
ChangeMainFontMessage::ChangeMainFontMessage(const string& nf):
  nextFont(nf)
{
  std::size_t found = nf.find("x");
  if (found == std::string::npos)
    throw std::runtime_error("Can't get main font size");
  
  fontWidth  = stoi(nf);
  fontHeight = stoi(nf.substr(found+1));
}

const string&
ChangeMainFontMessage::getNextFont(void) const
{
  return nextFont;
}

int
ChangeMainFontMessage::getFontWidth(void) const
{
  return fontWidth;
}

int
ChangeMainFontMessage::getFontHeight(void) const
{
  return fontHeight;
}
