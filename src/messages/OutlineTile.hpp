#ifndef __OUTLINE_TOOL_HPP__
#define __OUTLINE_TOOL_HPP__

#include "rltk.hpp"

class OutlineTileMessage: rltk::base_message_t
{
public:
  OutlineTileMessage();
  OutlineTileMessage(bool, int, int);

  bool show;  //!< Should we show the outline
  int x, y;   //!< Position in tiles starting a 0,0 at top left corner
};
#endif // !__OUTLINE_TOOL_HPP__
    
