#include "OutlineTile.hpp"

OutlineTileMessage::OutlineTileMessage():
 show(false),
 x(0),
 y(0)  
{

}

OutlineTileMessage::OutlineTileMessage(bool vShow, int vX, int vY):
  show(vShow),
  x(vX),
  y(vY)  
{
  
}
