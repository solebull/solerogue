#include "AttributeModifier.hpp"


AttributeModifierMessage::AttributeModifierMessage()
{

}

AttributeModifierMessage::AttributeModifierMessage
(const string& attrb,int value, size_t eId):
  attributeName(attrb),
  value(value),
  entityId(eId)
{

}

const string&
AttributeModifierMessage::getAttributeName() const
{
  return attributeName;
}

int
AttributeModifierMessage::getValue() const
{
  return value;
}

size_t
AttributeModifierMessage::getEntityId() const
{
  return entityId;
}
  
