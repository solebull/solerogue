#ifndef __ZOOM_MESSAGES_HPP__
#define __ZOOM_MESSAGES_HPP__

#include "rltk.hpp"

class ZoomInMessage : rltk::base_message_t{ };
class ZoomOutMessage : rltk::base_message_t{ };

#endif // ! __ZOOM_MESSAGES_HPP__
