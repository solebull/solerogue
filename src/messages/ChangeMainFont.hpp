#ifndef __CHANGE_MAIN_FONT_MESSAGE_HPP__
#define __CHANGE_MAIN_FONT_MESSAGE_HPP__

#include "rltk.hpp"

#include <string>

/** Use to change the font of the Main layer */
class ChangeMainFontMessage : rltk::base_message_t
{
public:
  ChangeMainFontMessage();
  ChangeMainFontMessage(const std::string&);

  const std::string& getNextFont(void) const;
  
  int getFontWidth(void) const;
  int getFontHeight(void) const;
  
private:
  std::string nextFont;
  
  int fontWidth;
  int fontHeight;
};

#endif // !__CHANGE_MAIN_FONT_MESSAGE_HPP__
