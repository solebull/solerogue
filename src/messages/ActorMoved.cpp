
#include "ActorMoved.hpp"

ActorMovedMessage::ActorMovedMessage()
{
}

ActorMovedMessage::ActorMovedMessage(entity_t * ACTOR,
				     const int fx, const int fy,
				     const int dx, const int dy,
				     ActorMovedDirection vDirection):
  mover(ACTOR), from_x(fx), from_y(fy), destination_x(dx), destination_y(dy),
  direction(vDirection)
{
  
}


ActorMovedDirection
ActorMovedMessage::getDirection(void) const
{
  return direction;
}
