#ifndef _ATTRIBURE_MODIFIER_MESSAGE_HPP_
#define _ATTRIBURE_MODIFIER_MESSAGE_HPP_

#include <string>
#include <cstddef> // USES size_t

#include "rltk.hpp"

#include "api/PlayerId.hpp" // USES player_id


using namespace rltk;
using namespace std;

/** Modify a named attribute for a Id'ed entity 
  *
  */
class AttributeModifierMessage : base_message_t
{
public:
  AttributeModifierMessage();
  AttributeModifierMessage(const string&, int, size_t eId=player_id);

  const string& getAttributeName(void) const;
  int getValue(void) const;
  size_t getEntityId(void) const;
  
private:
  string attributeName;
  int value; // Negative for a loose of i.e health
  size_t entityId;  // player_id if for the player
};

#endif // !_ATTRIBURE_MODIFIER_MESSAGE_HPP_
