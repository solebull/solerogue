#ifndef _CHANGE_NODE_INFO_HPP_
#define _CHANGE_NODE_INFO_HPP_

#include <string>

#include "rltk.hpp"

using namespace rltk;
using namespace std;

/** Change the Info layer message for a given Node (a map case) */
class ChangeNodeInfoMessage : base_message_t
{
public:
  ChangeNodeInfoMessage();
  ChangeNodeInfoMessage(const string&);

  string msg;
};

#endif // !_CHANGE_NODE_INFO_HPP_
