#ifndef _TOGGLE_VISIBILITY_MESSAGE_HPP_
#define _TOGGLE_VISIBILITY_MESSAGE_HPP_

#include <string>

#include "rltk.hpp"

using namespace rltk;
using namespace std;

/** Toggle the visibility of an interface element (HighScore)
  *
  */
class ToggleVisibilityMessage : base_message_t
{
public:
  ToggleVisibilityMessage();
  ToggleVisibilityMessage(const string&);

  const string& getElementName(void);
  
private:
  string elementName; // UI element to hide or show
};

#endif // !_TOGGLE_VISIBILITY_MESSAGE_HPP_
