
#include "ToggleVisibility.hpp"

using namespace rltk;
using namespace std;

ToggleVisibilityMessage::ToggleVisibilityMessage()
{
  
}

ToggleVisibilityMessage::ToggleVisibilityMessage(const string& aName):
  elementName(aName)
{
  
}

const string&
ToggleVisibilityMessage::getElementName()
{
  return elementName;
}
