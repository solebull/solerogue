#ifndef _CHANGE_OBJECTS_INFO_HPP_
#define _CHANGE_OBJECTS_INFO_HPP_

#include "rltk.hpp"

using namespace rltk;
using namespace std;

/** Change the position pointed by the mouse and the IngoLayer objects info */
class ChangeObjectsInfoMessage : base_message_t
{
public:
  ChangeObjectsInfoMessage();
  ChangeObjectsInfoMessage(int vX, int vY);

  int x;
  int y;
};

#endif // !_CHANGE_OBJECTS_INFO_HPP_
