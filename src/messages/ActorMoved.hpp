#ifndef _ACTOR_MOVED_MESSAGE_HPP_
#define _ACTOR_MOVED_MESSAGE_HPP_

#include "rltk.hpp"

using namespace rltk;

/** If actor moves and use a stair/hatch at the same time
  *
  *
  */
typedef enum
  {
   SAME = 0,
   DOWN = 1,
   UP = 2
  } ActorMovedDirection;

class ActorMovedMessage : base_message_t
{
public:
  ActorMovedMessage();
  ActorMovedMessage(entity_t*, const int, const int, const int, const int,
		    ActorMovedDirection vDirection=SAME);

  ActorMovedDirection getDirection(void) const;
  
  entity_t * mover;
  int from_x, from_y, destination_x, destination_y;
private:
  ActorMovedDirection direction;
};

#endif // !_ACTOR_MOVED_MESSAGE_HPP_
