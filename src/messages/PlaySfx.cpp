#include "PlaySfx.hpp"

PlaySfxMessage::PlaySfxMessage():
  base_message_t(),
  filename(""),
  pitch(1.0f),
  volume(100.0f)
{

}

PlaySfxMessage::PlaySfxMessage(const std::string& f):
  base_message_t(),
  filename(f),
  pitch(1.0f),
  volume(100.0f)
{

}
  
PlaySfxMessage::PlaySfxMessage(const std::string& f, float p, float v):
  base_message_t(),
  filename(f),
  pitch(p),
  volume(v)
{

}
  
