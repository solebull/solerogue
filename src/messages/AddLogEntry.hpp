#ifndef _ADD_LOG_ENTRY_HPP_
#define _ADD_LOG_ENTRY_HPP_

#include <string>

#include "rltk.hpp"

using namespace rltk;
using namespace std;

class AddLogEntryMessage : base_message_t
{
public:
  AddLogEntryMessage();
  AddLogEntryMessage(const std::string&);

  int nbMoves;  //!< The moves number the event occured at
  string time;  //!< The time of the event as 00:00:00 string
  string msg;   //!< The message
};
  

#endif // !_ADD_LOG_ENTRY_HPP_
