#include "ChangeMainFont.hpp"
#include <gtest/gtest.h>

TEST( Zoom, cm8x16 )
{
  ChangeMainFontMessage cm("8x16");
  ASSERT_EQ( cm.getFontWidth(),  8);
  ASSERT_EQ( cm.getFontHeight(), 16);
}

TEST( Zoom, cm32x8 )
{
  ChangeMainFontMessage cm("32x16");
  ASSERT_EQ( cm.getFontWidth(),  32);
  ASSERT_EQ( cm.getFontHeight(), 16);
}
