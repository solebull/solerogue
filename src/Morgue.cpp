#include "Morgue.hpp"

#include "rltk.hpp"

#include "api/PlayerId.hpp" // USES player_id
#include "api/Entity.hpp"
#include "api/Rng.hpp"

#include "systems/Player.hpp" // USES playerName

#include "MapManager.hpp"
#include "Score.hpp"

#include "components/Inventory.hpp"

#include <iostream>
#include <fstream>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#ifdef _WIN32
#  include <dirent.h>  // USES CreateDirectory
#  include <windows.h> // USES winbase.h
#endif


#include "api/Logger.hpp"

using namespace rltk;
using namespace std;


// Morgue isn't translated, only in english

Morgue::Morgue()
{
  LOG(LogLevel::Info, "Creating morgue");

  checkDirectory("morgue");

  ofstream os(getFilename("morgue"));
  os << "<body>";
  addHeader(os);
  os << "<h1>Morgue of " << playerName <<"</h1>"
     << "<div class='title'>" << playerTitle << "</div>";
  
  os << "Killed by " << entity(player_id)->component<Score>()->getKilledBy();
  
  int dId = MapManager::getInstance().getDungeonLevel();
  os << "<h2>Status of " << playerName <<"</h2>"
     << "<table>"
     << getTr("Moves number", to_string(nb_moves))
     << getTr("Dungeon level", to_string(dId))
     << "</table>";

  // Bonuses
  auto bonuses = entity(player_id)->component<Score>()->getBonuses();
  os << "<h2>Bonuses</h2>";
  if (bonuses.empty())
    os << "No bonus added" << endl;
  else
    {
      os  << "<table>";
      for (auto b : bonuses)
	  os << getTr(b.first, to_string(b.second));

      os << "</table>";
    }

  // Inventory
  auto inv = entity(player_id)->component<Inventory>()->getEntities();
  os << "<h2>Inventory</h2>";
  if (inv.empty())
    os << "No object in inventory" << endl;
  else
    {
      os  << "<table>";
      for (auto b : inv)
	  os << getTr(b->getName(), b->getDescription());

      os << "</table>";
    }

  // Kills
  auto kills = entity(player_id)->component<Score>()->getKills();
  os << "<h2>Kills</h2>";
  if (kills.empty())
    os << "No kills this time" << endl;
  else
    {
      os  << "<table>";
      for (auto b : kills)
	os << getTr(b.first, to_string(b.second));

      os << "</table>";
    }

  
  // Maps
  os << "<h2>Maps</h2>";
  int mapsid = 1;
  auto maps = MapManager::getInstance().getMaps();
  auto deathpos = entity(player_id)->component<Position>();
  auto deathmap = MapManager::getInstance().getDungeonLevel();

  for (auto m : maps)
    {
      string mapid="map-" + to_string(mapsid);
      os << "<h3>Level&nbsp;" << mapsid << "&nbsp;("
	 << m.getWidth() << "x" << m.getHeight()
	 << ")</h2>";

      os << "<div class='buttons'>";
      os << "<button onclick='zoomin(\"" << mapid << "\")'>Zoom in</button>";
      os << "<button onclick='zoomout(\"" << mapid << "\")'>Zoom out</button>";
      os << "</div>";
      os << "<pre id='" << mapid << "'>";
      for (int y=0; y < (int)m.getHeight(); ++y)
	{
	  for (int x=0; x < (int)m.getWidth(); ++x)
	  {
	    if (deathmap == mapsid &&
		deathpos->getX() == x && deathpos->getY() == y)
	      {
		os << "X ";
	      }
	    else if (m.getRevealed(x, y))
	      {
		const Node& n = m.getTile(x, y);
		os << (n == 1 ? "##" : "  ");
	      }
	    else
	      os << getDebris();
	  }
	os << endl;
	}
      
      os << "</pre>";
      ++mapsid;
    }

  os << "<script type='text/javascript' src='morgue.js'></script>";
  
  os << "</body>";
  os.close();
  // auto pl = entity(player_id);
}

// Check if exists and create it if needed
void
Morgue::checkDirectory(const string& dirname)
{
  const char* pathname = dirname.c_str();

  struct stat info;
  if( stat( pathname, &info ) != 0 )
    {
#ifdef _WIN32
      CreateDirectory( pathname, NULL );
#else      
      mkdir(pathname, 0700);
#endif      
      createSymlink("morgue/morgue.css", "../../src/morgue/morgue.css");
      createSymlink("morgue/morgue.js", "../../src/morgue/morgue.js");
    }
  else if( info.st_mode & S_IFDIR )
    LOG(LogLevel::Info, "%s is a directory\n", pathname );
  else
    LOG(LogLevel::Info, "%s is not a directory\n", pathname );
}

string
Morgue::getFilename(const string& subdir)
{
  char buf[80];
  auto epoch = std::time(nullptr); // NOW
  struct tm ts = *localtime(&epoch);
  strftime(buf, sizeof(buf), "%Y%m%d-%H%M%S", &ts);

  string s( subdir + "/morgue-" + playerName + "-" + buf +  ".html");
  return s;
}

void
Morgue::addHeader(ofstream& ofs)
{
  ofs << "<head>"
      << "<link rel='stylesheet' href='morgue.css'>"
      << "<title>Morgue of " <<playerName << "</title>";
  ofs << "</head>";
}


/** Note: on windows, it diesn't cerate a symlink, it copies the file.
  *
  */
void
Morgue::createSymlink(const string& newfile, const string& oldfile)
{
#ifndef _WIN32
  int ret = symlink(oldfile.c_str(), newfile.c_str());
  if (ret != 0)
    LOG(LogLevel::Error, "Error creating symlink to '%s' in '%s'",
	oldfile.c_str(), newfile.c_str());
#else
  auto ret=CopyFile(oldfile.c_str(), newfile.c_str(), FALSE );
  if (!ret) // boolean: false if failed
    LOG(LogLevel::Error, "Error creating symlink to '%s' in '%s'",
	oldfile.c_str(), newfile.c_str());
#endif
  
}

std::string
Morgue::getTr(const string& a, const string& b)
{
  return "<tr><th>" + a + "</th><td>" + b + "</td></tr>";

}

std::string
Morgue::getDebris(void)
{
  int i = rng::rdice(1, 10);
  switch (i)
    {
    case 1:
      return ". ";
      break;
    case 2:
      return " .";
      break;
    case 3:
      return "..";
      break;
    case 4:
      return "-.";
      break;
    default:
      return "  ";
    }
}

