#ifndef __GAME_MANAGER_HPP__
#define __GAME_MANAGER_HPP__

#include "Singleton.hpp"

/** Handle the Game lifetime and data generation needed for a new game
  *
  */
class GameManager final : public Singleton<GameManager>
{
public:
  GameManager(token);

  void newGame(void);
};

#endif // !__GAME_MANAGER_HPP__
