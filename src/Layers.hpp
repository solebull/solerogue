#ifndef _LAYERS_HPP_
#define _LAYERS_HPP_

// Layers ids
enum
  {
   TITLE_LAYER = 0,
   MAIN_LAYER,
   INFO_LAYER,
   LOG_LAYER,
   HUD_LAYER,
   HS_LAYER,   // HIGH SCORES
   MT_LAYER,   // METRICS
   PUM_LAYER,  // Pickup Menu
   INV_LAYER,  // Inventory layer
   ENI_LAYER,   // ENemy Info
  };

// Forward declarations
namespace rltk
{
  struct layer_t;
}
// End of forward declarations

// This is called when the screen resizes, to allow the GUI to redefine itself.
void resize_main(rltk::layer_t * l, int w, int h);
void resize_info(rltk::layer_t * l, int w, int h);


#endif // !_LAYERS_HPP_
