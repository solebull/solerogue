#ifndef __PICK_UP_MENU_HPP__
#define __PICK_UP_MENU_HPP__

#include "rltk.hpp"

#include <list>
#include <utility>

#include "systems/Menu.hpp"

// Forward declaration
class Entity;
// End of Forward declaration


class PickupMenuSystem : public MenuSystem
{
public:
  PickupMenuSystem();
  virtual ~PickupMenuSystem();

  virtual void configure() override;
  virtual void update(double duration_ms) override;

  virtual void cursorChanged(void) override;
  virtual void action(void) override;

protected:
  void createLayer(void);
  void setVisible(bool);
  
private:
  int layerId;
  bool visible;
};

#endif // !__PICK_UP_MENU_HPP__
