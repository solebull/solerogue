#ifndef __INVENTORY_SYSTEM_HPP__
#define __INVENTORY_SYSTEM_HPP__

#include "rltk.hpp"

#include "systems/Menu.hpp"

using namespace rltk;

// Forward declarations
class Entity;
// End of forward declarations

class InventorySystem : public MenuSystem
{
public:
  InventorySystem();
  ~InventorySystem();

  virtual void configure() override;
  virtual void update(double duration_ms) override;

  virtual void cursorChanged(void) override;
  virtual void action(void) override;
  
  void createLayer(void);
protected:
  void printActionMenu(void);
  void actionCursorChanged(void);
  std::string handleUnequip(int, const std::string&);
    
private:
  bool visible;
  int layerId;

  int actionCursor;
  Entity* currentObject;
};

#endif // !__INVENTORY_SYSTEM_HPP__
