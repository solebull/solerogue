#include "Inventory.hpp"

#include "Layers.hpp"
#include "messages/HudUpdate.hpp"
#include "messages/ToggleVisibility.hpp"
#include "messages/CursorLeft.hpp"
#include "messages/CursorRight.hpp"

#include "api/Entity.hpp"
#include "api/PlayerId.hpp" // USES player_id
#include "api/Weapon.hpp"

#include "config.h"    // USES _()
#include "i18n.hpp"

#include "components/Inventory.hpp"
#include "components/Character.hpp"

using namespace rltk::colors;

const color_t BG =  LightSteelBlue;
const int width = 800;
const int height = 600;


void
resize_inventory(layer_t * l, int w, int h) {
  // Simply set the width to the whole window width
  l->w = width;
  l->h = height;

  l->x = (w - l->w)/2;
  l->y = (h - l->h)/2;
}


InventorySystem::InventorySystem():
  MenuSystem(),
  visible(false),
  layerId(INV_LAYER),
  actionCursor(0),
  currentObject(nullptr)
{

}

InventorySystem::~InventorySystem()
{

}


void
InventorySystem::configure()
{
  MenuSystem::configure();
  
  system_name = "Inventory";

  if (visible)
    createLayer();

  subscribe<ToggleVisibilityMessage>
    ([this](ToggleVisibilityMessage &msg){
       
       if (msg.getElementName() == "Inventory")
	 {
	   visible = !visible;
	   if (!visible)
	     gui->delete_layer(layerId);
	   else
	     createLayer();
	 }
     });

  subscribe<CursorLeftMessage>
    ([this](CursorLeftMessage &msg){
       --actionCursor;
       actionCursorChanged();
     });

  subscribe<CursorRightMessage>
    ([this](CursorRightMessage &msg){
       ++actionCursor;
       actionCursorChanged();
     });
}

void
InventorySystem::actionCursorChanged()
{
  if (!currentObject)
    {
      actionCursor=0;
      return;
    }

  auto pa = currentObject->possibleActions();
  if (actionCursor < 0)
    actionCursor = pa.size() - 1;
  if (actionCursor > (int)pa.size() - 1)
    actionCursor = 0;
}


void
InventorySystem::update(double duration_ms)
{
    if (!visible)
      return;

  
    term(layerId)->clear(vchar{' ', WHITE, BG});

    term(layerId)->clear(vchar{' ', YELLOW, BG});
    term(layerId)->box(DARKEST_GREEN, BG);
    term(layerId)->print_center(0, _("| Inventory |"), Tomato, BG);


    auto inv = entity(player_id)->component<Inventory>();
    int y = 0;
    float totalWeight = 0.0f;
    for (auto e : inv->getEntities())
      {
	
	color_t fg = Black;
	color_t bg = BG;
	if (y == cursor)
	  {
	    bg = fg;
	    fg = BG;
	    currentObject = e;
	  }

	if (inv->isEquiped(e))
	    term(layerId)->print( 5 ,y +5, "EQU", fg, bg);

	term(layerId)->print( 10 ,y +5, e->getName(), fg, bg);
	term(layerId)->print( 30 ,y +5, e->getDescription(), fg, bg);
	totalWeight += e->getWeight();
	++y;
      }

    printActionMenu();
    char weightStr[80];
    sprintf(weightStr, _("Total weight : %.3f Kg").c_str(), totalWeight);
    term(layerId)->print( 2 ,35 , weightStr, Black, BG);
}

void
InventorySystem::createLayer()
{
  auto s = get_window()->getSize();
  
  int w = width;
  int h = height;
   
  int x = (s.x - w)/2;
  int y = (s.y - h)/2;

  gui->add_layer(layerId, x, y, w, h, "8x16", resize_inventory);
}

void
InventorySystem::cursorChanged(void)
{
  int s = (int)entity(player_id)->component<Inventory>()->getEntities().size();

  if (cursor > s - 1)
    cursor = 0;
  if (cursor < 0)
    cursor =  s - 1;

  actionCursor = 0;
}

void
InventorySystem::action(void)
{
  if (!visible)
    return;

  if (!currentObject)
    return;

  auto charac = entity(player_id)->component<Character>();
  Weapon* w = dynamic_cast<Weapon*>(currentObject);
  if (w)
    {
      // Unequip
      if (w == charac->getLHWeapon() && actionCursor == 0)
	{
	  charac->unequip(CH_LEFT);
	  return;
	}
      else if (w == charac->getRHWeapon() && actionCursor == 1)
	{
	  charac->unequip(CH_RIGHT);
	  return;
	}

      // Equip
      if (actionCursor == 0)
	{
	  charac->setLHWeapon(w);
	  return;
	}
      else
	{
	  charac->setRHWeapon(w);
	  return;
	}
    }
  
  // Eat the chicken leg!
  int y = 0;
  int aid = 0;
  auto inv = entity(player_id)->component<Inventory>();
  for (auto e : inv->getEntities())
    {
      if (y == cursor)
	{
	  auto pa = e->possibleActions();
	  for (auto a : pa)
	    {
	      if (aid == actionCursor)
		e->act(a);

	      ++aid;
	    }
	}
      ++y;
    }
  emit(HudUpdateMessage());
}

string
InventorySystem::handleUnequip(int actionIdx, const string& actionName)
{
  // Only prints unequip on the equiped hand action
  Weapon* w = dynamic_cast<Weapon*>(currentObject);
  if (w)
    {
      auto charac = entity(player_id)->component<Character>();
      if ((w == charac->getLHWeapon() && actionIdx == 0) ||
	  (w == charac->getRHWeapon() && actionIdx == 1))
	return "Unequip";
    }
  return actionName;
}

void
InventorySystem::printActionMenu(void)
{
  if (!currentObject)
    return;
    
  // We need currently selected object
  term(layerId)->print( 2 ,30 , "Actions :", Black, BG);

  int x = 22;
  int actionIdx = 0;
  auto pa = currentObject->possibleActions();

  for (auto ca : pa)
    {
      auto tca = TranslationManager::getInstance().get(ca);
      if (actionCursor == actionIdx)
	term(layerId)->print( x ,30 , handleUnequip(actionIdx, tca), BG, Black);
      else
	term(layerId)->print( x ,30 , handleUnequip(actionIdx, tca), Black, BG);
      
      x+=15;
      ++actionIdx;
    }
}

