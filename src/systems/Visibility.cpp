
#include "Visibility.hpp"


#include "Position.hpp"
#include "NavigatorHelper.hpp"
#include "PlayedTime.hpp"

#include "MapManager.hpp"
#include "Score.hpp"

#include "systems/Player.hpp" // USES nb_moves

#include "api/Logger.hpp"
#include "api/PlayerId.hpp" // USES player_id

#include "messages/AddLogEntry.hpp"
#include "messages/PlayerMoved.hpp"

#include "config.h" // USES _()

bool firstrun = true;

void
VisibilitySystem::configure()
{
    system_name = "Visibility System";
    subscribe<PlayerMovedMessage>
      ([this](PlayerMovedMessage &msg)  {
	 auto camera_loc = entity(player_id)->component<Position>();
	 Position camera_loc_deref = *camera_loc;

	 MapManager::getInstance().getCurrentMap()->resetVisible();
	 visibility_sweep_2d<Position, NavigatorHelper>
	   (camera_loc_deref, 10,[&](Position reveal)
  	     {
	       auto map = MapManager::getInstance().getCurrentMap();
	       reveal.bounds_check(map->getWidth(), map->getHeight());
	       const int idx = MapManager::getInstance().getCurrentMap()->idx(reveal.x, reveal.y);
	       MapManager::getInstance().getCurrentMap()->setVisible(reveal.x, reveal.y, MapManager::getInstance().getCurrentMap()->getVisible(idx) + 1);
	       
	       if (MapManager::getInstance().getCurrentMap()->getVisible(idx) > 8)
		 MapManager::getInstance().getCurrentMap()->setVisible(reveal.x, reveal.y, 8);

	       newVisibility(idx);
	       checkExit(idx);
				   
	       MapManager::getInstance().getCurrentMap()->setRevealed(reveal.x, reveal.y, true);
	     },	[](Position visibility_check) {
		  visibility_check.bounds_check();
		  return (MapManager::getInstance().getCurrentMap()->getTile(visibility_check.x, visibility_check.y) != 1);
		});
       });
}

void
VisibilitySystem::update(const double duration_ms)
{
  if (firstrun)
    {
      emit(PlayerMovedMessage{});
      firstrun = false;
    }
}

/** Check if the index is a unknown/unreveleated exit and send the 
    log message if needed */
void VisibilitySystem::checkExit(int idx)
{
  if (MapManager::getInstance().getCurrentMap()->getRevealed(idx)) return;

  int t = MapManager::getInstance().getCurrentMap()->getTile(idx);
  int actualLevel = MapManager::getInstance().getDungeonLevel();

  string s;
  switch(t)
    {
    case 2:  // Level down
      s = _("You found a down stair.");
      break;
    case 3:  // Level up
      if (actualLevel == 1)
	s = _("You found a stair leading up. "
	      "You can exit but will loose the game.");
      else
	s = _("You found a stair leading up.");
	
      break;
    case 4:  // Level hatch
      s = _("You found a hatch. You can go down using "
	    "'ENTER' key but won't be able to go back.");
      break;
    default:
      return;
    }
  if (t>1 && t< 5)
    {
      LOG(LogLevel::Info, s.c_str());
    }
  

  auto sc = entity(player_id)->component<Score>();
  sc->addExit();
  
  // Send Exit found message
  AddLogEntryMessage aem;
  aem.nbMoves = nb_moves;
  aem.time = playedTime.str();
  aem.msg = s;
  emit(aem);
}

void
VisibilitySystem::newVisibility(int idx)
{
  if (MapManager::getInstance().getCurrentMap()->getRevealed(idx)) return;

  auto sc = entity(player_id)->component<Score>();
  sc->addVisibility();
}
