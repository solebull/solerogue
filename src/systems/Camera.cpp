#include "Camera.hpp"

#include "Position.hpp"
#include "Renderable.hpp"
#include "Score.hpp"

#include "components/Character.hpp"
#include "components/Inventory.hpp"

#include "Layers.hpp"
#include "MapManager.hpp"
#include "ScriptManager.hpp"

#include "api/Rng.hpp"
#include "api/Entity.hpp"
#include "api/Attributes.hpp"

#include "messages/ChangeMainFont.hpp"

#include <cassert>

#include "api/PlayerId.hpp" // USES playerId

// Clipping info
int left_x, right_x, top_y, bottom_y;

void
CameraSystem::configure()
{
  system_name = "Camera System";
  // Create the player : Here the base class, without suffixes. If a class
  // named Inventory exists in ui/ (it exists), it must be called InventoryUi.
  auto player = create_entity()
    ->assign(Position(MapManager::getInstance().getCurrentMap()->getWidth()/2, MapManager::getInstance().getCurrentMap()->getHeight()/2))
    //    ->assign(Renderable('@', colors::YELLOW))
    ->assign(Renderable(0x01, colors::YELLOW))
    ->assign(Attributes())
    ->assign(Score())
    ->assign(Inventory())
    ->assign(Character());
  
  player_id = player->id;
  // Will be resized in update()
  /*
  buffer.resize(term(MAIN_LAYER)->term_width,
		term(MAIN_LAYER)->term_height);
  */

  // Zoom handling
  subscribe<ChangeMainFontMessage>
    ([this](ChangeMainFontMessage &msg){
       gui->delete_layer(MAIN_LAYER);
       LOG(LogLevel::Info, "Changing main layer zoom to %s",
	   msg.getNextFont().c_str());
       
       auto w=rltk::get_window();
       auto width = w->getSize().x;
       auto height = w->getSize().y;

       gui->add_layer(MAIN_LAYER, 0, 32, width-160, height-32,
		      msg.getNextFont(), resize_main);
       
     });

  // Tile outline handling
  subscribe<OutlineTileMessage>
    ([this](OutlineTileMessage &msg){
       this->outline = msg;
     });
  
}

void
CameraSystem::update(const double duration_ms)
{

  //  if (term(MAIN_LAYER)->dirty)

      buffer.resize(term(MAIN_LAYER)->term_width,
		    term(MAIN_LAYER)->term_height);
      buffer.clear();
      
      auto pl = entity(player_id);
      assert(pl && "Player entity is NULL");
      auto camera_loc = pl->component<Position>();
      assert(camera_loc && "Camera_loc entity is NULL");
      
      left_x = camera_loc->getX() - (term(MAIN_LAYER)->term_width / 2);
      right_x = camera_loc->getX() + (term(MAIN_LAYER)->term_width / 2);
      top_y = camera_loc->getY() - (term(MAIN_LAYER)->term_height/2);
      bottom_y = camera_loc->getY() + (term(MAIN_LAYER)->term_height/2)+1;

      int mcx = (term(MAIN_LAYER)->term_width/2)+1;
      int mcy = (term(MAIN_LAYER)->term_height/2)+1;
      //      LOG(LogLevel::Debug, "Settings map center to '%d, %d", mcx, mcy);
      buffer.setMapCenter(mcx, mcy);
      
      for (int y=top_y; y<bottom_y; ++y) {
	for (int x=left_x; x<right_x; ++x) {
	  if (x >= 0 && x < (int)MapManager::getInstance().getCurrentMap()->getWidth() && y >= 0 &&
	      y < (int)MapManager::getInstance().getCurrentMap()->getHeight()) {
	    vchar map_char{'.', color_t(0,0,64), colors::BLACK};
	    const int map_index = MapManager::getInstance().getCurrentMap()
	      ->idx(x,y);

	    if (MapManager::getInstance().getCurrentMap()->
		getRevealed(x, y)) {
	      switch (MapManager::getInstance().getCurrentMap()
		      ->getTile(x, y))
		{
		case 0 : map_char.glyph = '.'; break;
		  //		case 1 : map_char.glyph = '#'; break;
		case 1 : map_char.glyph = getWall(x, y); break;
		case 2 : map_char.glyph = 'v'; break;
		case 3 : map_char.glyph = '^'; break;
		case 4 : map_char.glyph = '='; break;
		default : map_char.glyph = 'E'; // This shouldn't happen
		}
	      map_char.foreground = color_t(96,96,96);
	    }
	    if (MapManager::getInstance().getCurrentMap()->getVisible(x, y)>0)
	      {
		uint8_t brightness = MapManager::getInstance().getCurrentMap()->
		  getVisible(map_index) *16 + 127;
		map_char.foreground = color_t(brightness,brightness,brightness);
	      } else {
	      if (MapManager::getInstance().getCurrentMap()->getTile(x, y) == 0)
		map_char.glyph = ' ';
	    }
	    buffer.set_char(x-left_x, y-top_y, map_char);
	    
	  }
	}
      }

  /// Drawn even if terminal isn't dirty
  auto map = MapManager::getInstance().getCurrentMap();
  auto entities = map->getEntities();
  for (auto& e : entities)
    drawEntity(e, map);

  ScriptManager::getInstance().call_OnFrame(duration_ms, &buffer);

  // Outline a given char (under mouse ?)
  if (outline.show) {
    vchar mc{'X', color_t(255, 255, 0), colors::BLACK};
    buffer.set_char(outline.x - left_x, outline.y - top_y , mc);
  }
  
  // Finally draw the layer
  buffer.draw(term(MAIN_LAYER));
}

/** Draw the given entity
  *
  * \param e The entity to be drawn
  * \param m The map we draw on
  *
  */
void
CameraSystem::drawEntity(Entity* e, Map* m)
{
  if (e->getX() > (int)m->getWidth() ||
      e->getY() > (int)m->getHeight())
    return;
  
  int lx = e->getX() - left_x;
  int ly = e->getY() - top_y;
  
  if (lx < 0 || ly < 0)
    return;
  
  if (m->getVisible(e->getX(), e->getY()) > 0)
    buffer.set_char(lx, ly, e->getVChar());
  else if (m->getRevealed(e->getX(), e->getY()))
    buffer.set_char(lx, ly, e->getDarkVChar());
}

/** Return a wall tile
  *
  * For instance a random one, but in near future, may come from a mod.
  * Especially designed for the multiwall mod.
  *
  */
int
CameraSystem::getWall(int x, int y)
{
  int depth=MapManager::getInstance().getDungeonLevel();
  int i = ScriptManager::getInstance().call_OnGetTile(depth, x, y, true);
  return i==-1 ? 176 : i;
}

