#include "SoundManager.hpp"

#include <gtest/gtest.h>

TEST( SoundManager, get_mood )
{
  SoundManager sm;
  ASSERT_EQ( sm.getMood(), 50);
}

TEST( SoundManager, inc_mood )
{
  SoundManager sm;
  sm.incMood(20);
  ASSERT_EQ( sm.getMood(), 70);

  // Limit to 100
  sm.incMood(50);
  ASSERT_EQ( sm.getMood(), 100);

}

TEST( SoundManager, dec_mood )
{
  SoundManager sm;
  sm.decMood(20);
  ASSERT_EQ( sm.getMood(), 30);

  // Limit to 0
  sm.decMood(50);
  ASSERT_EQ( sm.getMood(), 0);
}
