#include "EnemyInfo.hpp"

#include "Layers.hpp"
#include "MapManager.hpp"

#include "api/Entity.hpp"
#include "api/PlayerId.hpp" // USES player_id

#include "messages/PlayerMoved.hpp"

constexpr int ENEMY_VBAR = 1;

using namespace rltk::colors;

void
resize_enemyinfo(layer_t * l, int w, int h) {
  l->x = 5;
  l->y = 80;
  l->w = 40;
  l->h = 520;
}


EnemyInfoSystem::EnemyInfoSystem():
  base_system(),
  layerId(ENI_LAYER),
  nextTrackingId(ENEMY_VBAR),
  visible(false)
{

}
  
EnemyInfoSystem::~EnemyInfoSystem()
{

}

void
EnemyInfoSystem::configure()
{
  system_name = "Enemy info";
  
  if (visible)
    createLayer();

  subscribe<PlayerMovedMessage>([this](PlayerMovedMessage &msg)
     {
       auto m= MapManager::getInstance().getCurrentMap();
       auto es= m->getEntities();
              
       int tracked = 0;
       for (auto& e : es)
	 {
	   if (m->getVisible(e->getX(), e->getY()) > 0 && e->isEnemy())
	     {
	       setVisible(true);
	       addTracking(e);
	       ++tracked;
	     }

	 }

       setVisible(tracked > 0);
     });

}

void
EnemyInfoSystem::update(double duration_ms)
{
  if (!visible)
    return;

  
  term(layerId)->clear(vchar{' ', WHITE, DARKEST_GREEN});

  updateControls();
}

void
EnemyInfoSystem::updateControls()
{
  for (auto te : trackedEntities)
    {
      try
	{
	  retained->control<gui_vbar_t>(te.second)->value =
	    te.first->getAttributes()->getInt("health");
	}
      catch (const runtime_error& /* e */ )
      {
	// We certainly tried to get a non-existing GUI control
	cout << "Can't get GUI control with ID " << te.second << endl;
      }
    }
}

void
EnemyInfoSystem::addTracking(Entity* e)
{
  // Only add if not present in the map
  if (trackedEntities.find(e) == trackedEntities.end())
    {
      int x = 1 + nextTrackingId;
      trackedEntities[e] = nextTrackingId;

      auto att = e->getAttributes();
      int max = att->getInt("health-max");
      int hp = att->getInt("health");
      
      retained->add_vbar(nextTrackingId, x, 1, 28, 0, max, hp,
			 color_t(128,64,0), color_t(0,128,0), 
			 color_t(128,128,128), color_t(64,64,64),
			 WHITE, e->getName() + ": ");

      nextTrackingId++;
    }
}

void
EnemyInfoSystem::setVisible(bool vB)
{
  if (vB)
    {
      if (!visible)
	createLayer();
    }
  else
    {
      nextTrackingId = ENEMY_VBAR;
      trackedEntities.clear();      
      if (visible)
	{
	  gui->delete_layer(layerId);
	}
    }
      
  visible = vB;
}

void
EnemyInfoSystem::createLayer(void)
{
  gui->add_layer(ENI_LAYER, 5, 40, 80, 520, "8x16", resize_enemyinfo);
  retained = layer(ENI_LAYER);
}
