#ifndef _HIGH_SCORES_LAYER_HPP_
#define _HIGH_SCORES_LAYER_HPP_

#include "rltk.hpp"

#include <string>
#include <list>

#include "HighScoresEntry.hpp"

using namespace std;
using namespace rltk;

class HighScoresSystem : public base_system
{
public:
  HighScoresSystem();
  virtual ~HighScoresSystem();
  
  virtual void configure() override;
  virtual void update(double duration_ms) override;

  void add();
  
  void sort();

  void serialize(struct gzip_file& g);
  void deserialize(struct gzip_file& g);

  void save();
  void load();
  
  std::list<HighScoresEntry> scores;
  int layerId;

  void print(const std::string&);
  
protected:
  void createLayer();
  
private:
  bool visible;
  int version;
};

#endif // !_HIGH_SCORES_LAYER_HPP_
