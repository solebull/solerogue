#include "NodePointing.hpp"
#include <gtest/gtest.h>

#include "MapManager.hpp"

class NodePointingSystemTest: public NodePointingSystem
{
public:
  bool outOfBoundsTest(int x, int y)
  {
    return outOfBounds(x, y);
  }
};

TEST( NodePointingSystem, outOfBounds )
{
  NodePointingSystemTest np;

  auto w = MapManager::getInstance().getCurrentMap()->getWidth();
  auto h = MapManager::getInstance().getCurrentMap()->getHeight();
  
  // Tested map is 100x100
  ASSERT_EQ( np.outOfBoundsTest(-1, 5), true);
  ASSERT_EQ( np.outOfBoundsTest(0, -1), true);
  ASSERT_EQ( np.outOfBoundsTest(w + 10, 5), true);
  
  ASSERT_EQ( np.outOfBoundsTest(w -2, h - 2), false);
}
