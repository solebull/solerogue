#include "Zoom.hpp"

#include "api/Logger.hpp"

#include "messages/Zoom.hpp"
#include "messages/ChangeMainFont.hpp"

#include <algorithm>


int configure_call = 1;


using namespace std;

// Static variable assignment
string ZoomSystem::assets_dir = "";


/** Compute the weight of a font in order to sort them
  *
  * \param s A string of the form 8x16, 12x12
  *
  */
int
font_weight(const string& s)
{
  std::size_t found = s.find("x");
  if (found==std::string::npos)
    throw std::runtime_error("Can't get resolution weight");
  
  return stoi(s) + stoi(s.substr(found+1));
}

/** A callback used to sort two font strings by weight
  *
  */
bool
sort_font(const string& s1, const string& s2)
{
  return (font_weight(s1) < font_weight(s2));
}


/** Default constructor
  *
  */
ZoomSystem::ZoomSystem():
  current_font(0)
{

}

/** Change the assets dir to a new one
  *
  * This will change the tiles. A fonts.txt file is needed in the dir.
  *
  */
void
ZoomSystem::setAssetsDir(const string& ad)
{
  assets_dir = ad;
}

/** The ecs configure callback
  *
  */
void
ZoomSystem::configure()
{
  LOG(LogLevel::Error, "Calling ZoomSystem::configure %d", configure_call++);
  string line;
  system_name = "Zoom System";
  
  if (assets_dir.empty())
    throw std::runtime_error("ZoomSystem's 'assets_dir' can't be blank");

  string fonttxt = assets_dir + "/fonts.txt";
  const char* pathname = fonttxt.c_str();

  // Check for txt file
  LOG(LogLevel::Debug, "Opening '%s' fonts file", pathname);
  struct stat info;
  if( stat( pathname, &info ) != 0 )
    throw std::runtime_error("Can't find '" + fonttxt  + "'" );

  // Read font file
  ifstream myfile(fonttxt);
  if (myfile.is_open())
    {
      while ( getline (myfile,line) )
	{
	  string res;
	  std::size_t found = line.find(","); // Comma separated dvalue
	  if (found!=std::string::npos)
	    addFont(line.substr(0, found));
	}
      myfile.close();
    }

  // Sort vector
  sortFontVector();
  int median = fonts.size()/2;
  current_font = median;  
  emit(ChangeMainFontMessage(fonts[median]));
  
  subscribe<ZoomInMessage>
    ([this](ZoomInMessage &msg){
       next();
       LOG(LogLevel::Debug, "Zoom in key pressed! current font is now %d",
	   current_font);
       emit(ChangeMainFontMessage(fonts[current_font]));
     });

    subscribe<ZoomOutMessage>
    ([this](ZoomOutMessage &msg){
       previous();
       LOG(LogLevel::Debug, "Zoom out key pressed! current font is now %d",
	   current_font);
       emit(ChangeMainFontMessage(fonts[current_font]));
     });
}

/** The rltk's ecs update function
  *
  * \param duration_ms The frame delta.
  *
  */
void
ZoomSystem::update(const double duration_ms)
{

}

/** Add the given font to the font vector
  *
  * \param s The font string.
  *
  */
void
ZoomSystem::addFont(const string& s)
{
  // Only add square fonts
  std::size_t found = s.find("x");
  if (found==std::string::npos)
    throw std::runtime_error("Can't check for resolution squareness");
  
  if (stoi(s) == stoi(s.substr(found+1)))
    fonts.push_back(s);
  else
    {
      LOG(LogLevel::Warning,"Will not add non-square resolution : '%s'",
	  s.c_str());
    }
}

/** Returns the font vector
  *
  * Note : If you want it sorted, you must call \ref sortFontVector()
  * before.
  *
  * \return A reference to the font vector.
  *
  */
const vector<string>&
ZoomSystem::getFontVector(void)const
{
  return fonts;
}

/** Sort the the font vector
  *
  * Note: this will modify local vector.
  *
  */
void
ZoomSystem::sortFontVector(void)
{
  sort(fonts.begin(), fonts.end(), sort_font);
}


/** Set current_font to the previous vector item
  *
  */
void
ZoomSystem::previous(void)
{
  current_font--;
  // We have to check for max bound because of the unsigned nature of the
  // variable. When it could be -1, it becomes MAX-1 : extremly high positive.
  if (current_font < 0  || current_font >= fonts.size())
    current_font = 0;
}

/** Set current_font to the next vector item
  *
  */
void
ZoomSystem::next(void)
{
  current_font++;
  if (current_font >= fonts.size())
    current_font = fonts.size() - 1;
}
