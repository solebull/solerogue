#include "Gui.hpp"

#include "Layers.hpp" // USES MAIN_LAYER

#include "messages/Zoom.hpp"
#include "messages/CursorUp.hpp"
#include "messages/CursorDown.hpp"
#include "messages/AddLogEntry.hpp"
#include "messages/CursorAction.hpp"
#include "messages/CursorLeft.hpp"
#include "messages/CursorRight.hpp"
#include "messages/ToggleVisibility.hpp"
#include "messages/ChangeMainFont.hpp"
#include "messages/ChangeObjectsInfo.hpp"

#include "ui/Form.hpp"
#include "ui/Character.hpp"
#include "ui/Highscores.hpp"

#include "api/Logger.hpp"

#include "rltk.hpp"

#include <list>

using namespace std;

int gs_configure_call = 1;

// Static because called from a static CuiSystem member
static list<Form*> forms;

GuiSystem::GuiSystem()
{

}

GuiSystem::~GuiSystem()
{

}

void
GuiSystem::configure()
{
  LOG(LogLevel::Error, "Calling GuiSystem::configure %d", gs_configure_call++);

  system_name = "Gui system";

  HelpViewerUi::getInstance().configure();

  forms.emplace_back(new CharacterUi());
  forms.push_back(&log);
  forms.push_back(&info);
  forms.emplace_back(new HighscoresUi());
  /*
  for (auto& f: forms)
    f->configure();
  */
  subscribe<ToggleVisibilityMessage>
    ([this](ToggleVisibilityMessage &msg){
       
       if (msg.getElementName() == "Help")
	 {
	   HelpViewerUi::getInstance().toggleVisibility();
	 }
       else
	 {
	   LOG(LogLevel::Info, "Toggle %s visibility",
	       msg.getElementName().c_str());
	   for (auto& f: forms)
	     f->toggleVisibility(msg.getElementName());
	 }
     });

  subscribe<CursorUpMessage>([this](CursorUpMessage &msg){
       HelpViewerUi::getInstance().cursorUp();

       for (auto& f: forms)
	 f->cursorUp();

     });

  subscribe<CursorDownMessage>([this](CursorDownMessage &msg){
       ++cursor;
       HelpViewerUi::getInstance().cursorDown();

       for (auto& f: forms)
	 f->cursorDown();

     });

  subscribe<CursorLeftMessage>([this](CursorLeftMessage &msg){
      for (auto& f: forms)
	   f->cursorLeft();
       });

  subscribe<CursorRightMessage>([this](CursorRightMessage &msg){
     for (auto& f: forms)
       f->cursorRight();
  });

  subscribe<CursorActionMessage>([this](CursorActionMessage &msg){
      action();
   });
  
  subscribe<AddLogEntryMessage>([this](AddLogEntryMessage &msg)  {
    log.add(msg);				  
  });

  // Forwarding to Info

  subscribe<ChangeObjectsInfoMessage>
    ([this](ChangeObjectsInfoMessage &msg)
     {
       info.onChangeObjectsInfo(msg);
     });

  subscribe<ChangeMainFontMessage>
    ([this](ChangeMainFontMessage &msg){
       info.onChangeMainFont(msg);
     });

  subscribe<ChangeNodeInfoMessage>
    ([this](ChangeNodeInfoMessage &msg)
     {
       info.onChangeNodeInfo(msg);
       
     });

  
}

void
GuiSystem::update(double duration_ms)
{
  // UI dialogs is done in display_hook to be able to draw TTF font
  // after rltk's bitmap one.
  info.update(duration_ms);
}

void
GuiSystem::display_hook(void)
{
  HelpViewerUi::getInstance().draw();

  for (auto& f: forms)
    f->draw();
}

void
GuiSystem::cursorChanged()
{
  HelpViewerUi::getInstance().cursorChanged();
}

void
GuiSystem::action()
{
  HelpViewerUi::getInstance().action();

  for (auto& f: forms)
    if (f->action())
      return;
}

// Returns rltk can use ? (rltk/rltk.cpp:105)
bool
GuiSystem::resize_hook(unsigned int width, unsigned int height)
{
  HelpViewerUi::getInstance().resize(width, height);

  for (auto& f: forms)
    f->resize(width, height);
  
  return true;
}

/** Handle th mouse wheel events zooming feature 
  *
  */
bool
GuiSystem::inject_wheel(unsigned int x, unsigned int y, float delta)
{
  if (delta > 0)
    emit(ZoomInMessage{});

  if (delta < 0)
    emit(ZoomOutMessage{});

  return true;
}
