#ifndef _VISIBILITY_SYSTEM_HPP_
#define _VISIBILITY_SYSTEM_HPP_

#include "rltk.hpp"

using namespace rltk;

class VisibilitySystem : public base_system {
public:
  virtual void configure() override;
  bool firstrun = true;

  virtual void update(const double duration_ms) override;

protected:
  void checkExit(int);
  void newVisibility(int);
};

#endif // !_VISIBILITY_SYSTEM_HPP_
