#ifndef _ACTOR_RNDER_SYSTEM_HPP_
#define _ACTOR_RNDER_SYSTEM_HPP_

#include "rltk.hpp"

using namespace rltk;

extern int left_x, right_x, top_y, bottom_y;

class ActorRenderSystem : public base_system {
public:
  virtual void configure() override;

  virtual void update(const double duration_ms) override;
};

#endif // !_ACTOR_RNDER_SYSTEM_HPP_
