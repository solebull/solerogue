#include "SoundManager.hpp"

#include "api/Logger.hpp"
#include "api/File.hpp"

#include "messages/PlaySfx.hpp"


#include <random>

using namespace std;

SoundManager::SoundManager() :
  rltk::base_system(),
  mood(50)
{
  ambianceFiles = File::fglob("../media/music/ambiance/*.ogg");
  actionFiles = File::fglob("../media/music/action/*.ogg");
  sorrowFiles = File::fglob("../media/music/sorrow/*.ogg");
  dumpVector("Ambiance music files", ambianceFiles);
}

SoundManager::~SoundManager()
{

}

void
SoundManager::configure(void)
{
  system_name = "SoundManager";

  subscribe<PlaySfxMessage>
    ([this](PlaySfxMessage &msg){
       playSfx(msg.filename, msg.pitch, msg.volume);
       
     });

}

void
SoundManager::update(const double duration_ms)
{
  auto files = ambianceFiles; 

  if (mood<25)
    files = sorrowFiles;

  if (mood>70)
    files = actionFiles;

  
  if (music.getStatus() == sf::SoundSource::Status::Stopped)
    {
      if (files.size() > 0)
	{
	  // Get a random filename
	  std::random_device random_device;
	  std::mt19937 engine{random_device()};
	  std::uniform_int_distribution<int> dist(0, files.size() - 1);
	  string file = files[dist(engine)];
	  playMusic(file);
	}
    }

  if (currentSfx.empty())
    return;
  
  // Delete all sfx if stopped
  bool clear = true;
  for (auto const& s : currentSfx)
    if (s->sound->getStatus() != sf::SoundSource::Status::Stopped)
      clear = false;

  if (clear)
    {
      LOG(LogLevel::Info, "Deleting all Sfx from vector");
      currentSfx.clear();
    }
}

int
SoundManager::getMood(void)
{
  return mood;
}

void
SoundManager::playMusic(const std::string& f)
{
  if (!music.openFromFile(f))
    LOG(LogLevel::Error, "Can't open music file : %s", f.c_str());
  
  LOG(LogLevel::Error, "Playing music file : %s", f.c_str());
  music.play();
}

/** Just a debugging function used to dump vector content
  *
  */
void
SoundManager::dumpVector(const std::string& t,
			 const std::vector<std::string>& v)
{
  LOG(LogLevel::Debug, "Dump vector file '%s'", t.c_str());
  for (auto const& s : v)
    LOG(LogLevel::Debug, "  - %s", s.c_str());
}

void 
SoundManager::incMood(int m)
{
  mood += m;
  if (mood>100)
    mood = 100;
}

void 
SoundManager::decMood(int m)
{
  mood -= m;
  if (mood<0)
    mood = 0;
}


/**
 * pitch: normal at 1.0f
 * volume: up to 100.0f
 */
void
SoundManager::playSfx(const std::string& file, float pitch, float volume)
{
  auto b = new sf::SoundBuffer();
  if (!b->loadFromFile(file))
    throw("Error loading ambient OGG file");
    //LOG(LogLevel::Error, "Error loading ambient OGG file");

  unsigned int sampleRate = b->getSampleRate();
  LOG(LogLevel::Info, "ambient file sample rate : %d", sampleRate);
  unsigned int channels   = b->getChannelCount();
  LOG(LogLevel::Info, "ambient file channels : %d", channels);

  auto s = new sf::Sound();
  s->setPitch(pitch);
  s->setVolume(volume);
  s->setBuffer(*b);
  s->play();

  auto sfx = new Sfx();
  sfx->buffer = b;
  sfx->sound = s;
  currentSfx.push_back(sfx);
}

