#ifndef __METRICS_SYSTEM_HPP__
#define __METRICS_SYSTEM_HPP__

#include "rltk.hpp"

#include <string>

using namespace rltk;
using namespace std;

class MetricsSystem : public base_system
{
public:
  MetricsSystem();
  virtual ~MetricsSystem();

  virtual void configure() override;
  virtual void update(double duration_ms) override;

protected:
  void createLayer();
  
private:
  int layerId;
  bool visible;
  double nextUpdate;
  std::string dump;
  
};
#endif // !__METRICS_SYSTEM_HPP__

