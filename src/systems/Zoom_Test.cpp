#include "Zoom.hpp"
#include <gtest/gtest.h>

#include <vector>

using namespace std;

/** An override \ref ZoomSystem class implementation
  *
  */
class ZoomSystemTest : public ZoomSystem
{
public:
  /// An override of protected method \ref ZoomSystem::sortFontVector().
  void sortFontVector(void){ ZoomSystem::sortFontVector(); }
};

TEST( Zoom, stoi32x )
{
  ASSERT_EQ( stoi("32x"), 32);
}

TEST( Zoom, font_weight )
{
  ASSERT_EQ( font_weight("8x8"), 16);
}

TEST( Zoom, sort_font )
{
  ASSERT_TRUE( sort_font("8x8", "8x16"));
}

// Will not add non-square fonts
TEST( Zoom, dont_add_non_square )
{
  ZoomSystemTest z;
  ASSERT_EQ( z.getFontVector().size(), 0 );
  z.addFont("16x8");
  ASSERT_EQ( z.getFontVector().size(), 0 );
  z.addFont("32x32");
  z.addFont("8x8");
  ASSERT_EQ( z.getFontVector().size(), 2 );
}


TEST( Zoom, outOfBounds )
{
  ZoomSystemTest z;
  z.addFont("32x32");
  z.addFont("8x8");
  z.addFont("16x16");
  z.sortFontVector();
  
  auto fonts = z.getFontVector();
  for (auto& ft : fonts)
    cout << "Font = " << ft << endl;

  
  // Tested map is 100x100
  ASSERT_EQ( fonts[0], "8x8");
  ASSERT_EQ( fonts[1], "16x16");
  ASSERT_EQ( fonts[2], "32x32");
}
