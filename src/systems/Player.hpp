#ifndef _PLAYER_SYSTEM_HPP_
#define _PLAYER_SYSTEM_HPP_

#include "rltk.hpp"

#include <string>

using namespace std;
using namespace rltk;

extern int    nb_moves;
extern string playerName;
extern string playerTitle;

// Forward declarations
class Entity;
class Weapon;
// End of forward declarations


void generate_player_name();

/** The Player system
  *
  * Handles pressed key event and PlayerMovedMessage.
  *
  */
class PlayerSystem : public base_system
{
public:
  virtual void configure() override;
  virtual void update(const double duration_ms) override;

  void attack(Entity*, Weapon*);
  
  static bool isAlive(void);
protected:
  Entity* notWalkable(int, int) const;
};

#endif // !_PLAYER_SYSTEM_HPP_
