#include "Hud.hpp"

#include "Layers.hpp"

#include "api/Attributes.hpp"

#include "systems/Camera.hpp" // USES player_id
#include "config.h"           // USES _()

#include "Morgue.hpp"


#include "messages/AttributeModifier.hpp"
#include "messages/GameOver.hpp"
#include "messages/HudUpdate.hpp"

using namespace rltk::colors;

#include <iostream>

using namespace std;

constexpr int HEALTH_HBAR = 6;
constexpr int MANA_HBAR = 7;

void resize_retained(layer_t * l, int w, int h) {
  // Do nothing - we'll just keep on rendering away.
  l->x = 10;
  l->y = 10;
  l->w = 400;
  l->h = 60;
}

Hud::Hud():
  layerId(HUD_LAYER),
  gameOverEmitted(false)
{

}

Hud::~Hud()
{

}

void
Hud::configure()
{
  system_name = "HUD";

  gui->add_layer(HUD_LAYER, 10, 10, 400, 60, "8x16", resize_retained);
  retained = layer(HUD_LAYER);
  // add_hbar(const int handle, const int X, const int Y, const int W, const int MIN, const int MAX, const int VAL, 
  // 	const color_t FULL_START, const color_t FULL_END, const color_t EMPTY_START, const color_t EMPTY_END, const color_t TEXT_COL
  retained->add_hbar(HEALTH_HBAR, 1, 1, 46, 0, 100, 80,
		     color_t(128,64,0), color_t(0,128,0), 
		     color_t(128,128,128), color_t(64,64,64),
		     WHITE, "Santé: ");
  retained->add_hbar(MANA_HBAR, 1, 2, 46, 0, 100, 50, color_t(0,0,128),
		     color_t(0,0,255), color_t(128,128,128),
		     color_t(64,64,64), WHITE, _("Mana: "));


  updateControls();

  subscribe<AttributeModifierMessage>
    ([this](AttributeModifierMessage &msg){
       if (msg.getEntityId() == player_id)
	 {
	   auto pl = entity(player_id);
	   auto att = pl->component<Attributes>();
	   att->intInc("health", msg.getValue());

	   updateControls();

	   auto health  = att->getInt("health");
	   if (health < 1 && !gameOverEmitted)
	     {
	       emit(GameOverMessage());
	       gameOverEmitted = true;
	     }

	 }
     });

  subscribe<HudUpdateMessage>([this](HudUpdateMessage &msg)
    {
      updateControls();
      
    });
}

void
Hud::update(double duration_ms)
{
  term(layerId)->clear(vchar{' ', WHITE, DARKEST_GREEN});

 
}

void
Hud::updateControls()
{
  auto att = entity(player_id)->component<Attributes>();
  retained->control<gui_hbar_t>(HEALTH_HBAR)->value = att->getInt("health");
}

