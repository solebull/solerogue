#include "Metrics.hpp"

#include "Layers.hpp"
#include "messages/ToggleVisibility.hpp"

using namespace rltk::colors;

constexpr double MetricsUpdate = 100; // Update metrics layer every ms

void
resize_metrics(layer_t * l, int w, int h) {
  // Simply set the width to the whole window width
  l->w = 800;
  l->h = 500;

  l->x = (w - l->w)/2;
  l->y = 0;
}


MetricsSystem::MetricsSystem():
  layerId(MT_LAYER),
  visible(false),
  nextUpdate(MetricsUpdate + 1)
{

}

MetricsSystem::~MetricsSystem()
{
  
}

void
MetricsSystem::configure()
{
  system_name = "Metrics";
  
  if (visible)
    createLayer();

  subscribe<ToggleVisibilityMessage>
    ([this](ToggleVisibilityMessage &msg){
       
       if (msg.getElementName() == "Metrics")
	 {
	   visible = !visible;
	   if (!visible)
	     gui->delete_layer(layerId);
	   else
	     createLayer();
	 }
     });

}

void
MetricsSystem::update(double duration_ms)
{
  if (!visible)
    return;

  
  term(layerId)->clear(vchar{' ', WHITE, DARKEST_SEPIA});
      
  if (nextUpdate > MetricsUpdate)
    {
      dump = ecs_profile_dump();
      nextUpdate = 0;
    }
  else
    {
      nextUpdate += duration_ms;
    }


  int x = 10, y = 3;
  for (auto& c: dump)
    if (c == 10) // Newline
      {
	x = 10;
	++y;
      }
    else
      {
	term(layerId)->set_char( x ,y , vchar(c, WHITE, DARKEST_SEPIA));
	++x;
      }
    

}

void
MetricsSystem::createLayer()
{

  auto s = get_window()->getSize();

   int w = 800;
   int h = 500;
   
   int x = (s.x - w)/2;
   int y = 0;

  gui->add_layer(layerId, x, y, w, h, "8x16", resize_metrics);
}
