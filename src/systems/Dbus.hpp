#ifndef __DEBUS_SYSTEM_HPP__
#define __DEBUS_SYSTEM_HPP__

#include "rltk.hpp"

/** A system dedicated to remote playing through DBUS (or a twitch bot...)
  *
  * This system in not designed to be started by default. Please see
  * command line arguments (--help) for more
  *
  */
class DbusSystem : public rltk::base_system
{
 public:
  virtual void configure() override;
  virtual void update(const double duration_ms) override;
};


#endif // !__DEBUS_SYSTEM_HPP__

