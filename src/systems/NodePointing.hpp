#ifndef _NODE_POINTING_SYSTEM_HPP_
#define _NODE_POINTING_SYSTEM_HPP_

#include "rltk.hpp"

using namespace rltk;

class NodePointingSystem : public base_system {
public:
  virtual void configure() override;

  virtual void update(const double duration_ms) override;

protected:
  bool outOfBounds(int x, int y);

  // Keep main layer's font metrics to point the real char
  int fontWidth;
  int fontHeight;
};

#endif // !_NODE_POINTING_SYSTEM_HPP_
