#ifndef _ENEMY_INFO_SYSTEM_HPP_
#define _ENEMY_INFO_SYSTEM_HPP_

#include "rltk.hpp"

#include <map>

using namespace rltk;

// Forward declarations
class Entity;
// End of forward declarations


/** Handle VBar of health for one or multiple enemies */
class EnemyInfoSystem : public base_system
{
public:
  EnemyInfoSystem();
  virtual ~EnemyInfoSystem();

  virtual void configure() override;
  virtual void update(double duration_ms) override;

  void addTracking(Entity*);
  
protected:
  void createLayer(void);
  void updateControls();
  void setVisible(bool);
  
private:
  int layerId;
  layer_t* retained;

  int                    nextTrackingId;  //!< The next ID used to create vbar
  std::map<Entity*, int> trackedEntities;
  bool visible;
  
};

#endif // !_ENEMY_INFO_SYSTEM_HPP_
