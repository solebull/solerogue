#include "ActorRender.hpp"

#include "Renderable.hpp"
#include "Position.hpp"

#include "Layers.hpp"

#include "MapManager.hpp"

#include "systems/Camera.hpp" // Uses visible()

void ActorRenderSystem::configure()
{
  system_name = "Actor Render System";
}

void ActorRenderSystem::update(const double duration_ms)
{
  each<Position, Renderable>([] (entity_t &entity, Position &pos,
				 Renderable &render)
     {
       if (MapManager::getInstance().getCurrentMap()
	   ->getVisible(pos.getX(), pos.getY()))
	 {
	   term(MAIN_LAYER)->set_char(pos.getX()-left_x,
				      pos.getY()-top_y, vchar{
	       render.glyph, render.fg, render.bg });
	 }
     });
}

