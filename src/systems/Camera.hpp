#ifndef _CAMERA_SYSTEM_HPP_
#define _CAMERA_SYSTEM_HPP_

#include "rltk.hpp"


#include "api/Map.hpp"
#include "api/VcharBuffer.hpp"

#include "messages/OutlineTile.hpp"

using namespace rltk;

extern int left_x, right_x, top_y, bottom_y;

class CameraSystem : public base_system
{
 public:
  virtual void configure() override;
  virtual void update(const double duration_ms) override;

protected:
  void drawEntity(Entity*, Map*);
  int getWall(int, int);
  
private:
  VcharBuffer buffer;          //!< VcharBuffer instance
  OutlineTileMessage outline;  //!< Simply keep the outline configuration
};

#endif // !_CAMERA_SYSTEM_HPP_
