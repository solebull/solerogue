#ifndef __MENU_SYSTEM_HPP__
#define __MENU_SYSTEM_HPP__

#include "rltk.hpp"

/** A Menu system based on a cursor and message */
class MenuSystem : public rltk::base_system
{
public:
  MenuSystem();

  virtual void configure() override;
  
  virtual void cursorChanged()=0;
  virtual void action()=0;
  
protected:
  int cursor;
};


#endif // !__MENU_SYSTEM_HPP__
