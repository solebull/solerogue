#include "PickupMenu.hpp"

#include "messages/PlayerMoved.hpp"
#include "components/Inventory.hpp"

#include "config.h"       // USES _()
#include "Layers.hpp"
#include "Position.hpp"

#include "api/Entity.hpp"
#include "api/PlayerId.hpp" // USES player_id

#include "systems/Camera.hpp" // USES player_id

#include "MapManager.hpp"
#include "i18n.hpp"

#include <iostream>

using namespace std;
using namespace rltk;
using namespace rltk::colors;

const color_t BG = SEPIA;

void
resize_pickup(layer_t * l, int w, int h)
{
  l->w = 200;
  l->h = 200;
   
  l->x = 10;
  l->y = h - 120 - 200;
}


PickupMenuSystem::PickupMenuSystem():
  MenuSystem(),
  layerId(PUM_LAYER),
  visible(false)
{
  
}

PickupMenuSystem::~PickupMenuSystem()
{

}

void
PickupMenuSystem::configure()
{
  MenuSystem::configure();
  system_name = "Pickup menu";

  
  if (visible)
    createLayer();
  
  subscribe<PlayerMovedMessage>([this](PlayerMovedMessage &msg)
     {
       auto loc = entity(player_id)->component<Position>();
       auto es= MapManager::getInstance().getCurrentMap()->getEntities();

       int objects = 0;
       for (auto& e : es)
	 {
	   if (e->getX() == loc->getX() && e->getY() == loc->getY())
	     {
	       setVisible(true);
	       ++objects;
	     }

	 }

       setVisible(objects > 0);
     });

}

void
PickupMenuSystem::update(double duration_ms)
{
  if (!visible)
    return;

  term(layerId)->clear(vchar{' ', WHITE, BG});
  term(layerId)->print_center( 0, _("Pickup menu"), YELLOW, BG);

  auto loc = entity(player_id)->component<Position>();
  auto es= MapManager::getInstance().getCurrentMap()->getEntities();

  int y = 0;
  for (auto& e : es)
    if (e->getX() == loc->getX() && e->getY() == loc->getY())
      {
	color_t fg = White;
	color_t bg = BG;
	if (y == cursor)
	  {
	    bg = fg;
	    fg = BG;
	  }
	
	term(layerId)->print( 2 ,y +5, e->getName(), fg, bg);
	++y;
      }

  term(layerId)->print( 3 ,11, _("<Space> to pickup"), YELLOW, BG);

}

void
PickupMenuSystem::createLayer(void)
{
  cursor = 0;
  auto s = get_window()->getSize();

  int w = 200;
  int h = 200;
   
  int x = 10;
  int y = s.y - 120 - 200;
  
  gui->add_layer(layerId, x, y, w, h, "8x16", resize_pickup);
  term(layerId)->set_alpha(196);
}

void
PickupMenuSystem::setVisible(bool vB)
{
  if (vB)
    {
      if (!visible)
	createLayer();
    }
  else
    {
      if (visible)
	{
	  gui->delete_layer(layerId);
	}
    }
      
  visible = vB;
}

void
PickupMenuSystem::cursorChanged(void)
{
  auto loc = entity(player_id)->component<Position>();
  auto es= MapManager::getInstance().getCurrentMap()->getEntities();

  int s = 0;
  for (auto& e : es)
    if (e->getX() == loc->getX() && e->getY() == loc->getY())
      ++s;

  if (cursor > s - 1)
    cursor = 0;
  if (cursor < 0)
    cursor =  s - 1;
}

void
PickupMenuSystem::action(void)
{
  auto loc = entity(player_id)->component<Position>();
  auto es= MapManager::getInstance().getCurrentMap()->getEntities();

  int s = 0;
  for (auto& e : es)
    if (e->getX() == loc->getX() && e->getY() == loc->getY())
      {
	if (s == cursor)
	  {
	    MapManager::getInstance().getCurrentMap()->remove(e);
	    entity(player_id)->component<Inventory>()->add(e);
	    if (es.size() == 1)
	      setVisible(false);
	    return;
	  }
	++s;
      }
}

