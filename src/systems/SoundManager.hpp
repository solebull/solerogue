#ifndef _SOUND_MANAGER_HPP_
#define _SOUND_MANAGER_HPP_

#include <SFML/Audio.hpp>

#include <string>
#include <vector>

#include "rltk.hpp"

struct Sfx {
  sf::SoundBuffer* buffer;
  sf::Sound*       sound;
};

/** Is responsible for playing both music and SFX.
  *
  *
  *
  */
class SoundManager : public rltk::base_system
{
public:
  SoundManager();
  ~SoundManager();

  virtual void configure(void);
  virtual void update(const double duration_ms);

  int getMood(void);

  void incMood(int);
  void decMood(int);  

  void playSfx(const std::string&, float pitch=1.0f, float volume=80.0f);

protected:
  void playMusic(const std::string&);
  void dumpVector(const std::string&, const std::vector<std::string>&);
  
private:
  int mood;                 //!< Value 0 < mood < 100. 50 is a mediant value.

  sf::Music music;                        //!< SFML object used to play music
  std::vector<std::string> ambianceFiles; //!< List of found ambiance music
  std::vector<std::string> actionFiles; //!< List of found action music
  std::vector<std::string> sorrowFiles; //!< List of found sorrow music

  std::vector<Sfx*> currentSfx;  //!< Currently playing Sfx
};


#endif // !_SOUND_MANAGER_HPP_
