#ifndef _HUD_HPP_
#define _HUD_HPP_

/** \file Hud.hpp
  * The Hud system class header.
  *
  */

#include "rltk.hpp"

using namespace rltk;

/** A special layer showing player's health and mana 
  *
  * Note: To print the list with scrolling, use list::rbegin()
  *
  */
class Hud : public base_system
{
public:
  Hud();
  virtual ~Hud();

  virtual void configure() override;
  virtual void update(double duration_ms) override;

protected:
  void updateControls();
  
private:
  int layerId;             //!< The unique rltk's layer identifier
  layer_t* retained;       //!< A pointer to a retained (owned) layer
  /** Be sure we call ToggleVisibility only once for HighScores
    *
    * Fix a bug when the HighScore layer didn't show when killed by a 
    * entity. In fact, it received twice the ToggleVisibility message and
    * hide again.
    *
    */
  bool gameOverEmitted;    
};

#endif // !_HUD_HPP_
