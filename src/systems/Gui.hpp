#ifndef __GUI_SYSTEM_HPP__
#define __GUI_SYSTEM_HPP__

#include "Menu.hpp"

#include "ui/Log.hpp"
#include "ui/Info.hpp"
#include "ui/HelpViewer.hpp"

class GuiSystem : public MenuSystem
{
public:
  GuiSystem();
  virtual ~GuiSystem();

  virtual void configure() override;
  virtual void update(double duration_ms) override;

  virtual void cursorChanged() override;
  virtual void action() override;
  
  static void display_hook(void);
  static bool resize_hook(unsigned int, unsigned int);

  static bool inject_wheel(unsigned int, unsigned int, float);

private:
  LogUi  log;
  InfoUi info;
};
  
#endif // !__GUI_SYSTEM_HPP__
