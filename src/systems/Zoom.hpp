#ifndef _ZOOM_SYSTEM_HPP_
#define _ZOOM_SYSTEM_HPP_

#include <string>

#include "rltk.hpp"

using namespace rltk;
using namespace std;

int  font_weight(const string&);
bool sort_font(const string& s1, const string& s2);

/** The Zoom system
  *
  * Subscribe to zoom messages then compute the next resolution and
  * reload main layer.
  *
  */
class ZoomSystem : public base_system
{
public:
  static void setAssetsDir(const string&);

  ZoomSystem();
  
  virtual void configure() override;
  virtual void update(const double duration_ms) override;

  void                  addFont(const string&);
  const vector<string>& getFontVector(void)const;

protected:
  void sortFontVector(void);

  void previous(void);
  void next(void);
  
private:
  /** The assets directory we take the resolution list from
    *
    * The assets dir must also be passed to rltk's init() function.
    *
    *
    */
  static std::string       assets_dir;
  std::vector<std::string> fonts;                   //!< The fonts container
  std::vector<std::string>::size_type current_font; //!< the current font index
};

#endif // !_ZOOM_SYSTEM_HPP_
