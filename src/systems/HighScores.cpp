#include "HighScores.hpp"

#include "Layers.hpp"
#include "rltk.hpp"
#include "Score.hpp"

#include "MapManager.hpp"

#include "config.h" // USES _()

#include "messages/ToggleVisibility.hpp"

#include "systems/Player.hpp"

#include "api/PlayerId.hpp" // USES playerId
#include "api/Attributes.hpp"

#include "api/Logger.hpp"

#include "HighScoresFormatter.hpp"
#include "formatters/highscores/Text.hpp"
#include "formatters/highscores/Json.hpp"

using namespace rltk;
using namespace rltk::colors;

#define CL Tomato, DESATURATED_GREEN

// Highlighted
#define HL Thistle, DESATURATED_GREEN

// Header
#define CH FUCHSIA, DESATURATED_GREEN 

// Update this version when the serialization format change
//// Version 3: Add general level

// This is called when the screen resizes, to allow the GUI to redefine itself.
void resize_highscores(layer_t * l, int w, int h) {
  // Simply set the width to the whole window width
  l->w = 1000;
  l->h = 400;

  l->x = (w - l->w)/2;
  l->y = (h - l->h)/2;
}


HighScoresSystem::HighScoresSystem():
  layerId(HS_LAYER),
  visible(false)
  //  version(HIGHSCORE_VERSION)
{
  load();
}

void
HighScoresSystem::createLayer()
{
   auto s = get_window()->getSize();

   int w = 1000;
   int h = 400;
   
   int x = (s.x - w)/2;
   int y = (s.y - h)/2;

  gui->add_layer(layerId, x, y, w, h, "8x16", resize_highscores);
}


void
HighScoresSystem::configure()
{
  system_name = "HighScores";
  if (visible)
    createLayer();
  
  sort();

  /*
  subscribe<ToggleVisibilityMessage>
    ([this](ToggleVisibilityMessage &msg){

       if (msg.getElementName() == "HighScores")
	 {
	   auto att = entity(player_id)->component<Attributes>();
	   int health = att->getInt("health");
	   if (health < 1)
	     {
	       add();
	       if (visible) return;
	     }

	   // Just show the new one!
	   if (visible)
	     visible= !visible;
	   
	   if (!visible)
	     gui->delete_layer(layerId);
	 }
     });
  */
}

HighScoresSystem::~HighScoresSystem()
{
}
  
void
HighScoresSystem::update(double duration_ms)
{
  if (!visible)
    return;

  term(layerId)->clear(vchar{' ', YELLOW, DESATURATED_GREEN});
  term(layerId)->box(DARKEST_GREEN, DESATURATED_GREEN);
  term(layerId)->print_center(0, _("| HIGH SCORES |"), Tomato, DESATURATED_GREEN);
  if (PlayerSystem::isAlive())
    term(layerId)->print_center(24, _("| F6 to toggle visibility |"),
				Tomato, DESATURATED_GREEN);

  int y = 2;

  term(layerId)->print(22 ,y , _("Name"), CH);
  term(layerId)->print(45 ,y , _("Lev."), CH);
  term(layerId)->print(50 ,y , _("Kill by"), CH);
  term(layerId)->print(60 ,y , _("Score"), CH);
  term(layerId)->print(68 ,y , _("Moves"), CH);
  term(layerId)->print(75 ,y , _("Time"), CH);
  term(layerId)->print(82 ,y , _("Dungeon #"), CH);
  term(layerId)->print(95 ,y , _("Date/Time of play"), CH);

  ++y;
  int idx = 1;
  for (auto const& it : scores)
    {
      char buf[80];
      time_t now = it.getEpoch();
      struct tm ts = *localtime(&now);
      strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M", &ts);

      string kb = it.getKilledBy();
      kb[0] = toupper(kb[0]);
      
      if (it.isCurrentGame())
	{
	  term(layerId)->print( 1 ,y , ">", HL);
	  term(layerId)->print( 2 ,y , to_string(idx), HL);
	  term(layerId)->print( 5 ,y , it.name, HL);

	  term(layerId)->print(45 ,y , to_string(it.getLevel()), HL);
	  term(layerId)->print(50 ,y , kb, HL);
	  term(layerId)->print(60 ,y , to_string(it.score), HL);
	  term(layerId)->print(68 ,y , to_string(it.movesNb), HL);
	  term(layerId)->print(75 ,y , it.getPlayedTime(), HL);
	  term(layerId)->print(85 ,y , to_string(it.maxDungeonDepth), HL);
	  term(layerId)->print(95 ,y , buf, HL);
	  term(layerId)->print(111 ,y , "<", HL);
	}
      else
	{
	  term(layerId)->print( 5 ,y , it.name, CL);
	  term(layerId)->print( 2 ,y , to_string(idx), CL);
	  term(layerId)->print(45 ,y , to_string(it.getLevel()), CL);
	  term(layerId)->print(50 ,y , kb, CL);
	  term(layerId)->print(60 ,y , to_string(it.score), CL);
	  term(layerId)->print(68 ,y , to_string(it.movesNb), CL);
	  term(layerId)->print(75 ,y , it.getPlayedTime(), CL);
	  term(layerId)->print(85 ,y , to_string(it.maxDungeonDepth), CL);
	  term(layerId)->print(95 ,y , buf, CL);
	}
      ++y;
      ++idx;
      // Limit to 20 first high scores
      if (idx > 20) break;
    }
}

void
HighScoresSystem::sort()
{
  //  scores.sort(compare_scores);
}

/** Add our Player to the score table
  *
  * Should be called only once when player die.
  *
  */
void
HighScoresSystem::add()
{
  LOG(LogLevel::Debug, "HighScoresSystem::add called for %s", playerName);
  auto sc = entity(player_id)->component<Score>(); 
  auto att = entity(player_id)->component<Attributes>();
  
  HighScoresEntry s;
  s.name       = playerName + " " + playerTitle;
  s.score      = sc->compute();
  s.setPlayedTime(sc->playedTime);
  s.movesNb    = sc->nbMoves;
  s.setCurrentGame(true);
  s.maxDungeonDepth = MapManager::getInstance().getDeeperDungeon();
  s.setKilledBy(sc->getKilledBy());
  s.setLevel(att->getLevel("general").getLevel());

  LOG(LogLevel::Debug, "PlayedTime is '%s'. Moves : %d",
      s.getPlayedTime().c_str(), s.movesNb);
  
  scores.push_back(s);
  sort();
  save();
}

void
HighScoresSystem::serialize(struct gzip_file& g)
{
}

void
HighScoresSystem::deserialize(struct gzip_file& g)
{
}

void
HighScoresSystem::save()
{
}

void
HighScoresSystem::load()
{
}

/** Standalone print of highscore */
void
HighScoresSystem::print(const std::string& vFormat)
{
}
