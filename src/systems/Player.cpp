#include "Player.hpp"

#include "Position.hpp"
#include "Score.hpp"

#include "Layers.hpp"
#include "Morgue.hpp"
#include "Score.hpp"
#include "MapManager.hpp"
#include "ScriptManager.hpp"

#include "api/Attributes.hpp"
#include "api/Entity.hpp"

#include "messages/ActorMoved.hpp"
#include "messages/GameOver.hpp"
#include "messages/PlayerMoved.hpp"
#include "messages/AttributeModifier.hpp"
#include "messages/ToggleVisibility.hpp"
#include "messages/AddLogEntry.hpp"

#include "messages/Zoom.hpp"
#include "messages/CursorUp.hpp"
#include "messages/CursorDown.hpp"
#include "messages/CursorLeft.hpp"
#include "messages/CursorRight.hpp"
#include "messages/CursorAction.hpp"

#include "components/Character.hpp"


#include "systems/Camera.hpp" // USES player_id

#include "config.h" // _()

#include <sstream>
#include <iostream>
#include <cstdlib>
#include <cassert>


#include "namegen.h"

using namespace std;

// Total number of moved turn
int nb_moves = 0;

string playerName;
string playerTitle;
/** Do not try to use this directly : use PlayerSystem::isAlive() instead 
  * because it also handles negative health.
  *
  */
static bool gameover = false;

/** Generate and set a random name with a title
  *
  * Modify playerName and playerTitle only if none passed to program
  * arguments.
  *
  */
void
generate_player_name()
{
  if (playerTitle.empty())
    {
      cout << _("You can't play with an empty name. I generated one for you :")
	   << endl;
      // From https://en.wikipedia.org/wiki/Imperial,_royal_and_noble_ranks
      vector<string> ranks = { _("King of"), _("Prince of"),
			       _("Duke of"), _("Count of"),
			       _("Baron of"), _("Lord of"),
			       _("Earl of"),  _("Knight of")};

      int idx = std::rand() % ranks.size();
      assert( (idx != (int)ranks.size())  && "Index is vector size");
      
      string rank = ranks[idx];
      NameGen::Generator tgen("sV'i");
      string state = tgen.toString();
      state[0] = toupper(state[0]);
      
      playerTitle = rank + " " + state ;
      cout <<playerName << ' ' <<  playerTitle << endl;
    }
  
  if (playerName.empty())
    {
      NameGen::Generator generator("sVi");
      playerName = generator.toString();
      playerName[0] = toupper(playerName[0]);

    }

  cout << _("Name") << " : " << playerName << endl;
  cout << _("Title") << " : '" << playerTitle << "'" << endl;
  
  cout << _("Press a key to continue") << endl;
  cin.get();
}

void PlayerSystem::configure()
{
  system_name = "Player System";
  subscribe<ActorMovedMessage>
    ([this](ActorMovedMessage &msg){
       int node = MapManager::getInstance().getCurrentMap()->
	 getTile(msg.destination_x, msg.destination_y);

       // Player moves
       if (node == 1)
	 {
	   emit(AttributeModifierMessage("health", -1));
	   auto sc = entity(player_id)->component<Score>();
	   sc->setKilledBy(_("a wall"));
	 }
       else if (auto e = notWalkable(msg.destination_x, msg.destination_y))
	 {
	   // Hit
	   LOG(LogLevel::Info, "Attacking a %s", e->getName().c_str());
	   auto ch = entity(player_id)->component<Character>();
	   int d = 0;
	   if (ch->getCurrentHand() == CH_LEFT)
	     d = ch->getLHWeapon()->getDamage();
	   else
	     d = ch->getRHWeapon()->getDamage();
	     
	   if (e->attack(d))
	     {
	       LOG(LogLevel::Info, "%s is DEAD! ", e->getName().c_str());
	       auto m = MapManager::getInstance().getCurrentMap();
	       m->remove(e);
	       e->generateLoot(m);
	       auto sc = entity(player_id)->component<Score>();
	       sc->addKill(e->getSpecies());
	       auto& genlev = entity(player_id)->component<Attributes>()
		 ->getLevel("general");
	       if (genlev.inc(e->getExpWhenKilled()))
		 {
		   // Just advanced level, add log message...
		   char msg[80];
		   sprintf(msg,
			   _("CONGRATS: You advanced to level %d, next will be at %d exp.").c_str(),
			   genlev.getLevel(), genlev.getNext());
		   emit(AddLogEntryMessage(msg));

		   // Add a bonus score
		   char msg2[60];
		   sprintf(msg2, _("General level %d advance bonus.").c_str(),
			   genlev.getLevel());
		   auto sc = entity(player_id)->component<Score>();
		   sc->addBonus(msg2, genlev.getLevel() * 10);
		 }
	     }
	   // Hit back
	   auto w = e->getRandomWeapon();
	   if (w)
	     attack(e, w);
	   
	   char msg[80];
	   sprintf(msg, "You hit %s with %d damage", e->getName().c_str(), d);
	   emit(AddLogEntryMessage(msg));
	   emit(AttributeModifierMessage("health", 0 - e->getDeathByValue()));
	   auto sc = entity(player_id)->component<Score>();
	   sc->setKilledBy("A " + e->getDeathBy());
	 }
       else
	 {
	   msg.mover->component<Position>()->setX(msg.destination_x);
	   msg.mover->component<Position>()->setY(msg.destination_y);
	   msg.mover->component<Position>()->bounds_check();
	   ++nb_moves;
	 }

       // Entity moves
       ScriptManager::getInstance().
	 call_OnMove(MapManager::getInstance().getCurrentMap(),
		     entity(player_id));
       term(MAIN_LAYER)->dirty = true;
       emit(PlayerMovedMessage{});

     });

  subscribe<GameOverMessage>
    ([this](GameOverMessage &msg){
       LOG(LogLevel::Info, "Player received a GameOverMessage");
       gameover = true;
       emit(ToggleVisibilityMessage("HighScores"));
       Morgue m;
     });

  subscribe<CursorUpMessage>
    ([this](CursorUpMessage &msg){
       entity(player_id)->component<Character>()->changeCurrentHand();
     });

  subscribe<CursorDownMessage>
    ([this](CursorDownMessage &msg){
       entity(player_id)->component<Character>()->changeCurrentHand();
     });

  
  subscribe_mbox<key_pressed_t>();
}
  
void PlayerSystem::update(const double duration_ms)
{
  if (!PlayerSystem::isAlive())
    return;
  
  auto camera_loc = entity(player_id)->component<Position>();
  
  // Loop through the keyboard input list
  std::queue<key_pressed_t> * messages = mbox<key_pressed_t>();
  while (!messages->empty()) {
    key_pressed_t e = messages->front();
    messages->pop();
    
    if (e.event.key.code == sf::Keyboard::Left)
      emit(ActorMovedMessage( entity(player_id), camera_loc->getX(), camera_loc->getY(),
			      camera_loc->getX() - 1, camera_loc->getY() ));
    if (e.event.key.code == sf::Keyboard::Right)
      emit(ActorMovedMessage( entity(player_id), camera_loc->getX(), camera_loc->getY(),
			      camera_loc->getX() + 1, camera_loc->getY() ));
    if (e.event.key.code == sf::Keyboard::Up)
      emit(ActorMovedMessage( entity(player_id), camera_loc->getX(), camera_loc->getY(),
			      camera_loc->getX(), camera_loc->getY()-1 ));
    if (e.event.key.code == sf::Keyboard::Down)
      emit(ActorMovedMessage( entity(player_id), camera_loc->getX(), camera_loc->getY(),
			      camera_loc->getX(), camera_loc->getY()+1 ));
    if (e.event.key.code == sf::Keyboard::F1)
      emit(ToggleVisibilityMessage("Help"));
    if (e.event.key.code == sf::Keyboard::F2)
      emit(ToggleVisibilityMessage("Metrics"));
    if (e.event.key.code == sf::Keyboard::F6)
      emit(ToggleVisibilityMessage("HighScores"));
    if (e.event.key.code == sf::Keyboard::I)
      emit(ToggleVisibilityMessage("Inventory"));
    if (e.event.key.code == sf::Keyboard::A)
      emit(CursorUpMessage{});
    if (e.event.key.code == sf::Keyboard::C)
      emit(ToggleVisibilityMessage("Character"));
    if (e.event.key.code == sf::Keyboard::Q)
      emit(CursorDownMessage{});
    if (e.event.key.code == sf::Keyboard::W)
      emit(CursorLeftMessage{});

    // Zoom handling
    if (e.event.key.code == sf::Keyboard::Add)
      emit(ZoomInMessage{});
    if (e.event.key.code == sf::Keyboard::Subtract)
      emit(ZoomOutMessage{});
    
    if (e.event.key.code == sf::Keyboard::X)
      emit(CursorRightMessage{});
    if (e.event.key.code == sf::Keyboard::Space)
      emit(CursorActionMessage{});
    if (e.event.key.code == sf::Keyboard::Enter)
      {
	// Trying to go up and down
	auto loc = entity(player_id)->component<Position>();
	int node = MapManager::getInstance().getCurrentMap()->
	  getTile(loc->getX(), loc->getY());
	
	int actualLevel = MapManager::getInstance().getDungeonLevel();
	ActorMovedDirection dir;
	if (node == 4 || node == 2)
	  dir = DOWN;

	else if (node == 3) // Up stair
	  {
	    dir = UP;
	    if (actualLevel == 1)
	      {
		// Early exit
		auto sc = entity(player_id)->component<Score>();
		sc->addBonus(_("Early exit"), -30);
		emit(GameOverMessage());
	      }
	  }
	else
	  return;

	    MapManager::getInstance().go(dir);
	    emit(ActorMovedMessage( entity(player_id), loc->getX(), loc->getY(),
				    loc->getX(), loc->getY(), dir ));
      }

    // Other value (-1) include < > 
    if (e.event.key.code == -1)
      {
	LOG(LogLevel::Debug, "Key code: %d", e.event.key.code);
	LOG(LogLevel::Debug, "text code: %d", e.event.text.unicode);
      }
  }
}

bool
PlayerSystem::isAlive(void)
{
  if (gameover)
    return false;
  
  auto att = entity(player_id)->component<Attributes>();
  return (att->getInt("health") > 0);
}

/** Return an pointer to an entity if not walkable at this position */
Entity*
PlayerSystem::notWalkable(int x, int y) const
{
   auto entities = MapManager::getInstance().getCurrentMap()->getEntities();
   for (auto e : entities)
     if (x == e->getX() && y == e->getY() && !e->isWalkable())
       return e;

   return nullptr;
}

void
PlayerSystem::attack(Entity* e, Weapon* w)
{
  int d = w->getDamage();
  
  emit(AttributeModifierMessage("health", 0 - d));
  char m[120];
  sprintf(m, _("%s hurts you with its %s : %s - %d damage").c_str(),
	  e->getName().c_str(), w->getName().c_str(),
	  w->getInjuryName().c_str(), d);
  emit(AddLogEntryMessage(m));
}
