#include "Menu.hpp"

#include "messages/CursorUp.hpp"
#include "messages/CursorDown.hpp"
#include "messages/CursorAction.hpp"


using namespace rltk;

MenuSystem::MenuSystem():
base_system(),
  cursor(0)
{

}

void
MenuSystem::configure()
{
  subscribe<CursorUpMessage>
    ([this](CursorUpMessage &msg){
       --cursor;
       cursorChanged();
     });

  subscribe<CursorDownMessage>
    ([this](CursorDownMessage &msg){
       ++cursor;
       cursorChanged();
     });

  subscribe<CursorActionMessage>
    ([this](CursorActionMessage &msg){
       action();
     });
}
  
