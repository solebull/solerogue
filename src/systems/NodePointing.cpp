#include "NodePointing.hpp"

#include <iostream>

#include "Position.hpp"
#include "Layers.hpp"

#include "messages/OutlineTile.hpp"
#include "messages/ChangeMainFont.hpp"
#include "messages/ChangeNodeInfo.hpp"
#include "messages/ChangeObjectsInfo.hpp"

#include "MapManager.hpp"

#include "systems/Camera.hpp" // USES left_x etc...

#include "api/PlayerId.hpp" // USES player_id

#include "config.h"    // USES _()


using namespace std;
using namespace rltk::colors;

void
NodePointingSystem::configure()
{
  system_name = "Node Pointing";

  subscribe<ChangeMainFontMessage>
    ([this](ChangeMainFontMessage &msg){
       fontWidth  = msg.getFontWidth();
       fontHeight = msg.getFontHeight();
     });
}

void
NodePointingSystem::update(const double duration_ms)
{

  int mouse_x, mouse_y;
  const int titleHeight=  16; // 16 is the title layer's height
  std::tie(mouse_x, mouse_y) = get_mouse_position();

  // We're using a 16x16 terminal
  const int terminal_x = mouse_x / fontWidth;
  const int terminal_y = (mouse_y - titleHeight) / fontHeight;

  int mx = terminal_x + left_x;
  int my = terminal_y + top_y ;

  // Define to 1 in config.h.in to enable
#if DEBUG_NODE_POINTING
  emit(OutlineTileMessage(true, mx, my));
  // Too much terminal load, reactivate to debug
  /*  const int mdev_x = (int)(mouse_x) / fontWidth;
  const int mdev_y = (int)(mouse_y ) / fontHeight;
  LOG(LogLevel::Debug, "NodePointing center updated to '%dx%d", mdev_x, mdev_y);
  */
#endif
  
  auto camera_loc = entity(player_id)->component<Position>();

  string msg;

  if (outOfBounds(mx, my))
    {
      msg = "";
      emit(ChangeNodeInfoMessage(msg));
      return;	
    }
  
  if (!MapManager::getInstance().getCurrentMap()->getRevealed(mx, my))
    {
      msg = _("Unrevelated");
    }
  else
    {
      if (mx == camera_loc->getX() && my == camera_loc->getY())
	{
	  msg = _("YOU!");
      }
      else
	{
	  switch (MapManager::getInstance().getCurrentMap()->getTile(mx, my))
	    {
	    case 0 : msg = _("Floor"); break;
	    case 1 : msg = _("Wall"); break;
	    case 2 : msg = _("Down exit (continue)"); break;
	    case 3 : msg = _("Up exit (you coward!)"); break;
	    case 4 : msg = _("Escape hatch (Can't go back)"); break;
	    default : msg = ""; // This shouldn't happen
	    }
	}
    }

  // Change Info layer infos
  emit(ChangeNodeInfoMessage(msg));
  emit(ChangeObjectsInfoMessage(mx, my));
}

bool
NodePointingSystem::outOfBounds(int x, int y)
{
  if (x < 0) return true;
  if (x > (int)MapManager::getInstance().getCurrentMap()->getWidth())
    return true;
  if (y < 0) return true;
  if (y > (int)MapManager::getInstance().getCurrentMap()->getHeight())
    return true;

  return false;
}
