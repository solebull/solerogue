#include "PlayedTime.hpp"

#include <sstream>
#include <iomanip>

#include "systems/Player.hpp"

PlayedTime playedTime;

PlayedTime::PlayedTime():
  lastTime("")
{
  ela_start = chrono::system_clock::now();

}

string PlayedTime::str()
{
  if (PlayerSystem::isAlive())
    {
      auto ela_end = chrono::system_clock::now();
      lastTime = durationToString(ela_end - ela_start);
      return lastTime;
    }
  else
    return lastTime;
}

string
PlayedTime::durationToString(chrono::duration<double> d) const
{
  auto x = chrono::duration_cast<chrono::seconds>(d);
  auto sec = x.count();
  
  // Compute time
  auto hour = sec / 3600;
  auto rem = sec % 3600;
  auto min = rem / 60;
  
  sec = rem % 60;

  // Print time
  ostringstream oss2;
  oss2 << std::setw(2)<< std::setfill('0') << to_string(hour)
       << ':'
       << std::setw(2)<< std::setfill('0') << to_string(min)
       << ':'
       << std::setw(2)<< std::setfill('0') << to_string(sec);

  return oss2.str();
}
