# Solerogue Documentation

A C++ rogue project based on rltk library.

The project is based on some modern-C++ concepts :
- systems/    rltk base class with auto update callback;
- messages/   can be subscribe to by any rltk system;
- Entity      A scriptable and moddable in-game object/mob.
