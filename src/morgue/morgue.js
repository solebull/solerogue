
/** JS */

function zoomin(preid)
{
    var el = document.getElementById(preid);
    var style = window.getComputedStyle(el, null).getPropertyValue('font-size');
    var fs = parseFloat(style);
    document.getElementById(preid).style.fontSize = (fs + 1) + 'px';
}

function zoomout(preid)
{
    var el = document.getElementById(preid);
    var style = window.getComputedStyle(el, null).getPropertyValue('font-size');
    var fs = parseFloat(style);
    document.getElementById(preid).style.fontSize = (fs - 1) + 'px';
}
