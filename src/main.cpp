/* RLTK (RogueLike Tool Kit) 1.00
 * Copyright (c) 2016-Present, Bracket Productions.
 * Licensed under the MIT license - see LICENSE file.
 *
 * Example 10: Not really an example yet, playing with getting the ECS working.
 */

// You need to include the RLTK header
#include "rltk.hpp"

#include "config.h"

#include "MapManager.hpp"
#include "Position.hpp"
#include "Renderable.hpp"
#include "NavigatorHelper.hpp"
#include "Layers.hpp"
#include "PlayedTime.hpp"
#include "ScriptManager.hpp"

#include "layers/Layer.hpp"
#include "layers/TitleLayer.hpp"

#include "ui/HelpViewer.hpp"

#include "systems/ActorRender.hpp"
#include "systems/Hud.hpp"
#include "systems/Gui.hpp"
#include "systems/EnemyInfo.hpp"
#include "systems/Metrics.hpp"
#include "systems/NodePointing.hpp"
#include "systems/Camera.hpp"
#include "systems/Inventory.hpp"
#include "systems/PickupMenu.hpp"
#include "systems/Player.hpp"
#include "systems/SoundManager.hpp"
#include "systems/Visibility.hpp"
#include "systems/Zoom.hpp"

#include "ui/Highscores.hpp"

#include "messages/PlayerMoved.hpp"

#include "api/Logger.hpp"

#include <boost/program_options.hpp>

// We're using a stringstream to build the hello world message.
#include <algorithm>
#include <cstdlib>

// Getting current directory
#ifdef _WIN32
#  include <windows.h>
#  include <direct.h>
#  define GetCurrentDir _getcwd
#else
#include <unistd.h>
#  define GetCurrentDir getcwd
#endif

constexpr const char* FONT= "/usr/share/fonts/truetype/freefont/FreeMono.ttf";

char buff[FILENAME_MAX];
char* cwd;

/** Return the current working dir
  *
  */
const char*
get_cwd( void ) {
  cwd = GetCurrentDir( buff, FILENAME_MAX );
  printf("Retuning path CWD : %s\n", cwd);
  return cwd;
}

// For convenience, import the whole rltk namespace. You may not want to do this
// in larger projects, to avoid naming collisions.
using namespace rltk;
using namespace rltk::colors;
using std::size_t;

using namespace std;
namespace po = boost::program_options;


list<Layer*> layerList;

// Tick is called every frame. The parameter specifies how many ms have elapsed
// since the last time it was called.
void tick(double duration_ms)
{
  
  for (Layer* l: layerList)
    l->update(duration_ms);

  ecs_tick(duration_ms);
  
}


/** Handle graphics event before RLTK (dialogs)
  *
  */
void
display_hook()
{
  GuiSystem::display_hook();
}

/** Override RLTK event with SFML ones
  *
  */
bool
event_hook(sf::Event event)
{
   if (event.type == sf::Event::Resized)
     return GuiSystem::resize_hook(event.size.width, event.size.height);

   if (event.type == sf::Event::MouseWheelMoved)
     {
       return GuiSystem::inject_wheel(event.mouseWheel.x,
				      event.mouseWheel.y,
				      event.mouseWheel.delta);
     }
   return true;
}

#ifdef _WIN32
int WINAPI WinMain(HINSTANCE hinstance, HINSTANCE hPrevInstance,
                     LPSTR lpCmdLine, int nCmdShow) {
  char** argv;
  int argc=0;
#  else
int main(int argc, char* argv[]){
#endif
  //string assets_dir = "../ext/rltk/assets";
  string assets_dir = "../media/assets/";
  std::srand(std::time(0));

  // Logger instanciation
  static_logger = new Logger("error.log", LogLevel::Debug,
			     LOG_MEDIA_CERR | LOG_MEDIA_FILE);

  string assets_desc = string("Set the asseats dir. (Default to '") +
    assets_dir.c_str() + "')";
  
  // Declare the supported options. Trying to keep this list sorted by short
  // option.
#ifndef _WIN32  
  // Declare the supported options.
  po::options_description desc("Allowed options");
  desc.add_options()
    ("assets,a", po::value<string>(), assets_desc.c_str())
    ("help,h",         "Produce this help message and exit.")
    ("disable-mods,m", "Disable all mod loading at startup. (Empty game)")
    ("name,n",  po::value<string>(),"Set player's name (random one by default)")
    ("scores,s", po::value<string>()->default_value("text")
     ->implicit_value("text") ,
     "Print all highscores with optional format (text|json)")
    ("title,t",po::value<string>(),"Set player's title (random one by default)")
  
    ("version,v",         "Print version information and exit")
    ;
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);    

  if (vm.count("help")) {
    cout << desc << "\n";
    return 1;
  }

  if (!vm["scores"].defaulted()) {
    // Deactivate console output
    static_logger->setMediaType(LOG_MEDIA_CERR); 
    auto sf = vm["scores"].as<string>(); // Score format
    HighscoresUi hsu;
    hsu.print(sf);
    return 1;
  }

  static_logger->intro();
  
  if (vm.count("version")) {
    cout << VERSION_STRING << "\n";
    cout << "Compiled " << __DATE__ << " at " << __TIME__ << endl;
    return 1;
  }

  // Do we allowed more than 1 argc (only the program name) ?
  bool argc2_allowed = false;
  if (vm.count("disable-mods")) {
    ScriptManager::getInstance().disable();
  }
  if (vm.count("name")) {
    playerName = vm["name"].as<string>();
    argc2_allowed = true;
  }
  if (vm.count("title")) {
    playerTitle = vm["title"].as<string>();
    argc2_allowed = true;
  }
  if (vm.count("assets")) {
    assets_dir = vm["assets"].as<string>();
    argc2_allowed = true;
  }

  // Exit if non optional argument passed
  if (!argc2_allowed &&  argc > 1)
    {
      cout << "Unreconized option '" << argv[1] << "'" << endl;
      return 0;
    }
#endif // !_WIN32
  
  // locale and translation
  setlocale (LC_ALL, "");
  const char *tdir = getenv("PWD");
  string lg = getenv("LANG");
  TranslationManager::getInstance().setCountryCode(lg.substr(0, 2));
  
  bindtextdomain (PACKAGE, tdir);
  textdomain (PACKAGE);

  generate_player_name();
  
  // Initialize the library
  init(config_advanced(assets_dir, 1024, 768, "Solerogue"));
  ZoomSystem::setAssetsDir(assets_dir);
  set_default_font(FONT, 16);  // Need rltk::get_window
  
  gui->add_layer(MAIN_LAYER, 0, 32, 1024-160, 768-32, "32x32", resize_main);
  //  gui->delete_layer(MAIN_LAYER);
  //  gui->add_layer(MAIN_LAYER, 0, 32, 1024-160, 768-32, "32x32", resize_main);
  gui->add_layer(INFO_LAYER, 864, 32, 160, 768-32, "8x16", resize_info);   
  
  layerList.push_back(new TitleLayer(TITLE_LAYER, "SoleRogue"));

  HelpViewerUi::getInstance().openHelpDir(TranslationManager::getInstance()
					  .getHelpDir());

  ScriptManager::getInstance().init("mods/");
  ScriptManager::getInstance().call_OnInit();

  // Create our systems
  add_system<PlayerSystem>();
  add_system<VisibilitySystem>();
  add_system<CameraSystem>();
  add_system<ActorRenderSystem>();

  add_system<NodePointingSystem>();
  add_system<Hud>();
  //  add_system<HighScoresSystem>();
  add_system<MetricsSystem>();

  add_system<PickupMenuSystem>();
  add_system<InventorySystem>();
  add_system<EnemyInfoSystem>();
  add_system<GuiSystem>();

  add_system<ZoomSystem>();
  add_system<SoundManager>();

  rltk::optional_display_hook = display_hook;
  rltk::optional_event_hook   = event_hook;

  // Enter the main loop. "tick" is the function we wrote above.
  ecs_configure();
  run(tick);

  return 0;
}
