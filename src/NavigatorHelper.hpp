#ifndef _NAVIGATOR_HELPER_HPP_
#define _NAVIGATOR_HELPER_HPP_

#include "Position.hpp"

class NavigatorHelper {
public:
  static int get_x(const Position &loc);
  static int get_y(const Position &loc);
  static Position get_xy(const int &x, const int &y);
};

#endif  // !_NAVIGATOR_HELPER_HPP_
