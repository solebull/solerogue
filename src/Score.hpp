#ifndef _SCORE_HPP_
#define _SCORE_HPP_

#include <string>
#include <vector>
#include <map>
#include <utility>   // USes pair

using namespace std;

/** Designed as a Player entity's component
  *
  *
  * Keeps the number of killed entities by species.
  *
  * Note: playedTime and nbMoves are updated in InfoUi::update().
  *
  */
class Score
{
public:
  Score();

  void addExit();
  void addVisibility();

  void addBonus(const string&, int);
  const vector<pair<string, int>>& getBonuses(void) const;

  void          setKilledBy(const string&);
  const string& getKilledBy(void)const;
  
  int compute();

  // Kills by species summary
  void                           addKill(const std::string&);
  int                            getKill(const std::string&);
  const vector<pair<std::string, int>> getKills(void);

  // Public members ?!?
  string playedTime;
  int nbMoves;
  
private:
  string killedBy;
  int nbExits;                 // Number of found exits
  int nbVisibility;            // Number of found exits
  vector<pair<string, int>> bonuses;
  map<std::string, int> kills; //!< A summary of kill by species
};

#endif // !_SCORE_HPP_
