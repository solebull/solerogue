#include "MapManager.hpp"

#include "rltk.hpp"

#include "exceptions/MapOutOfBounds.hpp"

#include "api/Logger.hpp"

random_number_generator rng;


MapManager::MapManager(token):
  currentMap(0),
  deeperDungeon(1)
{
  maps.emplace_back(Map{40, 40});
}

Map*
MapManager::getCurrentMap()
{
  return getMapByIndex(currentMap);
}

Map*
MapManager::getMapByIndex(int idx)
{
  if (idx < 0 || idx > (int)maps.size())
    {
      LPUSH("GetMap");
      LOG(LogLevel::Info, "idx is %d", idx );
      LOG(LogLevel::Info, "Maps size is %d", maps.size() );
      LOG(LogLevel::Info, "Maps capacity is %d", maps.capacity() );
      LOG(LogLevel::Info, "Maps max_size is %d", maps.max_size() );
      LPOP();
      throw MapIndexOutOfBoundsException(idx);
    }
  return &maps[idx];
}

void
MapManager::newMap()
{
  size_t width =  this->maps[currentMap].getWidth();
  size_t height = this->maps[currentMap].getHeight();

  int newW =  (width * 1.15) + rng.roll_dice(1, 20);
  int newH =  (height * 1.15) + rng.roll_dice(1, 20);

  LOG(LogLevel::Info, "Generating a new map of %dx%d", newW, newH);
  assert(newW < 5000 && "REALLY BIG MAP");
  assert(newH < 5000 && "REALLY BIG MAP");

  this->maps.emplace_back(Map{newW, newH, (int)currentMap+1});
}

void
MapManager::go(ActorMovedDirection amd)
{
  
  if (amd == DOWN)
    {
      //LOG(LogLevel::Info, "CurrentMap : %d", this->currentMap);
      
      if (this->currentMap + 1 >= this->maps.size())
	this->newMap();
      
      ++currentMap;

    }
  else if (amd == UP)
    {
      if (currentMap == 0)
	LOG(LogLevel::Info, "YOU WON!!");
      else
	--currentMap;
    }

  if ((int)currentMap + 1 > deeperDungeon)
    deeperDungeon = (int)currentMap + 1;
}

/** Get the actual dungeon #
  *
  * \return The current dungeon level number as a signed integer.
  *
  */
int
MapManager::getDungeonLevel() const
{
  return (int)currentMap + 1;
}

int
MapManager::getDeeperDungeon() const
{
  return deeperDungeon;
}

const vector<Map>&
MapManager::getMaps(void) const
{
  return maps;
}
