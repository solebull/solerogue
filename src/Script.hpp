#ifndef __SCRIPT_HPP__
#define __SCRIPT_HPP__

#include "rltk.hpp"   // USES entity_t*

#include <string>
#include <iostream>

using namespace std;

// Forward declarations
class Map;
class VcharBuffer;
// End of forward declarations

class Script
{
public:
  Script(const string& vPath);
  virtual ~Script();

  virtual void call_OnInit(void)=0;
  virtual void call_OnGenerate(Map*)=0;
  virtual void call_OnMove(Map*, rltk::entity_t*)=0;
  virtual void call_OnFrame(double, VcharBuffer*)=0;
  /** Call getTile with given parameters on all scripts
    *
    *  \param mapLevel Actual dungeon depth
    *  \param x        Horizontal position of the tile
    *  \param y        Vertical position of the tile 
    *  \param isWall   Is the original to-be-replaced tile a wall ?
    *
    *
    * \return The character index to be drawn
    *
    */
  virtual int call_OnGetTile(int mapLevel, int x, int y, bool isWall)=0;
    
private:
  string path;
};




#endif // !__SCRIPT_HPP__
