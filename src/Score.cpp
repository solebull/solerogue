#include "Score.hpp"

#include <iostream>
#include <cmath>
#include <algorithm> // USES sort()
using namespace std;


Score::Score():
  nbExits(0),
  nbVisibility(0)
{

}

void
Score::addExit()
{
  ++nbExits;
}

void
Score::addVisibility()
{
  ++nbVisibility;
}


int
Score::compute()
{
  int vis = ceil(nbVisibility/100);
  int temp = (nbExits * 10) + vis;

  int b = 0;
  for (auto bo : bonuses)
    b += bo.second;
  return temp + b;
}

const vector<pair<string, int>>&
Score::getBonuses(void) const
{
  return this->bonuses;
}


void
Score::addBonus(const string& bonusName, int value)
{
  bonuses.push_back(make_pair(bonusName, value));
}

void
Score::setKilledBy(const string& kb)
{
  this->killedBy = kb;
}

const string&
Score::getKilledBy(void)const
{
  return this->killedBy;
}

/** May contain empty key */
void
Score::addKill(const std::string& key)
{
  kills[key]++;
}

int
Score::getKill(const std::string& key)
{
  return kills[key];
}

/** Return a kills vector sorted by most killed first */
const vector<pair<std::string, int>>
Score::getKills(void)
{
  vector<pair<string, int>> kvec;
  for (auto& k: kills)
    kvec.push_back(make_pair(k.first, k.second));
  
  std::sort(kvec.begin(), kvec.end(),
    [=](std::pair<string, int>& a, std::pair<string, int>& b)
      {
	return a.second > b.second;
      });
  
  return kvec;
}

