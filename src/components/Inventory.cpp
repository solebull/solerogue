#include "Inventory.hpp"

#include "components/Character.hpp"

#include "api/PlayerId.hpp" // USES player_id

Inventory::Inventory()
{

}

void
Inventory::add(Entity* e)
{
  entities.insert(e);
}

std::unordered_set<Entity*>
Inventory::getEntities()
{
  return entities;
}

void
Inventory::consume(Entity* e)
{
  entities.erase(e);
}

bool
Inventory::isEquiped(Entity* e) const
{
  auto c = entity(player_id)->component<Character>();
  return ((e == c->getLHWeapon()) || (e == c->getRHWeapon()));
}

