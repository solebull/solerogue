#ifndef __CHARACTER_HPP__
#define __CHARACTER_HPP__

#include <api/Weapon.hpp>

typedef enum
  {
   CH_LEFT = 0,
   CH_RIGHT,
  } CurrentHand_t;   

/** The logic behind character equipement
  * 
  * The UI part of the equipement is handled in CharacterUi.
  * Handle Player (or others ?) equipment 
  *
  **/
class Character
{
public:
  Character();
  virtual ~Character();

  const Weapon* getLHWeapon(void) const;
  const Weapon* getRHWeapon(void) const;

  void setLHWeapon(Weapon*);
  void setRHWeapon(Weapon*);
  void unequip(CurrentHand_t);
  
  CurrentHand_t getCurrentHand(void) const;
  void changeCurrentHand(void);
  
private:
  Weapon* leftHand;
  Weapon* rightHand;

  /** The default weapon
    *
    * Note: this can't be a non-pointer instance. It causes a segfault
    * when trying to get a translated Entity name.
    *
    */
  Weapon* bareHand;
  
  CurrentHand_t currentHand;
};

#endif // !__CHARACTER_HPP__
