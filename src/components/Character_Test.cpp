#include "Character.hpp"
#include "api/Weapon.hpp"
#include <gtest/gtest.h>

TEST( Character, currentHand_change )
{
  Character c;
  ASSERT_EQ( c.getCurrentHand(),  CH_LEFT);
}

TEST( Character, currentHandChange )
{
  Character c;
  c.changeCurrentHand();
  ASSERT_EQ( c.getCurrentHand(),  CH_RIGHT);
  c.changeCurrentHand();
  ASSERT_EQ( c.getCurrentHand(),  CH_LEFT);
}

TEST( Character, unequip_left )
{
  Weapon w("UnequipTest");
  Character c;
  auto bareHand = c.getLHWeapon();
  c.setLHWeapon(&w);
  ASSERT_EQ( c.getLHWeapon(),  &w);
  c.unequip(CH_LEFT);
  ASSERT_NE( c.getLHWeapon(),  &w);
  ASSERT_EQ( c.getLHWeapon(),  bareHand);
}

TEST( Character, unequip_right )
{
  Weapon w("UnequipTest");
  Character c;
  auto bareHand = c.getRHWeapon();
  c.setRHWeapon(&w);
  ASSERT_EQ( c.getRHWeapon(),  &w);
  c.unequip(CH_RIGHT);
  ASSERT_NE( c.getRHWeapon(),  &w);
  ASSERT_EQ( c.getRHWeapon(),  bareHand);
}

