#ifndef __INVENTORY_HPP__
#define __INVENTORY_HPP__

#include <unordered_set>

// Forward declarations
class Entity;
// End of forward declarations

class Inventory
{
public:
  Inventory();
  void add(Entity*);
  void consume(Entity*);

  std::unordered_set<Entity*> getEntities();

  bool isEquiped(Entity*) const;
  
private:
  std::unordered_set<Entity*> entities;
};

#endif // !__INVENTORY_HPP__
