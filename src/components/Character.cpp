#include "Character.hpp"

#include "config.h" // USES _()

#include "api/Rng.hpp"

// Generic callback
int hand_damage()
{
  return rng::rdice(1, 4);
}

/** The Character component default constructor */
Character::Character():
  bareHand(nullptr),
  currentHand(CH_LEFT)
{

  bareHand = new Weapon("Bare hand");
  bareHand->setOnDamage(&hand_damage);
  bareHand->setDamageString("1d4");
  
  leftHand  = bareHand;
  rightHand = bareHand;
}

Character::~Character()
{
  // delete bareHand; // Causes a segfault
}


const Weapon*
Character::getLHWeapon(void) const
{
  return leftHand;
}

const Weapon*
Character::getRHWeapon(void) const
{
  return rightHand;
}

void
Character::setLHWeapon(Weapon* w)
{
  leftHand = w;

  // Exclusive impl.
  if (rightHand == w)
    rightHand = bareHand;
}

void
Character::setRHWeapon(Weapon* w)
{
  rightHand = w;

  // Exclusive impl.
  if (leftHand == w)
    leftHand = bareHand;
}

CurrentHand_t
Character::getCurrentHand(void) const
{
  return currentHand;
}

void
Character::changeCurrentHand(void)
{
  if (currentHand == CH_LEFT)
    currentHand = CH_RIGHT;
  else
    currentHand = CH_LEFT;
}

/** Unequip the weapon in the given hand and go back to bareHand
  *
  * \ch The hand to empty.
  *
  */
void
Character::unequip(CurrentHand_t ch)
{
  if (ch == CH_LEFT)
    leftHand = bareHand;
  else
    rightHand = bareHand;
}

