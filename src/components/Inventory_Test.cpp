#include "Inventory.hpp"
#include <gtest/gtest.h>

#include "api/Entity.hpp"


TEST( Inventory, ctor )
{
  Inventory i;
  ASSERT_EQ( i.getEntities().size(),  0);
}

TEST( Inventory, add )
{
  Entity e("aze");
  Inventory i;
  i.add(&e);
  ASSERT_EQ( i.getEntities().size(),  1);
}

TEST( Inventory, add_two )
{
  Entity e("aze");
  Entity e2("aze");
  Inventory i;
  i.add(&e);
  i.add(&e2);
  ASSERT_EQ( i.getEntities().size(),  2);
}

// Adding twice the same pointer should only add once
TEST( Inventory, add_twice )
{
  Entity e("aze");
  Inventory i;
  i.add(&e);
  i.add(&e);
  ASSERT_EQ( i.getEntities().size(),  1);
}

