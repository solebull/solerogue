#include "ScriptManager.hpp"

#include <iostream>

#include <stdexcept>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>

#include "Script.hpp"
#include "scripts/So.hpp"
#include "systems/Gui.hpp"

#include "i18n.hpp"

#include "api/Logger.hpp"

ScriptManager::ScriptManager(token):
  enabled(true)
{
  
}

void
ScriptManager::init(const string& dirname)
{
  if (!enabled) return;

  if (!checkDirectory(dirname))
    throw runtime_error("Cannot open mods directory");

  addSubScrips(dirname);
}

bool
ScriptManager::checkDirectory(const string& dirname)
{
  const char* pathname = dirname.c_str();

  struct stat info;
  if( stat( pathname, &info ) != 0 )
    return false;
  else if( info.st_mode & S_IFDIR )
    return true;
  else
    return false;
}

bool
ScriptManager::checkFile(const string& dirname)
{
  const char* pathname = dirname.c_str();

  struct stat info;
  if( stat( pathname, &info ) == 0 )
    return true;
  else
    return false;
}


void
ScriptManager::addSubScrips(const string& dirname)
{
  if (!enabled) return;

  string cc = TranslationManager::getInstance().getCountryCode();
  
  DIR *dir;
  struct dirent *ent;
  if ((dir = opendir (dirname.c_str())) != nullptr) {
    /* print all the files and directories within directory */
    while ((ent = readdir (dir)) != nullptr)
      {
	if (ent->d_name[0] != '.')
	  {
	    string moddir = dirname + ent->d_name;
	    addScript(moddir, ent->d_name);
	    string helpdir = moddir + "/help" + "/" + cc;
	    string fallback = moddir + "/help" + "/en/";
	    LOG(LogLevel::Info, "Calling openHelpDir for '%s'",
		ent->d_name);
	    HelpViewerUi::getInstance().openHelpDir(helpdir, fallback,
						    ent->d_name);
	  }
      }
    closedir (dir);
  }
  else
    {
      /* could not open directory */
      perror ("");
    }
}

void
ScriptManager::addScript(const string& path, const string& libname)
{
  if (!enabled) return;

  if (!checkDirectory(path))
    return;
  
  string initso = path + "/" + "lib" + libname + ".so";
  TranslationManager::getInstance().parse(path);
  
  if (checkFile(initso))
    {
      LOG(LogLevel::Info, "Adding script '%s'", initso.c_str());
      SoScript* so = new SoScript(initso);
      scripts.emplace_back(so);
    }
  else
    LOG(LogLevel::Error, "Can't find file '%s'", initso.c_str());
}

void
ScriptManager::call_OnInit()
{
  if (!enabled) return;

  for (auto& s : scripts)
    s->call_OnInit();
}

void
ScriptManager::call_OnGenerate(Map* m)
{
  if (!enabled) return;

  for (auto& s : scripts)
    s->call_OnGenerate(m);
}


void
ScriptManager::call_OnMove(Map* m,  rltk::entity_t* pl)
{
  if (!enabled) return;

  for (auto& s : scripts)
    s->call_OnMove(m, pl);
}

void
ScriptManager::call_OnFrame(double duration, VcharBuffer* vb)
{
  if (!enabled) return;
  
  for (auto& s : scripts)
    s->call_OnFrame(duration, vb);
}

/** Deactivate ScriptManager work
  *
  * All mod works use early exits based on this var value.
  *
  */
void
ScriptManager::disable(void)
{
  enabled = false;
}

/** Is the ScriptManager enabled ?
  *
  * \return \c true if enabled.
  *
  */
bool
ScriptManager::isEnabled(void)
{
  return enabled;
}

/** Call getTile with given parameters on all scripts
  *
  * Returns the last wall non equal to -1.
  *
  *  \param mapLevel Actual dungeon depth
  *  \param x        Horizontal position of the tile
  *  \param y        Vertical position of the tile 
  *  \param isWall   Is the original to-be-replaced tile a wall ?
  *
  *
  * \return The character index to be drawn or -1 if no changes.
  *
  */
int
ScriptManager::call_OnGetTile(int mapLevel, int x, int y, bool isWall)
{
  if (!enabled) return -1;

  int ret = -1;
  for (auto& s : scripts)
    {
      int r = s->call_OnGetTile(mapLevel, x, y, isWall);
      if (r != -1)
	ret = r;
    }
  return ret;
}
