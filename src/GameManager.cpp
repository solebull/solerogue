#include "GameManager.hpp"

#include "systems/Player.hpp" // USES generate_player_name();
#include "api/PlayerId.hpp" // USES player_id
#include "messages/AddLogEntry.hpp"
#include "messages/ResetPlayer.hpp"
#include "messages/HudUpdate.hpp"

GameManager::GameManager(token)
{

}

/** Generate new or reset data for a new game
  *
  */
void
GameManager::newGame(void)
{
  generate_player_name();
  string msg="You resurrected as " + playerName + ' ' + playerTitle;
  emit(AddLogEntryMessage(msg));
  emit(ResetPlayerMessage());

  // And finally
  emit(HudUpdateMessage());
}
