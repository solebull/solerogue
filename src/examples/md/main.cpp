#include <md4c.h>

#include <iostream>

#include <cstring>

using namespace std;

    /* Caller-provided rendering callbacks.
     *
     * For some block/span types, more detailed information is provided in a
     * type-specific structure pointed by the argument 'detail'.
     *
     * The last argument of all callbacks, 'userdata', is just propagated from
     * md_parse() and is available for any use by the application.
     *
     * Note any strings provided to the callbacks as their arguments or as
     * members of any detail structure are generally not zero-terminated.
     * Application has take the respective size information into account.
     *
     * Callbacks may abort further parsing of the document by returning non-zero.
     */
int enter_block(MD_BLOCKTYPE type, void* /*detail*/, void* /*userdata*/)
{
  string s;
  switch (type)
    {
    case MD_BLOCK_DOC:
      s = "body";
      break;
    case MD_BLOCK_P:
      s = "p";
      break;
    default:
      s = to_string(type);
    }
  
  cout << "<" << s << ">" << endl;
  return 0;
}
int leave_block(MD_BLOCKTYPE /*type*/, void* /*detail*/, void* /*userdata*/)
{

  return 0;
  
}

int enter_span(MD_SPANTYPE type, void* /*detail*/, void* /*userdata*/)
{
  string s;
  switch (type)
    {
    case MD_SPAN_EM:
      s = "em";
      break;
    case MD_SPAN_STRONG:
      s = "strong";
      break;
    default:
      s = to_string(type);
    }
  
  cout << "<" << s << ">" <<endl;

  
  return 0;
  
}
int leave_span(MD_SPANTYPE /*type*/, void* /*detail*/, void* /*userdata*/)
{
  
  return 0;
}


int text(MD_TEXTTYPE type, const MD_CHAR* text, MD_SIZE size, void* /*userdata*/)
{
  switch (type)
    {
    case MD_TEXT_NORMAL:
      for (int i = 0; i< size; ++i)
	cout << text[i];
      break;
      
    }
  return 0;
  
}

/* Debug callback. Optional (may be NULL).
 *
 * If provided and something goes wrong, this function gets called.
 * This is intended for debugging and problem diagnosis for developers;
 * it is not intended to provide any errors suitable for displaying to an
 * end user.
 */
void debug_log(const char* msg, void* /*userdata*/)
{
  cout << "ERROR: " << msg << endl;
}


int
main()
{

  MD_RENDERER rn;
  rn.abi_version = 0;
  rn.enter_block = &enter_block;
  rn.leave_block = &leave_block;
  rn.enter_span = &enter_span;
  rn.leave_span = &leave_span;
  rn.text = &text;
  rn.debug_log = &debug_log;
  rn.syntax = NULL;

  const char* text = "Test *mark* **down**";
  
  //  md_parse(const MD_CHAR* text, MD_SIZE size, const MD_PARSER* parser, void* userdata);
  //
  // see https://github.com/mity/md4c/blob/master/src/md4c.h
  int ret = md_parse(text, strlen(text), &rn, NULL);
  if (ret != 0)
    cout << "ERROR reading md file : " << ret << endl;
  return 0;
}
