// You need to include the RLTK header
#include "rltk.hpp"

// We're using a stringstream to build the hello world message.
#include <sstream>
#include <string>
#include <cwchar>

#include "config.h"

// For convenience, import the whole rltk namespace. You may not want to do this
// in larger projects, to avoid naming collisions.
using namespace std;

using namespace rltk;
using namespace rltk::colors;

constexpr const char* FONT= "/usr/share/fonts/truetype/freefont/FreeMono.ttf";

// Tick is called every frame. The parameter specifies how many ms have elapsed
// since the last time it was called.
void tick(double duration_ms) {
  if (console->dirty)
  {
    console->clear(vchar{' ', WHITE, GREY});
    /*
    console->print(1,1,"Hello World", WHITE, BLACK);

    console->print(1,2,"Text avec accents : éàè", WHITE, BLACK);
    */
    //print(console.get(), 1, 3, "Text avec accents : éàè", BLACK, WHITE);
    /*
    vchar c(130, WHITE, BLACK);
    cout << "Accent : " << (char)130 << endl;
    console->set_char(1,5,c);

    text.setString("Hello world");
    */
  }
  print(console.get(), 100, 100, 10, 10, "Text éééà", sf::Color::White, sf::Color::Black);
  //  window->display();
}

// Your main function
int main()
{
  setlocale (LC_ALL, "");
  const char *tdir = getenv("PWD");
  string lg = getenv("LANG");
  
  bindtextdomain (PACKAGE, tdir);
  textdomain (PACKAGE);

  init(config_simple("../ext/rltk/assets", 80, 25, "solerogue i18n tests",
		     "8x16", false));
  set_default_font(FONT, 8);
  
  //  window = rltk::get_window();

  // Enter the main loop. "tick" is the function we wrote above.
  run(tick);

  return 0;
}

