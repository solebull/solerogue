
#include "Singleton.hpp"

#include <string>
#include <iostream>

using namespace std;
class Aze final : public Singleton<Aze>
{
public:
  Aze(token){};
  ~Aze(){};

  void setName(const string& n){ name = n; }

  void debug(){  cout << "Aze " << name << endl;}
  
private:
  string name;
  
};

int
main()
{
  auto& a =  Aze::getInstance();
  a.setName("auto a");
  a.debug();
  //  Aze* r = new Aze();
  
  auto& b =  Aze::getInstance();
  b.debug();
  

  
}
