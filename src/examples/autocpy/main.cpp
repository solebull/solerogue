// Trying to forbid auto to retrieve map

#include <vector>
#include <iostream>

using namespace std;

class Map
{
public:
  Map(int v):i(v){}
  int i;

private:
  // Map(const Map&);
  Map& operator=(const Map&);
};

class MapManager
{
public:
  MapManager()
  {
    maps.emplace_back(Map(5));
  }
  Map* getMapV1(){ return &maps[0]; }

private:
  vector<Map> maps;
};


int
main()
{
  MapManager mm;
  auto m1 = mm.getMapV1();
  m1->i = 20;
  cout << mm.getMapV1()->i << endl;
}
