// Testing rltk serialization feature

#include "rltk.hpp"

#include <string>
#include <list>
#include <iostream>

using namespace rltk;
using namespace std;

random_number_generator rng;

#define MAX 160

class Score
{
public:
 Score():s(0),name(""){
   s= rng.roll_dice(1, MAX);
   name = "AZE" + to_string(rng.roll_dice(1, MAX));
  };
  Score(int sc, const string& n):s(sc), name(n) { };

  void serialize(struct gzip_file& g) const
  {
    g.serialize<int>(s);
    g.serialize<string>(name);
  }

  void deserialize(struct gzip_file& g)
  {
    g.deserialize<int>(s);
    g.deserialize<string>(name);
  }

  void debug() const
  {
    cout << "Score: " << s << endl;
    cout << "Name: " << name << endl;
  }
  
  int s;
  string name;
};

class HighScores
{
public:
  HighScores() {};
  ~HighScores() {};

  void serialize(struct gzip_file& g)
  {
    size_t sz = scores.size();
    g.serialize<size_t>(sz);
    for (auto const& s: scores)
      {
	s.serialize(g);
      }
  }

  void deserialize(struct gzip_file& g)
  {
    size_t sz;
    g.deserialize<size_t>(sz);

    for (size_t i=0; i < sz; ++i)
      {
	Score s;
	s.deserialize(g);
	scores.push_back(s);
      }
  }

  void debug() 
  {
    for (auto const& s: scores)
      {
	s.debug();
      }

  }
  list<Score> scores;
};


void
serialize()
{
  /*
  HighScores hg;
  hg.scores.push_back(Score(123));
  */
  
  Score s;
  
  struct gzip_file gz("seralize-test.gz", "w");
  // gz.serialize<Score>(s);
  s.serialize(gz);
}

void deserialize()
{
  HighScores hg;
  struct gzip_file gz("seralize-test.gz", "r");
  hg.deserialize(gz);
  hg.debug();
  
}


int
main()
{
  /*
  HighScores hg;
  int r = rng.roll_dice(1, MAX);
  for (int i = 0; i < r; ++i)
    {
      Score s;
      hg.scores.push_back(s);
    }

  struct gzip_file gz("seralize-test.gz", "w");
  hg.serialize(gz);
  */
  deserialize();

  return 0;
}
