#include <functional>
#include <iostream>
#include <iostream>
#include <map>

using namespace std;

/** An empty Player class */
class Player
{

};

/** An example Entity class */
class Entity
{
public:
  using ActionFn = std::function<void(Player*)>;

  /** Add the given action into the action map
    *
    * \param verb   The action verb
    * \param action The action callbacl/lambda
    *
    */
  void add(const string& verb, ActionFn action)
  {
   actions.insert(pair<string, ActionFn>(verb, action));
  }

  /** Fire this action
    * 
    * If the verb doesn't xist in the map, nothing is done and a warning
    * is issued in the console.
    *
    * /param verb The action, defined by a verb
    *
    */
  void act(const string& verb)
  {
    if (actions[verb])
      actions[verb](&player);
    else
      cout << "Entity doesn't know how to " << verb << endl;
  }

  /** Returns a list of possible actions on this entity 
    *  
    * \retur A string list.
    *
    */
  void possibleActions()
  {
    cout << "Entity knows how to ";
    for (auto& a : actions)
      cout << a.first << " ";

    cout << endl;
  }
private:
  map<string, ActionFn> actions; //!< A verb/action map container
  Player                player;  //!< A player instance
};


int main()
{
  Entity e;
  e.add("eat", [](Player*)
   {
     cout << "Eating" << endl;
   });
  e.add("cry", [](Player*)
   {
     cout << "I'm so sad" << endl;
   });

  e.possibleActions();


  //
  e.act("eat");
  e.act("walk");
}
