#ifndef _HIGHSCORES_ENTRY_HPP_
#define _HIGHSCORES_ENTRY_HPP_

#include "rltk.hpp"

#include <string>
#include <ctime>

using namespace rltk;
using namespace std;

class HighScoresEntry
{
public:
  HighScoresEntry();

  void serialize(struct gzip_file&) const;
  void deserialize(struct gzip_file&);

  void setCurrentGame(bool);
  bool isCurrentGame(void) const;
  time_t getEpoch() const;

  void          setKilledBy(const string&);
  const string& getKilledBy(void)const;

  void          setPlayedTime(const string&);
  const string& getPlayedTime(void)const;
  
  void setLevel(int);
  int  getLevel(void) const;

  bool operator==(HighScoresEntry const &rhs) const { 
    return (rhs.getEpoch() == this->epoch) &&
      (rhs.score == this->score); 
  }
  
  int score;
  int movesNb;
  int maxDungeonDepth;
  string name;

private:
  string killedBy;
  bool   currentGame;
  time_t epoch;       // Date/time the game were played
  int    level;       // The player's general level
  string playedTime;  //!< Game duration as a string
};


#endif // !_HIGHSCORES_ENTRY_HPP_
