#include "MapManager.hpp"
#include <gtest/gtest.h>

#include <iostream>

#include "api/Logger.hpp"

using namespace std;

TEST( MapManager, deeper_ctor )
{
  auto& m = MapManager::getInstance();
  ASSERT_EQ( m.getDeeperDungeon(),  1);
}

TEST( MapManager, deeper_2 )
{
  auto& m = MapManager::getInstance();
  m.go(DOWN);
  ASSERT_EQ( m.getDeeperDungeon(),  2);
}

TEST( MapManager, deeper_3 )
{
  auto& m = MapManager::getInstance();
    
  m.go(DOWN);
  m.go(UP);
  ASSERT_EQ( m.getDeeperDungeon(),  3);
}

// MapManager has a getMaps function and have an initial map
TEST( MapManager, go_getMaps )
{
  auto& mm = MapManager::getInstance();
  auto maps = mm.getMaps();
  ASSERT_EQ( maps.size(),  3);
}

TEST( MapManager, go_segfault )
{
  auto& m = MapManager::getInstance();
  for (int i = 0; i < 10; ++i)
    {
      LOG(LogLevel::Debug, "Going down '%d'", i);
      m.go(DOWN);
    }
  
  ASSERT_EQ( m.getDeeperDungeon(),  12);
}


/** I suspect the modification of a Map get from getCurrentMap impossiible */
TEST( MapManager, getCurrentMap_modification )
{
  // With an auto m, the modifications fail
  auto m = MapManager::getInstance().getCurrentMap();
  Node n = 14;
  m->setTile(10, 10, n);
  ASSERT_EQ( MapManager::getInstance().getCurrentMap()->getTile(10, 10),  n);
}
