#include "Text.hpp"

#include <sstream>

using namespace std;

HighScoresTextFormatter::~HighScoresTextFormatter()
{

}

void
HighScoresTextFormatter::start(void)
{
  cout << "Lvl   = Player's general level." << endl;
  cout << "Epoch = The date/time the play occured." << endl;
  cout << endl;
  
  columnsWidth.resize(4);
  
  // Headers
  columnsWidth[0] = 3; // Index of the entry (min for Idx)
  columnsWidth[1] = 3; // Name
  columnsWidth[2] = 3; // Lvl
  columnsWidth[3] = 8; // Played
  columnsWidth[4] = 10; // Epoch

  columnsWidth[5] = 5; // Score
}

void
HighScoresTextFormatter::print(const HighScoresEntry& e)
{
  int namel = e.name.size();
  if (namel > columnsWidth[1])
    columnsWidth[1] = namel;
  
  entries.push_back(e);
}

string
HighScoresTextFormatter::getTime(time_t epoch)
{
  auto now = std::time(nullptr);
  char buf[80];
  struct tm epochTm = *localtime(&epoch);
  struct tm startOfDay = *localtime(&now);
  startOfDay.tm_sec=0;
  startOfDay.tm_min=0;
  startOfDay.tm_hour=0;
  time_t startOfDayTime = mktime(&startOfDay);
  if (epoch < startOfDayTime)
    strftime(buf, sizeof(buf), "%Y-%m-%d", &epochTm);
  else
    strftime(buf, sizeof(buf), "%H:%M", &epochTm);
  return string(buf);
}


void
HighScoresTextFormatter::end(void)
{
  int idx = 1;

  // Compute index width
  ostringstream oss1;
  oss1 << to_string(entries.size() + 1);
  columnsWidth[0] = oss1.str().size();

  
  cout << std::setfill(' ');

  // Header
  //  string idxs = "Idx" + string((columnsWidth[0]/2), ' ');
  cout << std::setw(columnsWidth[0]+1) << "Idx" <<" |";
  string name = "Name" + string((columnsWidth[1]/2), ' ');
  cout << std::setw(columnsWidth[1]+1) << name <<" |";
  
  cout << std::setw(columnsWidth[2]+1) << "Lvl" <<" |"; 
  cout << std::setw(columnsWidth[3]+1) << "Played " <<" |"; 
  cout << std::setw(columnsWidth[4]+1) << "Epoch   " <<" |"; 

  cout << std::setw(columnsWidth[5]+1) << "Score" <<" |"; 
  cout << endl;
  
  // Line
  cout << std::setfill('-');
  cout << std::setw(columnsWidth[0] + 1) << "-" << "-+"; 
  cout << std::setw(columnsWidth[1] + 1) << "-"<< "-+";
  cout << std::setw(columnsWidth[2] + 1) << "-"<< "-+";
  cout << std::setw(columnsWidth[3] + 1) << "-"<< "-+";
  cout << std::setw(columnsWidth[4] + 1) << "-"<< "-+";

  cout << std::setw(columnsWidth[5] + 1) << "-"<< "-+";
  cout << endl;
  
  // Print everything
  cout << std::setfill(' ');
  for (auto& e: entries)
    {
      cout << std::setw(columnsWidth[0] + 1) << idx++ << " |"; 
      cout << std::setw(columnsWidth[1] + 1) << e.name << " |";
      cout << std::setw(columnsWidth[2] + 1) << e.getLevel() <<" |"; 
      cout << std::setw(columnsWidth[3] + 1) << e.getPlayedTime() <<" |"; 
      cout << std::setw(columnsWidth[4] + 1) << getTime(e.getEpoch()) <<" |"; 
      cout << std::setw(columnsWidth[5] + 1) << e.score << " |";
      cout << endl;
    }
}
