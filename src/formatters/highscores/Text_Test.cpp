#include "Text.hpp"

#include <gtest/gtest.h>

class HighScoresTextFormatterTest : public HighScoresTextFormatter
{
public:
  string getTimeTest(time_t e)
  {
    return getTime(e);
  }
};

// If the epoch is from today, returns a HH::SS formatted string
TEST( HighScoresTextFormatter, getTime_today )
{
  auto now = std::time(nullptr);
  HighScoresTextFormatterTest htf;
  auto time = htf.getTimeTest(now - 1);
  ASSERT_EQ(time.size(), strlen("00:01"));
}

// If the epoch is from today, returns a HH::SS formatted string
TEST( HighScoresTextFormatter, getTime_long )
{
  HighScoresTextFormatterTest htf;
  auto time = htf.getTimeTest(01);
  ASSERT_EQ(time.size(), strlen("2020-08-01"));
}
