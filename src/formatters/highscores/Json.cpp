#include "Json.hpp"

#include <iostream>

using namespace std;

HighScoresJsonFormatter::HighScoresJsonFormatter():
  first(true),
  index(0)
{

}

HighScoresJsonFormatter::~HighScoresJsonFormatter()
{

}


void
HighScoresJsonFormatter::start(void)
{
  cout <<"["  << endl;
}

void
HighScoresJsonFormatter::print(const HighScoresEntry& e)
{

  if (first)
    first = false;
  else
    cout << ",";

  // Entry
  cout << "{";
  cout << "\"Index\" : \"" << index++ << "\",";
  cout << "\"Name\" : \"" << e.name << "\",";
  cout << "\"Level\" : \"" << e.getLevel() << "\",";
  cout << "\"Score\" : \"" << e.score << "\",";
  cout << "\"MovesNumber\" : \"" << e.movesNb << "\",";
  cout << "\"MaxDungeonDepth\" : \"" << e.maxDungeonDepth << "\",";
  cout << "\"StartedEpoch\" : \"" << e.getEpoch() << "\",";
  cout << "\"KilledBy\" : \"" << e.getKilledBy() << "\",";
  
  cout << "\"PlayeTimed\" : \"" << e.getPlayedTime() << "\"";

  cout << "}" << endl;
      
}

void HighScoresJsonFormatter::end(void)
{
  cout <<"]"  << endl;
}
