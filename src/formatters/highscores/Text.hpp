#ifndef __FORMATTERS_HIGHSCORES_TEXT_HPP__
#define __FORMATTERS_HIGHSCORES_TEXT_HPP__

#include "HighScoresFormatter.hpp"
#include "HighScoresEntry.hpp"

#include <vector>
#include <list>
#include <time.h>       /* time_t, struct tm, time, mktime */

/** A very basic column-based console formatter.
  *
  */
class HighScoresTextFormatter : public HighScoresFormatter
{
public:
  ~HighScoresTextFormatter();
  
  virtual void start(void) override;
  virtual void print(const HighScoresEntry&) override;
  virtual void end(void) override;

protected:
  string getTime(time_t);
  
private:
  std::vector<int>           columnsWidth; //!< Keeps each colmns width
  std::list<HighScoresEntry> entries;      //!< The entry list
};

#endif // !__FORMATTERS_HIGHSCORES_TEXT_HPP__
