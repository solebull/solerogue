#ifndef __FORMATTERS_HIGHSCORES_JSON_HPP__
#define __FORMATTERS_HIGHSCORES_JSON_HPP__

#include "HighScoresFormatter.hpp"
#include "HighScoresEntry.hpp"

#include <vector>
#include <list>
#include <time.h>       /* time_t, struct tm, time, mktime */

/** A very basic column-based console formatter.
  *
  */
class HighScoresJsonFormatter : public HighScoresFormatter
{
public:
  HighScoresJsonFormatter();
  ~HighScoresJsonFormatter();
  
  virtual void start(void) override;
  virtual void print(const HighScoresEntry&) override;
  virtual void end(void) override;

private:
  bool first; //!< First entry ?
  int index;  //!< Entry index
};

#endif // !__FORMATTERS_HIGHSCORES_JSON_HPP__
