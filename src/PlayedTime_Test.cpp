#include "PlayedTime.hpp"
#include <gtest/gtest.h>

class TestablePlayedTime : public PlayedTime
{
public:
  string dTS(int i)
  {
    chrono::duration<double> d = (chrono::duration<double>)i;
    return durationToString(d);
  }
  
};


TEST( PlayedTime, durationToString )
{
  TestablePlayedTime til;
  ASSERT_EQ(    til.dTS(1), "00:00:01");
  ASSERT_EQ(   til.dTS(61), "00:01:01");
  ASSERT_EQ( til.dTS(3601), "01:00:01");
}
