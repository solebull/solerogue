#include <gtest/gtest.h>

#include "HighScoresEntry.hpp"

using namespace std;

#define FILENAME "serialize-test.gz"

TEST( HighScoreEntry, currentGame )
{
  HighScoresEntry hse;
  ASSERT_EQ( hse.isCurrentGame(),  false);
  hse.setCurrentGame(true);
  ASSERT_EQ( hse.isCurrentGame(),  true);
}

TEST( HighScoreEntry, killedBy )
{
  HighScoresEntry hse;
  ASSERT_EQ( hse.getKilledBy(),  "");
  hse.setKilledBy("aze");
  ASSERT_EQ( hse.getKilledBy(),"aze");
}

TEST( HighScoreEntry, level )
{
  HighScoresEntry hse;
  ASSERT_EQ( hse.getLevel(),  1);
  hse.setLevel(5);
  ASSERT_EQ( hse.getLevel(),  5);

  ASSERT_THROW(hse.setLevel(-5), runtime_error);
}

// Two scores with a same epoch but different score shouldn't be esual
TEST( HighScoreEntry, diff_epoch )
{
  HighScoresEntry hs1; hs1.score = 10;
  HighScoresEntry hs2; hs2.score = 12;
  ASSERT_EQ( hs1 == hs2, false);
}



TEST( HighScoreEntry, serialize )
{
  /*
  struct gzip_file gz(FILENAME, "w");

  HighScoresEntry hse;
  hse.score = 780;
  hse.serialize(gz);
  
  struct gzip_file gz2(FILENAME, "r");
  HighScoresEntry hse2;
  hse2.deserialize(gz2);

  ASSERT_EQ( hse2.score,  hse.score);
  */
}
