#ifndef __MORGUE_HPP__
#define __MORGUE_HPP__

#include <string>
#include <fstream>

using namespace std;

class Morgue
{
public:
  Morgue();

protected:
  void checkDirectory(const string&);
  string getFilename(const string&);

  void addHeader(ofstream&);
  void createSymlink(const string&, const string&);

  std::string getTr(const string&, const string&);
  std::string getDebris(void);

};

#endif // ! __MORGUE_HPP__
