#ifndef _POSITION_HPP_
#define _POSITION_HPP_

class Position
{
public:
  Position();
  Position(const int X, const int Y);
  
  void bounds_check();
  void bounds_check(int w, int h);

  int getX(void) const;
  int getY(void) const;
  void setX(int);
  void setY(int);
  
  // Shouldn't be public
  // Can't for instance, see ROADMAP
  int x, y;	

};

#endif // !_POSITION_HPP_
