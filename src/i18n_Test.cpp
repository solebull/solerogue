#include "i18n.hpp"

#include <gtest/gtest.h>


TEST( TranslationManager, getHelpDir )
{
  TranslationManager::getInstance().setCountryCode("en");
  ASSERT_EQ( TranslationManager::getInstance().getHelpDir(), "help/en/" );

  TranslationManager::getInstance().setCountryCode("fr");
  ASSERT_EQ( TranslationManager::getInstance().getHelpDir(), "help/fr/" );
}
