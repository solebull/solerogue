
#include "MapOutOfBounds.hpp"

MapIndexOutOfBoundsException::MapIndexOutOfBoundsException(int idx)
{
  w = "Map index out of bound : " + to_string(idx);
}
  
const char*
MapIndexOutOfBoundsException::what() const throw()
{
  return w.c_str();
}
