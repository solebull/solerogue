#ifndef _MAP_OUT_OF_BOUNDS_EXCEPTION_HPP_
#define _MAP_OUT_OF_BOUNDS_EXCEPTION_HPP_

#include <exception>
#include <string>

using namespace std;


/** The requested index is out of bounds in the map vector */
class MapIndexOutOfBoundsException: public exception
{
public:
  MapIndexOutOfBoundsException(int x);
  virtual const char* what() const throw();

private:
  string w;
};


#endif // !_MAP_OUT_OF_BOUNDS_EXCEPTION_HPP_
