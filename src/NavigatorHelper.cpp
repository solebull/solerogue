#include "NavigatorHelper.hpp"

int
NavigatorHelper::get_x(const Position &loc)
{
  return loc.getX();
}

int
NavigatorHelper::get_y(const Position &loc)
{
  return loc.getY();
}

Position
NavigatorHelper::get_xy(const int &x, const int &y)
{
  return Position{x,y};
}
