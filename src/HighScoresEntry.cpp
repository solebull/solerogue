#include "HighScoresEntry.hpp"

#include "api/Logger.hpp"

#include <ctime> // USES time()

HighScoresEntry::HighScoresEntry():
  score(0),
  movesNb(0),
  maxDungeonDepth(0),
  currentGame(false),
  level(1)
{
  epoch = std::time(nullptr); // NOW
  
}

void
HighScoresEntry::serialize(struct gzip_file& g) const
{
  if (playedTime.empty())
    {
      LOG(LogLevel::Warning,"About to serialize an entry with empty PlayedTime",
	  playedTime.c_str());
      //      return; // Used to remove incomplete past scores
    }

  g.serialize<int>(score);
  g.serialize<int>(level);
  g.serialize<int>(movesNb);
  g.serialize<int>(maxDungeonDepth);
  g.serialize<time_t>(epoch);
  g.serialize<string>(name);
  g.serialize<string>(playedTime);
  g.serialize<string>(killedBy);
}

void
HighScoresEntry::deserialize(struct gzip_file& g)
{
  g.deserialize<int>(score);
  g.deserialize<int>(level);
  g.deserialize<int>(movesNb);
  g.deserialize<int>(maxDungeonDepth);
  g.deserialize<time_t>(epoch);
  g.deserialize<string>(name);
  g.deserialize<string>(playedTime);
  if (playedTime.empty())
    {
      LOG(LogLevel::Warning, "Deserialized PlayedTime is %s",
	  playedTime.c_str());
    }
  g.deserialize<string>(killedBy);
}

void
HighScoresEntry::setCurrentGame(bool cg)
{
  currentGame = cg;
}

bool
HighScoresEntry::isCurrentGame() const
{
  return currentGame;
}

time_t
HighScoresEntry::getEpoch() const
{
  return epoch;
}

void
HighScoresEntry::setKilledBy(const string& kb)
{
  this->killedBy = kb;
}

const string&
HighScoresEntry::getKilledBy(void)const
{
  return this->killedBy;
}

void
HighScoresEntry::setLevel(int l)
{
  if (l < 0)
    {
      LOG(LogLevel::Error, "Trying to set negative level '%d'", l);
      throw runtime_error("Can't set negative level");

    }
  
  level = l;
}

int
HighScoresEntry::getLevel(void) const
{
  return level;
}


void
HighScoresEntry::setPlayedTime(const string& pt)
{
  if (pt.empty())
    {
      LOG(LogLevel::Warning, "Played time set to empty string");
    }
  
  this->playedTime = pt;
}

const string&
HighScoresEntry::getPlayedTime(void)const
{
  return this->playedTime;
}
