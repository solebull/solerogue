#ifndef _MAP_MANAGER_HPP_
#define _MAP_MANAGER_HPP_

#include <vector>
#include "api/Map.hpp"
#include "Singleton.hpp"

#include "messages/ActorMoved.hpp" // USES ActorMovedDirection

using namespace std;

class MapManager final : public Singleton<MapManager>
{
public:
  MapManager(token);

  Map* getCurrentMap();
  Map* getMapByIndex(int);
  
  void go(ActorMovedDirection);
  
  int getDungeonLevel(void)  const;
  int getDeeperDungeon(void) const;

  const vector<Map>& getMaps(void) const;
  
protected:
  void newMap(void);
  
private:
  vector<Map> maps;
  size_t currentMap;
  int deeperDungeon;  //!< The deeper dungeon the player reach
};

#endif // !_MAP_MANAGER_HPP_
