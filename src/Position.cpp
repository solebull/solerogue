#include "Position.hpp"

#include "MapManager.hpp"

Position::Position() {}
Position::Position(const int X, const int Y) :
  x(X), y(Y)
{
  
}

void
Position::bounds_check()
{
  auto m = MapManager::getInstance().getCurrentMap();
  int w = (int)m->getWidth();
  int h = (int)m->getHeight();
  bounds_check(w, h);
}

int
Position::getX(void) const
{
  return x;
}

int
Position::getY(void) const
{
  return y;
}

void
Position::setX(int vX)
{
  x = vX;
}

void
Position::setY(int vY)
{
  y = vY;
}
  
void
Position::bounds_check(int w, int h)
{
  if (x <  0)  x = 0;
  if (x >= w)  x = w - 1;
  if (y <  0)  y = 0;
  if (y >= h)  y = h - 1;
}

