#ifndef __HIGHSCORES_FORMATTER_HPP__
#define __HIGHSCORES_FORMATTER_HPP__

#include "HighScoresEntry.hpp"

/** A superclass used to implement new HighScores formatter
  *
  */
class HighScoresFormatter
{
public:
  virtual ~HighScoresFormatter(){};

  virtual void start(void)=0;                   //!< Very start of the list
  virtual void print(const HighScoresEntry&)=0; //!< Called for each score
  virtual void end(void)=0;                     //!< Very end of the list
};
#endif // !__HIGHSCORES_FORMATTER_HPP__
