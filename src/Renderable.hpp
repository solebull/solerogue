#ifndef _RENDERABBLE_HPP_
#define _RENDERABBLE_HPP_

#include "rltk.hpp"

using namespace rltk;
using namespace rltk::colors;

class Renderable {
public:
  Renderable();
  Renderable(const char Glyph, const color_t foreground);
  int glyph; 
  color_t fg=colors::WHITE; 
  color_t bg=colors::BLACK; 
};

#endif // !_RENDERABBLE_HPP_
