#ifndef _PLAYED_TIME_HPP_
#define _PLAYED_TIME_HPP_

#include <string>
#include <chrono>

using namespace std;

// Forward declaration
class PlayedTime;
// End of forward declaration


extern PlayedTime playedTime;

class PlayedTime
{
public:
  PlayedTime();

  string str();
  
protected:
  string durationToString(chrono::duration<double>)const;
  
private:
  chrono::time_point<std::chrono::system_clock> ela_start;

  string lastTime; //!< Last computed time
  
};
#endif // !_PLAYED_TIME_HPP_


