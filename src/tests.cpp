
#include <gtest/gtest.h>

#include "api/Logger.hpp"


int main(int ac, char* av[])
{
  // Logger instanciation
  static_logger = new Logger("unit-tests.log", LogLevel::Debug, LOG_MEDIA_FILE);

  testing::InitGoogleTest(&ac, av);
  return RUN_ALL_TESTS();
}
