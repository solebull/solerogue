#include "HelpViewer.hpp"


#include "config.h"
#include "i18n.hpp"      // USES default_font
#include "MdParser.hpp"

#include "api/Logger.hpp"

#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <fstream>
#include <streambuf>

#ifdef _WIN32
#  include <windows.h>
#endif

using namespace std;

int hv_configure_call = 1;

HelpViewerUi::HelpViewerUi(token):
  visible(false),
  width(800),
  height(600),
  cursor(0),
  dialog_color(sf::Color(80, 80, 80, 220)),
  border_dev(4)
{

}
 
HelpViewerUi::~HelpViewerUi()
{

}

void
HelpViewerUi::configure()
{
  LOG(LogLevel::Error, "Calling HelpViewerUi::configure %d", hv_configure_call++);
  /*  auto win = rltk::get_window()->getSize();
  resize(win.x, win.y);
  */
  
  // Configure drawables
  sf::RectangleShape* rectangle = new sf::RectangleShape();
  rectangle->setSize(sf::Vector2f(width, height));
  rectangle->move(x, y);
  rectangle->setFillColor(dialog_color);
  drawables.push_back(rectangle);
  
  sf::RectangleShape* border = new sf::RectangleShape();
  border->setSize(sf::Vector2f(width + (border_dev * 2),
			     height + (border_dev * 2)));
  border->move(x - border_dev, y - border_dev);
  border->setOutlineColor(dialog_color);
  border->setOutlineThickness(1);
  border->setFillColor(sf::Color(255, 255, 255, 0)); // 0 to only show outline
  drawables.push_back(border);


  auto title_color = sf::Color(180, 180, 180, 255);
  auto title_border_color = sf::Color(180, 180, 255, 255);
  sf::RectangleShape* title_rect = new sf::RectangleShape();
  title_rect->setSize(sf::Vector2f(300, 30));
  title_rect->move(x + 80, y - 15);
  title_rect->setOutlineColor(title_border_color);
  title_rect->setFillColor(title_color);
  drawables.push_back(title_rect);

  sf::Text* t1 = new sf::Text("Help Viewer", default_font, 15);
  t1->move(x + 100, y - 10);
  t1->setFillColor(sf::Color(100, 100, 255, 255));
  drawables.push_back(t1);
  
  if (actual_file.empty())
    actual_file = mdfiles.begin()->second;
}

/** If can't open dirname, will open fallback instead */
void
HelpViewerUi::openHelpDir(const std::string& dirname, const string& fallback,
			  const string& modname)
{
  DIR *dir;
  struct dirent** ent;
  if ((dir = opendir (dirname.c_str())) != nullptr) {
    // index page is a special case
    addFile(getModName(modname, "index"), dirname + "index.md");

#ifdef _WIN32
    string fn = dirname + "/*";
    WIN32_FIND_DATA data;
    HANDLE hFind = FindFirstFile(dirname.c_str(), &data);  // FILES
    if ( hFind != INVALID_HANDLE_VALUE ) {
        do
	  {
	    char* na = data.cFileName;
	    if (na[0] != '.' &&  na[strlen(na) - 1] != '~')
	      {
		if (strcmp(na, "index.md") != 0) // && !modname.empty())
		  {
		    string moddir = dirname + na;
		    addFile(getModName(modname, na), moddir);
		  }
	      }
	  }
	while (FindNextFile(hFind, &data));
        FindClose(hFind);
    }
#else    
    /* print all the files and directories within directory */
    int nbfiles = scandir(dirname.c_str(), &ent, NULL, alphasort);
    while (nbfiles--)
      {
	char* na = ent[nbfiles]->d_name;
	
	if (na[0] != '.' &&  na[strlen(na) - 1] != '~')
	  {
	    if (strcmp(na, "index.md") != 0) // && !modname.empty())
	      {
		string moddir = dirname + na;
		addFile(getModName(modname, na), moddir);
	      }
	  }
      }
    closedir (dir);
#endif
  }
  else
    {
      if (dirname != fallback)
	{
	  LOG(LogLevel::Info, "'%s' is empty, opening 'help/en/' instead",
	      dirname.c_str());
	  openHelpDir(fallback, fallback, modname);
	}
      else
	{
	  if (fallback == "help/en/")
	    throw std::runtime_error("Can't open " + dirname);
	  else
	    LOG(LogLevel::Info, "Can't open help files at '%s'",
		fallback.c_str());
	}
    }
}


void
HelpViewerUi::draw()
{
  if (!visible) return;
  
  auto window = rltk::get_window();

  for (auto d: drawables)
    window->draw(*d);

  // Print pages
  int yy = 0;
  for (auto m : mdfiles)
    {
      sf::Color c = cursor == yy ? sf::Color::Red : sf::Color::White;
      print(nullptr, x, y, 5, 1 + yy++, m.first, c);
    }

  showFile(actual_file);
}

/** Resize according to the given window dimensions */
void
HelpViewerUi::resize(unsigned int w, unsigned int h)
{
  x = (w - width) / 2;
  y = (h - height) / 2;
  layerx = x + 140;
  layery = y;
  drawables.clear();   // Remove actual rectangles
  configure();
}

void
HelpViewerUi::toggleVisibility()
{
  visible = !visible;
}

void
HelpViewerUi::addFile(const std::string& key, const std::string& file)
{
  mdfiles.emplace_back(make_pair(key, file));
}

void
HelpViewerUi::cursorChanged()
{
  int s = mdfiles.size();
  if (cursor > s - 1)
    cursor = 0;
  if (cursor < 0)
    cursor =  s - 1;

  // Handle index
  int i = 0;
  for (auto m : mdfiles)
    if (cursor == i++)
      {
	actual_file = m.second;
	LOG(LogLevel::Info, "Actual help file is now '%s'",
	    actual_file.c_str());
      }
}

void
HelpViewerUi::action()
{
  //
}

void
HelpViewerUi::cursorUp()
{
  if (!visible) return;

  cursor--;
  cursorChanged();
}

void
HelpViewerUi::cursorDown()
{
  if (!visible) return;

  cursor++;
  cursorChanged();
}

void
HelpViewerUi::showFile(const std::string& filename)
{
  if (filename != shown_file)
    {
      std::ifstream t(filename);
      std::string str;
      
      t.seekg(0, std::ios::end);   
      str.reserve(t.tellg());
      t.seekg(0, std::ios::beg);

      str.assign((std::istreambuf_iterator<char>(t)),
		 std::istreambuf_iterator<char>());

      shown_file = filename;
      shown_file_content = str;
    }

  MdParser md;
  int ret = md.parse(shown_file_content);
  if (ret != 0)
    LOG(LogLevel::Error, "ERROR reading md file : %s ", ret);
  
}


int
HelpViewerUi::getX() const
{
  return x;
}

int
HelpViewerUi::getY() const
{
  return y;
}

std::string
HelpViewerUi::removeExtension(const std::string& str)
{
  size_t idx = str.find_last_of(".");
  return str.substr(0, idx);
}

std::string
HelpViewerUi::getModName(const std::string& mod, const std::string& file)
{
  string f = removeExtension(file);
  
  if (mod.empty())
    return f;
  else
    return mod + "::" + f;
}

