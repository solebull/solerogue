#include "Form.hpp"

#include "rltk.hpp"

using namespace std;

Form::Form(int w, int h):
  width(w),
  height(h),
  x(0),
  y(0)
{
  
}

void
Form::center()
{
  auto win = rltk::get_window()->getSize();
  center(win.x, win.y);
}

void
Form::center(unsigned int wx, unsigned int wy)
{
  x = (wx - width) / 2;
  y = (wy - height) / 2;

}

void
Form::resize(unsigned int wx, unsigned int wy)
{
  center(wx, wy);
}
