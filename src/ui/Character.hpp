#ifndef _CHARACTER_UI_HPP_
#define _CHARACTER_UI_HPP_

#include "ui/Form.hpp"

#include <string>
#include <list>

#include "rltk.hpp"

class CharacterUi : public Form
{
public:
  CharacterUi();
  
  virtual void configure() override;
  virtual void draw() override;

  virtual void cursorUp() override;
  virtual void cursorDown() override;
  virtual void cursorLeft() override;
  virtual void cursorRight() override;

  virtual bool action() override;
  
  virtual void toggleVisibility(const std::string&) override;
  virtual void resize(unsigned int, unsigned int) override;

private:
  bool visible;
  std::list<sf::Drawable*> drawables; // Automatically drawn in draw()

  int cursor;
  int actionCursor;
};

#endif // !_CHARACTER_UI_HPP_
