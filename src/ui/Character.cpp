#include "Character.hpp"

#include "config.h"      // USES _()
#include "i18n.hpp"      // USES default_font

#include "api/PlayerId.hpp" // USES player_id
#include "api/Entity.hpp"
#include "api/Weapon.hpp"

#include "components/Inventory.hpp"
#include "components/Character.hpp"

using namespace std;

CharacterUi::CharacterUi():
  Form(800, 550),
  visible(false),
  cursor(0),
  actionCursor(0)
{
  center();
}

void
CharacterUi::configure()
{
  sf::Color dialog_color(120, 120, 170, 220);
  
  // Background
  sf::RectangleShape* rectangle = new sf::RectangleShape();
  rectangle->setSize(sf::Vector2f(width, height));
  rectangle->move(x, y);
  rectangle->setFillColor(dialog_color);
  drawables.push_back(rectangle);

  int border_dev = 4;

  sf::RectangleShape* border = new sf::RectangleShape();
  border->setSize(sf::Vector2f(width + (border_dev * 2),
			     height + (border_dev * 2)));
  border->move(x - border_dev, y - border_dev);
  border->setOutlineColor(dialog_color);
  border->setOutlineThickness(1);
  border->setFillColor(sf::Color(255, 255, 255, 0)); // 0 to only show outline
  drawables.push_back(border);

  // Title
  auto title_color = sf::Color(180, 180, 180, 255);
  auto title_border_color = sf::Color(180, 180, 255, 255);
  sf::RectangleShape* title_rect = new sf::RectangleShape();
  title_rect->setSize(sf::Vector2f(300, 30));
  title_rect->move(x + 80, y - 15);
  title_rect->setOutlineColor(title_border_color);
  title_rect->setFillColor(title_color);
  drawables.push_back(title_rect);

  sf::Text* t1 = new sf::Text(_("Character"), default_font, 15);
  t1->move(x + 100, y - 10);
  t1->setFillColor(sf::Color(100, 100, 255, 255));
  drawables.push_back(t1);
}

void
CharacterUi::draw()
{
  if (!visible) return;
  
  auto window = rltk::get_window();
  
  for (auto d: drawables)
    window->draw(*d);
}


void
CharacterUi::toggleVisibility(const std::string& elem)
{
  if (elem == "Character")
    visible = !visible;
}

void
CharacterUi::cursorUp()
{
  if (!visible) return;

  cursor++;
  /*  if (cursor >= weaponListSize)
    cursor = 0;
  */
}

void
CharacterUi::cursorDown()
{
  if (!visible) return;

  cursor--;
  /*  if (cursor < 0)
    cursor = weaponListSize - 1;
  */
}


void
CharacterUi::cursorLeft()
{
  if (!visible) return;

  actionCursor--;
  if (actionCursor < 0)
    actionCursor = 1;
}

void
CharacterUi::cursorRight()
{
  if (!visible) return;

  actionCursor++;
  if (actionCursor > 1)
    actionCursor = 0;
}

bool
CharacterUi::action()
{
  if (!visible) return false;

  return false;
}

/** Resize and re-center the Character form
  *
  * \param w The new SFML window width
  * \param w The new SFML window height
  *
  */
void
CharacterUi::resize(unsigned int w, unsigned int h)
{
  Form::resize(w, h);
  drawables.clear();
  configure();
}


