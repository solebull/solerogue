#include "Highscores.hpp"

#include "api/Logger.hpp"

#include "systems/Player.hpp" // USES playerTitle

#include "api/PlayerId.hpp" // USES player_id
#include "api/Attributes.hpp"

#include "MapManager.hpp"   // USED in add() function
#include "Score.hpp"

#include "HighScoresFormatter.hpp"
#include "formatters/highscores/Text.hpp"
#include "formatters/highscores/Json.hpp"

#include <tgmath.h>  // USES ceil()
#include <algorithm> // USES std::find()

using namespace std;

const string FILENAME="highscores.gz";
constexpr int HIGHSCORE_VERSION = 3;

bool
compare_scores(const HighScoresEntry& first, const HighScoresEntry& second)
{
  return ( first.score > second.score );
}

/** The Highscore ui form constructor
  *
  *
  * \param test Used in unit tests to use an empty scores list
  *
  */
HighscoresUi::HighscoresUi(bool test):
  Form(800, 540),
  filename(FILENAME),
  visible(true),
  version(HIGHSCORE_VERSION),
  currentPage(1),
  maxPage(-1),
  scoresPerPage(23),
  tPages(NULL),
  playerIsDead(false)
{
  auto w = rltk::get_window();
  // w can be NULL if we're running in console (without GUI)
  if (w)
    y = w->getSize().y - 120;

  if (test)
    filename = FILENAME+"-test";
  else
    load();
  
  maxPage = computeMaxPage(scores.size());
			   
  // Copy first page
  int i=0;
  for (auto& s:scores)
    {
      pageScores.push_back(s);
      i++;
      if ( i>= scoresPerPage) break;
    }
  
  configure();
}

void
HighscoresUi::configure()
{
  int border_dev = 4;
  sf::Color dialog_color(120, 150, 32, 220);

  auto w = rltk::get_window();
  // Early exit if running in console
  if (!w)
    return ;

  y = (w->getSize().y - height) / 2;
  x = (w->getSize().x - width) / 2;

  // Background
  sf::RectangleShape* rectangle = new sf::RectangleShape();
  rectangle->setSize(sf::Vector2f(width, height));
  rectangle->move(x, y);
  rectangle->setFillColor(dialog_color);
  drawables.push_back(rectangle);

  auto border = new sf::RectangleShape(sf::Vector2f(width + border_dev*2,
						    height + border_dev*2));
  border->move(x - border_dev, y - border_dev);
  border->setOutlineColor(dialog_color);
  border->setOutlineThickness(1);
  border->setFillColor(sf::Color(255, 255, 255, 0)); // 0 to only show outline
  drawables.push_back(border);

  // Title
  auto title_color = sf::Color(180, 180, 180, 255);
  auto title_border_color = sf::Color(180, 180, 255, 255);
  sf::RectangleShape* title_rect = new sf::RectangleShape();
  title_rect->setSize(sf::Vector2f(300, 30));
  title_rect->move(x + 80, y - 15);
  title_rect->setOutlineColor(title_border_color);
  title_rect->setFillColor(title_color);
  drawables.push_back(title_rect);

  sf::Text* t1 = new sf::Text(_("HighScores"), default_font, 15);
  t1->move(x + 100, y - 10);
  t1->setFillColor(sf::Color(100, 100, 255, 255));
  drawables.push_back(t1);

  // Headers
  /*  sf::Text* h1 = new sf::Text(_("Name"), default_font, 15);
  h1->move(x + 20, y + 20);
  h1->setFillColor(sf::Color(0, 0, 25, 255));
  drawables.push_back(h1);
  */
  
  addHeader(_("Idx"), x+20);
  addHeader(_("Name"), x+100);
  addHeader(_("Level"), x+180);
  addHeader(_("Killed by"), x+200);
  addHeader(_("Score"), x+300);
  addHeader(_("Depth"), x+360);
  addHeader(_("Moves"), x+420);
  addHeader(_("Time"), x+520);
  addHeader(_("Date/Time of play"), x+620);
  
  //Actions
  tPages = new sf::Text("", default_font, 15);
  tPages->move(x + 10, y + height - 25);
  tPages->setFillColor(sf::Color(0, 0, 25, 255));
  drawables.push_back(tPages);
  setPages(1);
}

void HighscoresUi::addHeader(const std::string& text, int x)
{
  auto t2 = get_wstring(text);
  sf::Text* h1 = new sf::Text(t2, default_font, 15);
  h1->move(x, y + 20);
  h1->setFillColor(sf::Color(0, 0, 25, 255));
  drawables.push_back(h1);
}

void
HighscoresUi::draw()
{
  if (!visible) return;

  auto window = rltk::get_window();
  for (auto d: drawables)
    window->draw(*d);

  int py = y + 40;
  int idx = 0 + ((currentPage - 1) * scoresPerPage);
  for (auto s:pageScores)
    {
      auto col = sf::Color(0, 0, 25, 255);
      if (s.isCurrentGame())
	col = sf::Color(0, 255, 25, 255);
	  
      ostringstream oss;
      oss << std::setfill(' ') << std::setw(3) << ++idx;
      sf::Text p1(oss.str(), default_font, 15);
      p1.move(x + 20, py);
      p1.setFillColor(col);
      window->draw(p1);

      sf::Text p2(truncate(s.name, 13), default_font, 15);
      p2.move(x + 60, py);
      p2.setFillColor(col);
      window->draw(p2);

      sf::Text p3(std::to_string(s.getLevel()), default_font, 15);
      p3.move(x + 190, py);
      p3.setFillColor(col);
      window->draw(p3);

      sf::Text p4(truncate(s.getKilledBy(), 10), default_font, 15);
      p4.move(x + 210, py);
      p4.setFillColor(col);
      window->draw(p4);

      sf::Text p5(std::to_string(s.score), default_font, 15);
      p5.move(x + 310, py);
      p5.setFillColor(col);
      window->draw(p5);

      sf::Text p6(std::to_string(s.maxDungeonDepth), default_font, 15);
      p6.move(x + 380, py);
      p6.setFillColor(col);
      window->draw(p6);

      sf::Text p7(std::to_string(s.movesNb), default_font, 15);
      p7.move(x + 430, py);
      p7.setFillColor(col);
      window->draw(p7);

      sf::Text p8(s.getPlayedTime(), default_font, 15);
      p8.move(x + 520, py);
      p8.setFillColor(col);
      window->draw(p8);

      char buf[80];
      time_t now = s.getEpoch();
      struct tm ts = *localtime(&now);
      strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M", &ts);
      string s9(buf);
      
      sf::Text p9(s9, default_font, 15);
      p9.move(x + 620, py);
      p9.setFillColor(col);
      window->draw(p9);
      
      py += 20;
    }
}

void
HighscoresUi::cursorUp()
{

}

void
HighscoresUi::cursorDown()
{

}

void
HighscoresUi::cursorLeft()
{
  if (playerIsDead) return;  // Simplest early exit
  
  currentPage--;
  if (currentPage < 1)
    currentPage = 1;
  setPages(currentPage);
}

void
HighscoresUi::cursorRight()
{
  if (playerIsDead) return;  // Simplest early exit

  currentPage++;
  if (currentPage > maxPage)
    currentPage = maxPage;
  setPages(currentPage);
}

bool
HighscoresUi::action()
{
  return false;
}

void
HighscoresUi::toggleVisibility(const std::string& s)
{
  if (s=="HighScores")
    {
      // Trying to get player's health status
      if (!PlayerSystem::isAlive())
	{
	  // Player is dead :
	  //   don't handle pagination
	  //   show its insertion page
	  playerIsDead = true;
	  auto i = add();
	  setPages(computeMaxPage(i));
	  tPages->setString("Pagination disabled : you're dead. "
			    "You can close the game.");
	}
      
      
      LOG(LogLevel::Debug, "Toggle visibility of HighscoresUi");
      visible = !visible;
    }
}

void
HighscoresUi::resize(unsigned int w, unsigned int h)
{
  Form::resize(w*0.8, h*0.8);
  configure();
}

void
HighscoresUi::update(double duration_ms)
{

}

/** Used to set the text of the pages text
 *
 */
void
HighscoresUi::setPages(int current)
{
  // Update label
  string page = "w/x - page " + to_string(current) + "/" + to_string(maxPage);
  tPages->setString(page);

  // Update list
  int from = ((current - 1) * scoresPerPage ) + 1;
  int to = from + (scoresPerPage - 1);
  LOG(LogLevel::Debug, "Printing score from %d to %d", from, to);

  pageScores.clear();
  
  int i=1;
  for (auto& s:scores)
    {
      if (i>=from && i<=to)
	pageScores.push_back(s);
      
      i++;
    }
}

int
HighscoresUi::add()
{
  LOG(LogLevel::Debug, "HighScoresSystem::add called for %s",
      playerName.c_str());
  auto sc = entity(player_id)->component<Score>(); 
  auto att = entity(player_id)->component<Attributes>();
  
  HighScoresEntry s;
  s.name       = playerName + " " + playerTitle;
  s.score      = sc->compute();
  s.setPlayedTime(sc->playedTime);
  s.movesNb    = sc->nbMoves;
  s.setCurrentGame(true);
  s.maxDungeonDepth = MapManager::getInstance().getDeeperDungeon();
  s.setKilledBy(sc->getKilledBy());
  s.setLevel(att->getLevel("general").getLevel());

  return addScore(s);
}

void
HighscoresUi::sort()
{
  scores.sort(compare_scores);
}


void
HighscoresUi::save()
{
  LOG(LogLevel::Info, "Saving Highscores in '%s'", filename.c_str());
  struct gzip_file gz(filename, "w");
  serialize(gz);
}

void
HighscoresUi::serialize(struct gzip_file& g)
{
  g.serialize(version);
  
  size_t sz = scores.size();
  LOG(LogLevel::Debug, "Serializing %d scores", sz);
  g.serialize<size_t>(sz);
  for (auto const& s: scores)
    s.serialize(g);
}

void
HighscoresUi::load()
{
  try
    {
      struct gzip_file gz(filename, "r");
      deserialize(gz);
    }
  catch (const std::runtime_error& e)
    {
      // Search for a doesn't exist error from rltk
      string s(e.what());
      if (s.find("does not exist.") != std::string::npos)
	{
	  LOG(LogLevel::Error,
	      _("Highscores file (%s) doesn't exist. First launch ?").c_str(),
	      filename.c_str());
	}
      else
	{
	  throw std::runtime_error("Highscores file loading error");
	}
    }
  catch(const std::exception& e)
    {
      // Simply ignore an obscure RLTK error from
      // ../ext/rltk/rltk/serialization_utils.hpp's
      // throw_gzip_exception(gzFile &gzfile) function
      if (std::strcmp(e.what(), "0 ") != 0)
	{
	  LOG(LogLevel::Error,
	      _("Highscores file (%s) deserialization error : '%s'").c_str(),
	      filename.c_str(), e.what());
	  throw std::runtime_error("Highscores file loading error");
	}
    }  
}

void
HighscoresUi::deserialize(struct gzip_file& g)
{
  int sver;
  g.deserialize(sver);
  assert(sver==version && "HighScores version mismatch");

  size_t sz;
  g.deserialize<size_t>(sz);
  
  for (size_t i=0; i < sz; ++i)
    {
      HighScoresEntry s;
      s.deserialize(g);
      scores.push_back(s);
    }
}

void
HighscoresUi::print(const std::string&  vFormat)
{
  // To lower case
  string format = vFormat;
  std::transform(format.begin(), format.end(), format.begin(), ::tolower);
  // Generalisation
  HighScoresFormatter* hsf;
  
  if (format == "text")
    {
      hsf = new HighScoresTextFormatter();
    }
  else if (format == "json")
    {
      hsf = new HighScoresJsonFormatter();
    }
  else
    {
      cout << "'" << format << "' highscores formatter not allowed!" << endl;
      return;
    }

  hsf->start();
  for (auto const& s: scores)
    hsf->print(s);
  hsf->end();
  delete hsf;
  
}

string
HighscoresUi::truncate(const string& str, long unsigned int width,
		       bool show_ellipsis)
{
  if (str.length() > width)
    {
      if (show_ellipsis)
	return str.substr(0, width - 3) + "...";
      else
	return str.substr(0, width);
    }
  return str;
}

/** Compute the maxPage value based on a number of loaded highscores
  *
  */
int
HighscoresUi::computeMaxPage(int hsn)
{
  double highscoresPerPage = (double)scoresPerPage;
  return ceil(hsn / highscoresPerPage);
}

int
HighscoresUi::getScoresPerPage(void)
{
  return scoresPerPage;
}

void
HighscoresUi::setScoresPerPage(int v)
{
  scoresPerPage = v;
}

/** Add the given score to the scores list then sort it
  *
  * \return The index of the inserted score once list is sorted.
  *
  */
int
HighscoresUi::addScore(const HighScoresEntry& s)
{
  scores.push_back(s);
  sort();
  save();

  // Trying to get the inserted index
  auto iter = std::find(scores.begin(), scores.end(), s);
  if (iter == scores.end())
    return -1;
  auto dist = std::distance(scores.begin(), iter) + 1;
  return  dist;
}

/** Returns a non-modifiable list of all known scores
  *
  * \return a STL list of HighScoresEntry objects.
  *
  */
const std::list<HighScoresEntry>&
HighscoresUi::getScores(void) const
{
  return scores;
}
