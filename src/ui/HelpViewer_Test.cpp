#include "HelpViewer.hpp"
#include <gtest/gtest.h>

TEST( HelpViewer, without_ext )
{
  ASSERT_EQ( HelpViewerUi::removeExtension("index.md"),  "index");
  ASSERT_EQ( HelpViewerUi::removeExtension("index"),  "index");
  ASSERT_EQ( HelpViewerUi::removeExtension("filename.test"),  "filename");
}

TEST( HelpViewer, modname )
{
  ASSERT_EQ( HelpViewerUi::getModName("","index.md"),  "index");
  ASSERT_EQ( HelpViewerUi::getModName("cat","index.md"),  "cat::index");
  ASSERT_EQ( HelpViewerUi::getModName("cat","Aze.md"),  "cat::Aze");
}
