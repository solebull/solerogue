#ifndef _LOG_UI_HPP_
#define _LOG_UI_HPP_

#include "ui/Form.hpp"

#include "messages/AddLogEntry.hpp"

#include <list>
#include <string>

#include "rltk.hpp"

/** Used to store log entries in LogUi's list without keeping full rltk message
  *
  */
struct LogEntry
{
  int nbMoves;          //!< Number of steps the player played
  std::string time;     //!< The time the event arrived at
  std::wstring message; //!< The message to be printed to screen
};

/** The log panel UI form
  *
  *
  *
  */
class LogUi : public Form
{
public:
  LogUi();
  
  virtual void configure() override;
  virtual void draw() override;

  virtual void cursorUp() override;
  virtual void cursorDown() override;
  virtual void cursorLeft() override;
  virtual void cursorRight() override;

  virtual bool action() override;
  
  virtual void toggleVisibility(const std::string&) override;
  virtual void resize(unsigned int, unsigned int) override;

  void add(AddLogEntryMessage);
  
private:
  bool visible;                       //!< Is this form visible ?
  std::list<sf::Drawable*> drawables; //!< Automatically drawn in draw()

  int cursor;                         //!< Cursor item index
  int actionCursor;                   //!< Index of the action cursor
  std::list<LogEntry*> messages;      //!< The list of log messages
};

#endif // !_LOG_UI_HPP_
