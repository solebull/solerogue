#ifndef _INFO_UI_HPP_
#define _INFO_UI_HPP_

#include "ui/Form.hpp"

#include <list>
#include <string>

#include "rltk.hpp"

#include "messages/ChangeMainFont.hpp"
#include "messages/ChangeNodeInfo.hpp"
#include "messages/ChangeObjectsInfo.hpp"

/** The log panel UI form
  *
  */
class InfoUi : public Form
{
public:
  InfoUi();
  
  virtual void configure() override;
  virtual void draw() override;

  virtual void cursorUp() override;
  virtual void cursorDown() override;
  virtual void cursorLeft() override;
  virtual void cursorRight() override;

  virtual bool action() override;
  
  virtual void toggleVisibility(const std::string&) override;
  virtual void resize(unsigned int, unsigned int) override;

  void update(double duration_ms);

  void onChangeObjectsInfo(const ChangeObjectsInfoMessage&);
  void onChangeMainFont(const ChangeMainFontMessage&);
  void onChangeNodeInfo(const ChangeNodeInfoMessage&);
  
protected:
  std::wstring concatName(const std::string&, const std::string&) const;
  
private:
  bool visible;                       //!< Is this form visible ?
  std::list<sf::Drawable*> drawables; //!< Automatically drawn in update()

  int cursor;                         //!< Cursor item index
  int actionCursor;                   //!< Index of the action cursor

  sf::Text* tLevel;  //!< The current char level
  sf::Text* tMoves;  //!< The number of moves
  sf::Text* tDung;   //!< The dungeon level
  sf::Text* tTime;   //!< The played time
  sf::Text* tReveal; //!< The revealed percent
  sf::Text* tArea;   //!< The map's area
  sf::Text* tFps;    //!< The current FPS

  sf::Text* tObjectInfo;
  sf::Text* tZoom;
  sf::Text* tNodeInfo;

  sf::Text* tWeaponL;  // Left-arm weapon
  sf::Text* tWeaponR;  // Right-arm weapon

  sf::Color activeWeapon;
  sf::Color inactiveWeapon;
  sf::Color textColor;
  
  double nextFPS;
  std::string lastFPS;
  std::string current_zoom;
  double zoom_time;
  std::wstring nodeInfo;
};

#endif // !_INFO_UI_HPP_
