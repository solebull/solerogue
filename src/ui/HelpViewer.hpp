#ifndef __HELP_VIEWER_HPP__
#define __HELP_VIEWER_HPP__

#include "rltk.hpp"

#include "Singleton.hpp"

#include <string>
#include <utility> // USES std:pair<>
#include <list>

/** ANot a ui/Form subclass because called indevidually by GuiSystem */
class HelpViewerUi : public Singleton<HelpViewerUi>
{
public:
  HelpViewerUi(token);
  virtual ~HelpViewerUi();

  /*
  virtual void update(double duration_ms) override;
  */
  void configure();
  void draw();

  void openHelpDir(const std::string&,
		   const std::string& fallback="help/en/",
		   const std::string& modname="");

  void resize(unsigned int, unsigned int);
  void toggleVisibility();

  void cursorUp();
  void cursorDown();
  void cursorChanged();
  void action();

  int getX() const;
  int getY() const;

  static std::string getModName(const std::string&, const std::string&);
  static std::string removeExtension(const std::string&);
  
protected:
  void addFile(const std::string&, const std::string&);
  void showFile(const std::string&);
  
private:
  bool visible;
  int x, y;
  int width;
  int height;

  std::list<std::pair<std::string, std::string>> mdfiles;
  int cursor;
  std::string actual_file;
  std::string shown_file;
  std::string shown_file_content;

  // Graphics settings
  sf::Color dialog_color;   // Dialog's background color
  int border_dev;           // Distance between main rectangle and border
  std::list<sf::Drawable*> drawables; // Automatically drawn in draw()
};

#endif // !__HELP_VIEWER_HPP__
