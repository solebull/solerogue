#include "ui/Highscores.hpp"

#include <gtest/gtest.h>
#include <iostream>

class TestableHighscoresUi : public HighscoresUi
{
public:
  TestableHighscoresUi():HighscoresUi(true){}
  int _computeMaxPage(int v){ return computeMaxPage(v); }
  int _addScore(const HighScoresEntry& hse){ return addScore(hse); }

  void dump(void)
  {
    cout << "== Dumping HighScores :" << endl;
    int index = 0;
    auto sc = getScores();
    for (auto s : sc)
      cout << "  " << ++index << " - " <<  s.score << endl;
  }
};

// Compute maxPage based on loaded highscores number
TEST( HighscoresUi, computeMaxPage )
{
  TestableHighscoresUi thu;
  thu.setScoresPerPage(20);
  ASSERT_EQ( thu._computeMaxPage(1), 1);
  ASSERT_EQ( thu._computeMaxPage(20), 1);
  ASSERT_EQ( thu._computeMaxPage(21), 2);
  ASSERT_EQ( thu._computeMaxPage(40), 2);
  ASSERT_EQ( thu._computeMaxPage(41), 3);
}

TEST( HighscoresUi, scoresPerPage )
{
  TestableHighscoresUi thu;
  thu.setScoresPerPage(100);
  ASSERT_EQ( thu.getScoresPerPage(), 100);
}

/** When in unit tests, scores list must be empty (in test)
  * We shoudln't load game's ones.
  *
  */
TEST( HighscoresUi, empty_scores )
{
  TestableHighscoresUi thu;
  // And we should have a getScores() getter function
  ASSERT_EQ( thu.getScores().size(), 0);
}

/** We should have a add_score and it should return the index we added
 * score at
 *
 */
TEST( HighscoresUi, add_score_index )
{
  TestableHighscoresUi thu;
  thu.setScoresPerPage(100);

  HighScoresEntry s1;  s1.score = 10;
  ASSERT_EQ( thu._addScore(s1), 1); // First and only score

  HighScoresEntry s2;  s2.score = 25;
  ASSERT_EQ( thu._addScore(s2), 1); // Better than first

  HighScoresEntry s3;  s3.score = 15;
  auto i3 = thu._addScore(s3);
  ASSERT_EQ(i3, 2); 

  HighScoresEntry s4;  s4.score = 5;
  auto i4 = thu._addScore(s4);
  ASSERT_EQ(i4, 4); // Poor boi, only third xD
}

