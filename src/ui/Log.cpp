#include "ui/Log.hpp"

#include "config.h"      // USES _()
#include "i18n.hpp"      // USES default_font

#include "api/Logger.hpp"


using namespace std;

int config_calls = 0;

/** The maximum number of printed recent messages
  *
  */
constexpr int MAX_MESSAGES = 4;

LogUi::LogUi():
  Form(1024, 120)
{
  visible = true;

  auto w = rltk::get_window();
  y = w->getSize().y - 120;
}

void
LogUi::configure() 
{
  LOG(LogLevel::Error, "LogUi::configure() called %d times", ++config_calls);
  
  sf::Color dialog_color(120, 120, 170, 220);
  messages.push_back(new LogEntry{0, "---", _W("Welcome to solerogue")});

  auto window = rltk::get_window();

  x = 0;
  y = window->getSize().y - height;
  width = window->getSize().x;
  height = 120;
  
  // Background
  sf::RectangleShape* rectangle = new sf::RectangleShape();
  rectangle->setSize(sf::Vector2f(width, height));
  rectangle->move(0, y);
  rectangle->setFillColor(dialog_color);
  drawables.push_back(rectangle);

  int border_dev = 4;

  sf::RectangleShape* border = new sf::RectangleShape();
  border->setSize(sf::Vector2f(width + (border_dev * 2),
			     height + (border_dev * 2)));
  border->move(x - border_dev, y - border_dev);
  border->setOutlineColor(dialog_color);
  border->setOutlineThickness(1);
  border->setFillColor(sf::Color(255, 255, 255, 0)); // 0 to only show outline
  drawables.push_back(border);

  // Title
  auto title_color = sf::Color(180, 180, 180, 255);
  auto title_border_color = sf::Color(180, 180, 255, 255);
  sf::RectangleShape* title_rect = new sf::RectangleShape();
  title_rect->setSize(sf::Vector2f(300, 30));
  title_rect->move(x + 80, y - 15);
  title_rect->setOutlineColor(title_border_color);
  title_rect->setFillColor(title_color);
  drawables.push_back(title_rect);

  sf::Text* t1 = new sf::Text(_("Log"), default_font, 15);
  t1->move(x + 100, y - 10);
  t1->setFillColor(sf::Color(100, 100, 255, 255));
  drawables.push_back(t1);
}

/** Print the form content on each frame
  *
  */
void
LogUi::draw() 
{
  if (!visible) return;
  
  auto window = rltk::get_window();
  
  for (auto d: drawables)
    window->draw(*d);

  int yt = y + 22;

  int ym = messages.size() > MAX_MESSAGES ? MAX_MESSAGES : messages.size();
  list<LogEntry*>::const_reverse_iterator it;
  for(it = messages.crbegin(); it != messages.crend(); ++it)
    {
      default_text.setFillColor(sf::Color(0, 0, 0, 255));
      default_text.setOutlineColor(sf::Color(0, 0, 0, 255));

      default_text.setString(to_string((*it)->nbMoves));
      sf::Vector2i vec(x + 20, yt);
      default_text.setPosition(window->mapPixelToCoords(vec));
      window->draw(default_text);

      default_text.setString((*it)->time);
      sf::Vector2i vec2(x + 80, yt);
      default_text.setPosition(window->mapPixelToCoords(vec2));
      window->draw(default_text);

      default_text.setString((*it)->message);
      sf::Vector2i vec3(x + 180, yt);
      default_text.setPosition(window->mapPixelToCoords(vec3));
      window->draw(default_text);
      
      yt += 20;
      --ym;
      if (ym<1) break;
    }
}

void
LogUi::cursorUp() 
{

}
void
LogUi::cursorDown() 
{

}
void
LogUi::cursorLeft() 
{

}
void
LogUi::cursorRight() 
{

}

bool
LogUi::action() 
{
  return false;
}

void
LogUi::toggleVisibility(const string&) 
{

}

void
LogUi::resize(unsigned int w, unsigned int h)
{
  Form::resize(w, h);
  drawables.clear();
  configure();
}

/** Add the given message */
void
LogUi::add(AddLogEntryMessage msg)
{
  messages.push_back(new LogEntry{msg.nbMoves, msg.time, get_wstring(msg.msg)});
  cout << msg.msg<< endl;
}
