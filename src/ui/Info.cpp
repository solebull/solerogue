#include "Info.hpp"

#include "config.h"      // USES _()
#include "i18n.hpp"      // USES default_font

#include "Score.hpp"
#include "MapManager.hpp"
#include "PlayedTime.hpp"
#include "api/Entity.hpp"

#include "api/PlayerId.hpp" // USES player_id
#include "api/Attributes.hpp"

#include "systems/Player.hpp" // USES playerName

#include "components/Character.hpp"

using namespace std;

constexpr double FPSupdate = 100; //!< Update FPS count every ms


// Default font and size
#define DF default_font,13

InfoUi::InfoUi():
  Form(400, 400),
  visible(true),
  cursor(0),
  actionCursor(0),
  activeWeapon(255, 255, 255),
  inactiveWeapon(0, 150, 0),
  textColor(0,180,0),
  nextFPS(0.0),
  lastFPS(""),
  current_zoom(""),
  zoom_time(0)

{

  
}

void
InfoUi::configure()
{
  int border_dev = 4;
  sf::Color dialog_color(1, 50, 32, 220);

  auto w = rltk::get_window();
  y = 18; // TitleLayer's height
  height = w->getSize().y - y - border_dev - 120; // 120 is Log height
  x = w->getSize().x - 250; // 200 is our own width
  width = 250;

  // Background
  sf::RectangleShape* rectangle = new sf::RectangleShape();
  rectangle->setSize(sf::Vector2f(width, height));
  rectangle->move(x, y);
  rectangle->setFillColor(dialog_color);
  drawables.push_back(rectangle);

  auto border = new sf::RectangleShape(sf::Vector2f(0, height));
  border->move(x - border_dev, y);
  border->setOutlineColor(dialog_color);
  border->setOutlineThickness(1);
  border->setFillColor(sf::Color(255, 255, 255, 0)); // 0 to only show outline
  drawables.push_back(border);

  // Labels
  int labinc = 12;
  int ly = 20;

  sf::Text* t1 = new sf::Text(concatName(_("Name"),playerName),DF);
  t1->move(x + 7, ly);
  t1->setFillColor(textColor);
  drawables.push_back(t1);
  ly += labinc;

  sf::Text* t2 = new sf::Text(concatName(_("Title"),playerTitle),DF);
  t2->move(x + 7, ly);
  t2->setFillColor(textColor);
  drawables.push_back(t2);
  ly += labinc;

  auto lvl = entity(player_id)->component<Attributes>()->getLevel("general")
    .getLevel();
  tLevel = new sf::Text(concatName(_("Level"),to_string(lvl)), DF);
  tLevel->move(x + 7, ly);
  tLevel->setFillColor(textColor);
  drawables.push_back(tLevel);
  ly += labinc;

  tMoves = new sf::Text(concatName(_("Moves"),to_string(nb_moves)), DF);
  tMoves->move(x + 7, ly);
  tMoves->setFillColor(textColor);
  drawables.push_back(tMoves);
  ly += labinc;

  int dId = MapManager::getInstance().getDungeonLevel();
  tDung = new sf::Text(concatName(_("Dungeon #"), to_string(dId)), DF);
  tDung->move(x + 7, ly);
  tDung->setFillColor(textColor);
  drawables.push_back(tDung);
  ly += labinc;

  tTime = new sf::Text(concatName(_("Time"), playedTime.str()), DF);
  tTime->move(x + 7, ly);
  tTime->setFillColor(textColor);
  drawables.push_back(tTime);
  ly += labinc;

  // Revealed %age : created with 0.0f placeholder here, updated in update()
  // with real value
  double pc = 0.0f;
  ostringstream oss3;
  oss3.setf( std::ios_base::fixed, std::ios_base::floatfield );
  oss3 << _("Revealed") << " : " << std::setprecision(2) << pc << "%";
  tReveal = new sf::Text(oss3.str(), DF);
  tReveal->move(x + 7, ly);
  tReveal->setFillColor(textColor);
  drawables.push_back(tReveal);
  ly += labinc;

  double area = MapManager::getInstance().getCurrentMap()->getArea();
  ostringstream oss4;
  oss4.setf( std::ios_base::fixed, std::ios_base::floatfield );
  oss4 << _("Map Area") << " : " << std::setprecision(3) << area << " ha";
  tArea = new sf::Text(oss4.str(), DF);
  tArea->move(x + 7, ly);
  tArea->setFillColor(textColor);
  drawables.push_back(tArea);
  ly += labinc;

  // Fps
  tFps = new sf::Text(concatName(_("FPS"), lastFPS), DF);
  tFps->move(x + 7, ly);
  tFps->setFillColor(textColor);
  drawables.push_back(tFps);
  ly += labinc;

  // NodeInfo
  ly += labinc;
  tNodeInfo = new sf::Text("", DF);
  tNodeInfo->move(x + 7, ly);
  tNodeInfo->setFillColor(sf::Color(180, 180, 215, 255));
  drawables.push_back(tNodeInfo);
  ly += labinc;
  
  // ObjectInfo
  ly += labinc;
  tObjectInfo = new sf::Text("", DF);
  tObjectInfo->move(x + 7, ly);
  tObjectInfo->setFillColor(sf::Color(180, 180, 215, 255));
  drawables.push_back(tObjectInfo);
  ly += labinc;

  // Zoom
  tZoom = new sf::Text("", DF);
  tZoom->move(x + 7, ly);
  tZoom->setFillColor(sf::Color(255, 255, 255, 255));
  drawables.push_back(tZoom);
  ly += labinc;

  ly += labinc;

  // Weapons
  tWeaponL = new sf::Text("", DF);;
  tWeaponL->move(x + 7, ly);
  tWeaponL->setFillColor(inactiveWeapon);
  drawables.push_back(tWeaponL);
  ly += labinc;
  tWeaponR = new sf::Text("", DF);;
  tWeaponR->move(x + 7, ly);
  tWeaponR->setFillColor(inactiveWeapon);
  drawables.push_back(tWeaponR);
  
}

// Name or other. In fact two strings
wstring
InfoUi::concatName(const string& a, const string& b) const
{
  ostringstream oss;
  oss << a  << " : " << b;
  return get_wstring(oss.str());
}

void
InfoUi::draw()
{
  if (!visible) return;

  auto window = rltk::get_window();
  
  for (auto d: drawables)
    window->draw(*d);

  // Draw level
  auto lvl = entity(player_id)->component<Attributes>()->getLevel("general")
    .getLevel();
  tLevel->setString(concatName(_("Level"),to_string(lvl)));
  tMoves->setString(concatName(_("Moves"),to_string(nb_moves)));

  int dId = MapManager::getInstance().getDungeonLevel();
  tDung->setString(concatName(_("Dungeon #"), to_string(dId)));
  tTime->setString(concatName(_("Time"), playedTime.str()));

  // Revealed %
  double pc = MapManager::getInstance().getCurrentMap()->getRevealedPercent();
  if (!MapManager::getInstance().getCurrentMap()->isRevealedBonusAdded()
      && pc > 75)
    {
      auto sc = entity(player_id)->component<Score>();
      char buf[80];
      sprintf(buf, _("Revealed bonnus for map %d").c_str(), dId);
      sc->addBonus(buf, 50);	
      MapManager::getInstance().getCurrentMap()->setRevealedBonusAdded();
    }
  ostringstream oss3;
  oss3.setf( std::ios_base::fixed, std::ios_base::floatfield );
  oss3 << _("Revealed") << " : " << std::setprecision(2) << pc << "%";
  tReveal->setString(get_wstring(oss3.str()));

  // Area
  double area = MapManager::getInstance().getCurrentMap()->getArea();
  ostringstream oss4;
  oss4.setf( std::ios_base::fixed, std::ios_base::floatfield );
  oss4 << _("Map Area") << " : " << std::setprecision(3) << area << " ha";
  tArea->setString(oss4.str());

  // FPS
  tFps->setString(concatName(_("FPS"), lastFPS));

  // NodeInfo
  tNodeInfo->setString(nodeInfo);

  // Weapon
  auto ch = entity(player_id)->component<Character>();
  auto weap = ch->getLHWeapon();
  string lh ="None";
  if (weap)
    lh = weap->getName() + " (" + ch->getLHWeapon()->getDamageString() + ")";
  
  tWeaponL->setString(concatName(_("Left hand"), lh));
  
  string rh = ch->getRHWeapon()->getName() + " (" + ch->getRHWeapon()
		    ->getDamageString() + ")";
  tWeaponR->setString(concatName(_("Right hand"), rh));

  if (ch->getCurrentHand() == CH_LEFT)
    {
      tWeaponL->setFillColor(activeWeapon);
      tWeaponR->setFillColor(inactiveWeapon);
    }
  else
    {
      tWeaponL->setFillColor(inactiveWeapon);
      tWeaponR->setFillColor(activeWeapon);
    }
    
    
}

void
InfoUi::cursorUp()
{
}

void
InfoUi::cursorDown()
{
}

void
InfoUi::cursorLeft()
{
}

void
InfoUi::cursorRight()
{
}

bool
InfoUi::action()
{
  return false;
}
  
void
InfoUi::toggleVisibility(const std::string&)
{
  
}

void
InfoUi::resize(unsigned int w, unsigned int h)
{
  Form::resize(w, h);
  drawables.clear();
  configure();

}

void
InfoUi::update(double duration_ms)
{
  if (nextFPS > FPSupdate)
    {
      int fps = 1000 / duration_ms;
      lastFPS = to_string(fps);
      nextFPS = 0;
    }
  else
    {
      nextFPS += duration_ms;
    }

  // Zoom handling
  if (!current_zoom.empty() && zoom_time > 0)
    {
      tZoom->setString("Zoom to tile " + current_zoom);
      zoom_time -= duration_ms;
    }

  if (zoom_time < 0)
    {
      tZoom->setString("");
    }

  // Score update
  auto sc = entity(player_id)->component<Score>();
  sc->playedTime = playedTime.str();
  sc->nbMoves    = nb_moves;
}

void
InfoUi::onChangeObjectsInfo(const ChangeObjectsInfoMessage& msg)
{
  auto es= MapManager::getInstance().getCurrentMap()->getEntities();
  
  for (auto& e : es)
    {
      if (e->getX() == msg.x && e->getY() == msg.y)
	{
	  tObjectInfo->setString(e->getName());
	  return;
	}
    }

  // Reset
  tObjectInfo->setString("");
}

void
InfoUi::onChangeMainFont(const ChangeMainFontMessage& msg)
{

  current_zoom = msg.getNextFont();
  zoom_time=100;
}

void
InfoUi::onChangeNodeInfo(const ChangeNodeInfoMessage& msg)
{
  nodeInfo = get_wstring(msg.msg);
}
