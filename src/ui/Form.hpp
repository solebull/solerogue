#ifndef __FORM_HPP__
#define __FORM_HPP__

#include <string>

/** Not called Dialog because there's no modality feature here */
class Form
{
public:
  Form(int, int);
  
  virtual void configure()=0;
  virtual void draw()=0;

  virtual void cursorUp()=0;
  virtual void cursorDown()=0;
  virtual void cursorLeft()=0;
  virtual void cursorRight()=0;

  /** Return true if event consumed. 
    *
    * If returns true, action chain is stopped.
    *
    */
  virtual bool action()=0; 

  void center();
  void center(unsigned int, unsigned int);
  
  virtual void toggleVisibility(const std::string&)=0;
  virtual void resize(unsigned int, unsigned int);
  
protected:
  int width, height; //!< The dimension of the form
  int x, y;          //!< The position of the top left corner
};

#endif // !__FORM_HPP__
