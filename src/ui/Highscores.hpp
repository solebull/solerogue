#ifndef __HIGHSCORES_UI_HPP__
#define __HIGHSCORES_UI_HPP__

#include "ui/Form.hpp"

#include <list>
#include <string>

#include "rltk.hpp"
#include "HighScoresEntry.hpp"

/** The log panel UI form
  *
  */
class HighscoresUi : public Form
{
public:
  HighscoresUi(bool test=false);
  
  virtual void configure() override;
  virtual void draw() override;

  virtual void cursorUp() override;
  virtual void cursorDown() override;
  virtual void cursorLeft() override;
  virtual void cursorRight() override;

  virtual bool action() override;
  
  virtual void toggleVisibility(const std::string&) override;
  virtual void resize(unsigned int, unsigned int) override;

  void update(double duration_ms);
  void print(const std::string&);

  int  getScoresPerPage(void);
  void setScoresPerPage(int);

  const std::list<HighScoresEntry>& getScores(void) const;
  
protected:
  void addHeader(const std::string& text, int x);
  void setPages(int);
  
  void serialize(struct gzip_file&);
  void deserialize(struct gzip_file&);

  int  add(void);
  void sort(void);
  void load(void);
  void save(void);

  std::string truncate(const string& str, long unsigned int width,
		       bool show_ellipsis=true);

  int computeMaxPage(int);
  int addScore(const HighScoresEntry&);

  
private:
  std::string filename;               //!< The scores file
  bool visible;                       //!< Is this form visible ?
  int  version;                       //!< The serialization format version

  std::list<sf::Drawable*> drawables; //!< Automatically drawn in draw()

  int currentPage;                    //!< Currently drawn page
  int maxPage;                        //!< Max highscores page
  int scoresPerPage;                  //!< Number of printed scores per page
  
  /** To-be-drawn scores of the curent page
    *
    */
  std::list<HighScoresEntry> pageScores;
  std::list<HighScoresEntry> scores;  //!< All scores
  
  sf::Text* tPages; //!< Used to draw actual/max pages

  bool playerIsDead;                  //!< Is the player dead
};

#endif // !__HIGHSCORES_UI_HPP__
