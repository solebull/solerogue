#include "Position.hpp"
#include <gtest/gtest.h>

#include "MapManager.hpp"


TEST( Position, with_params )
{
  int x=2, y=5;
  Position p(x, y);
  ASSERT_EQ( p.x, x);
  ASSERT_EQ( p.y, y);
}

TEST( Position, bounds_check )
{
  auto w = MapManager::getInstance().getCurrentMap()->getWidth();
  auto h = MapManager::getInstance().getCurrentMap()->getHeight();
  
  Position p(w  + 10, h + 10);
  p.bounds_check();
  ASSERT_EQ( p.x, w - 1);
  ASSERT_EQ( p.y, h - 1);
}

TEST( Position, bounds_check_negative )
{
  Position p(-5, -4);
  p.bounds_check();
  ASSERT_EQ( p.x, 0);
  ASSERT_EQ( p.y, 0);
}
