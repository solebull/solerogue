#ifndef _I18N_HPP_
#define _I18N_HPP_

#include "rltk.hpp"
#include "Singleton.hpp"

#include <string>
#include <map>
#include <cstdint>  // USES uint8_t

extern sf::Font default_font;
extern sf::Text default_text;

void         set_default_font(const std::string&, int);
sf::Font     get_default_font(void);

std::wstring get_wstring(const std::wstring & s);
std::wstring get_wstring(const std::string & s);
void print(rltk::virtual_terminal* t, int lx, int ly, int x, int y,
	   std::string s, const sf::Color& f,
	   const sf::Color& b = sf::Color::Black);

void setTempFontSize(int);
void removeTempFontSize();

class TranslationManager : public Singleton<TranslationManager>
{
public:
  TranslationManager(token);
  void parse(const std::string&);

  void               setCountryCode(const std::string&);
  const std::string& getCountryCode(void);

  const std::string& get(const std::string&);
  uint8_t            getGlyph(const std::string&);

  std::string getHelpDir(void);
  
private:
  std::map<std::string, std::string> strings;
  std::map<std::string, uint8_t>     glyphs;
  std::string country_code;
  std::string lastError;
};

#endif  // !_I18N_HPP_
