#include "MdParser.hpp"

#include "rltk.hpp"
#include "i18n.hpp"
#include "config.h" // USES UNUSED()

#include <iostream>

#include "api/Logger.hpp"

using namespace std;

static sf::RenderWindow* default_window=nullptr;

static sf::Color fore = sf::Color::White;
static sf::Color back = sf::Color::Black;

int xcursor = 1;
int ycursor = 1;

int layerx;
int layery;

static int in_header = -1;  // -1 if not in header, otherwise, the header #

string last_feature_error = "";

void print_word(const string& word);

void
nyi(const std::string& md)
{
  if (last_feature_error != md)
    {
      LOG(LogLevel::Info, "HelpViewer doesn't implement markdown feature %s",
	  md.c_str());

      last_feature_error = md;
    }
}

void print(int x, int y, wstring s)
{
  if (!default_window)
    throw std::runtime_error("default window pointer is NULL. "
			     "Did you call set_default_font()");

  if (ycursor > 23)
    return;
  
  default_text.setString(s);
  default_text.setFillColor(fore);
  default_text.setOutlineColor(back);
  sf::Vector2i vec(layerx + (x*8), layery + (y*16));
  default_text.setPosition(default_window->mapPixelToCoords(vec));
  default_window->draw(default_text);
}

void newline(int n)
{
  xcursor = 1;
  ycursor += n;
}

/** When closing some spans, we need to go one char back */
void backspace(int n = 1)
{
  xcursor -= n;
  if (xcursor < 1)
    xcursor = 1;
}

int enter_block(MD_BLOCKTYPE type, void* detail, void* /*userdata*/)
{
  MD_BLOCK_H_DETAIL* det = (MD_BLOCK_H_DETAIL*)detail;

  string s;
  switch (type)
    {
    case MD_BLOCK_DOC:
      UNUSED(s);
      break;
    case MD_BLOCK_P:
      newline(2);
      break;
    case MD_BLOCK_TABLE:
    case MD_BLOCK_THEAD:
    case MD_BLOCK_TBODY:
    case MD_BLOCK_TR:
    case MD_BLOCK_TD:
    case MD_BLOCK_TH:
      nyi("Will not handle HTML tables!");
      break;
    case MD_BLOCK_UL:
    case MD_BLOCK_OL:
    case MD_BLOCK_LI:
      nyi("Will not handle lists!");
      break;
    case MD_BLOCK_HTML:
      nyi("MD_BLOCK_HTML");
      break;
    case MD_BLOCK_CODE:
      nyi("MD_BLOCK_CODE");
      break;
    case MD_BLOCK_H:
      newline(1);
      setTempFontSize((5 - det->level) * 10);
      in_header = det->level;
      break;
    case MD_BLOCK_HR:
      newline(1);
      while(xcursor <= 50)
        print(xcursor++, ycursor, L"_");
      break;
    case MD_BLOCK_QUOTE:
      nyi("MD_BLOCK_QUOTE");
    }
  return 0;
}
int leave_block(MD_BLOCKTYPE type, void* detail, void* /*userdata*/)
{
  MD_BLOCK_H_DETAIL* det = (MD_BLOCK_H_DETAIL*)detail;
  switch (type)
    {
    case MD_BLOCK_H:
      newline(3 - in_header);
      removeTempFontSize();
      in_header = -1;
      break;
    default:
      UNUSED(det);
      //nyi("Leaving block");
    }
  return 0;
  
}

int enter_span(MD_SPANTYPE type, void* /*detail*/, void* /*userdata*/)
{
  string s;
  switch (type)
    {
    case MD_SPAN_EM:
      default_text.setStyle(default_text.getStyle()  + sf::Text::Italic );

      break;
    case MD_SPAN_STRONG:
      default_text.setStyle(default_text.getStyle()  + sf::Text::Bold );
      break;
    default:
      s = to_string(type);
    }
  
  return 0;
  
}
int leave_span(MD_SPANTYPE type, void* /*detail*/, void* /*userdata*/)
{
  string s;
  switch (type)
    {
    case MD_SPAN_EM:
      default_text.setStyle(default_text.getStyle()  - sf::Text::Italic );
      backspace();
      break;
    case MD_SPAN_STRONG:
      default_text.setStyle(default_text.getStyle()  - sf::Text::Bold );
      backspace();
      break;
    default:
      s = to_string(type);
    }
  return 0;
}


void check_newline(int nbchar = 1)
{
  if ((xcursor + nbchar) > 50) // In nb of character in a line
    {
      xcursor = 1;
      ycursor++;
    }
}

void
print_word(const wstring& word)
{
  check_newline(word.size());
  for (int i = 0; i< (int)word.size(); ++i)
    {
      wstring s(1, word[i]);
      print(xcursor++, ycursor, s);
      if (in_header > -1)
	xcursor += (3 - in_header);
      
    }
  print(xcursor++, ycursor, L" ");
  if (in_header > -1)
    xcursor += (3 - in_header);

}
  

void
print_word(const string& word)
{
  wstring ws = get_wstring(word);
  print_word(ws); 
}

int
text(MD_TEXTTYPE type, const MD_CHAR* text, MD_SIZE size, void* /*userdata*/)
{
  string atext(text);
  stringstream check1(atext.substr(0, size));
  string word;

  switch (type)
    {
    case MD_TEXT_NORMAL:
      while(getline(check1, word, ' '))
	print_word(word);
      break;
    case MD_TEXT_NULLCHAR:
      nyi("MD_TEXT_NULLCHAR");
      break;
    case MD_TEXT_BR:
      newline(2);
      break;
    case MD_TEXT_LATEXMATH:
      nyi("MD_TEXT_LATEXMATH");
      break;
    case MD_TEXT_HTML:
      nyi("MD_TEXT_HTML");
      break;
    case MD_TEXT_CODE:
      nyi("MD_TEXT_CODE");
      break;
    case MD_TEXT_ENTITY:
      nyi("a...; MD_TEXT_ENTITY");
      break;
    case MD_TEXT_SOFTBR:
      // Not adding soft break to final document
      break;
    }
  
  return 0;
}

/* Debug callback. Optional (may be NULL).
 *
 * If provided and something goes wrong, this function gets called.
 * This is intended for debugging and problem diagnosis for developers;
 * it is not intended to provide any errors suitable for displaying to an
 * end user.
 */
void
debug_log(const char* msg, void* /*userdata*/)
{
  LOG(LogLevel::Error, "Markdown parser error : %s", msg);
}



MdParser::MdParser()
{
  default_window = rltk::get_window();

  rn.abi_version = 0;
  rn.enter_block = &enter_block;
  rn.leave_block = &leave_block;
  rn.enter_span = &enter_span;
  rn.leave_span = &leave_span;
  rn.text = &text;

  rn.debug_log = &debug_log;
  rn.syntax = NULL;
}


/** \param content A markdown-formated string */
int
MdParser::parse(const std::string& content)
{
  ycursor = 1;
  xcursor = 1;

  return md_parse(content.c_str(), content.size(), &rn, NULL);
}
