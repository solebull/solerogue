#ifndef __SCRIPT_MANAGER_HPP__
#define __SCRIPT_MANAGER_HPP__

#include "Singleton.hpp"

#include "rltk.hpp"   // USES entity_t*
#include "api/VcharBuffer.hpp"

#include <list>
#include <string>

#include <iostream>

using namespace std;

// Forward declarations
class Map;
class Script;
// End of forward declarations

class ScriptManager : public Singleton<ScriptManager>
{
public:
  ScriptManager(token);
  void init(const string&);

  void call_OnInit();
  void call_OnGenerate(Map*);
  void call_OnMove(Map*,  rltk::entity_t*);
  void call_OnFrame(double, VcharBuffer*);
  int call_OnGetTile(int, int, int, bool);  
  
  bool isEnabled(void);
  void disable(void);
  
protected:
  bool checkDirectory(const string&);
  bool checkFile(const string&);
  void addSubScrips(const string&);
  void addScript(const string&, const string&);
  
private:
  list<Script*> scripts;
  bool enabled;
};

#endif // !__SCRIPT_MANAGER_HPP__
