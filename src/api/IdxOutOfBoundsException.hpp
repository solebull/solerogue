#ifndef _IDX_OUT_OF_BOUNDS_EXCEPTION_HPP_
#define _IDX_OUT_OF_BOUNDS_EXCEPTION_HPP_

#include <exception>
#include <string>

using namespace std;


/** The Map::idx function is about to returning a out of bounds index */
class IdxOutOfBoundsException: public exception
{
public:
  IdxOutOfBoundsException(int x, int y, int vectorSize, int width, int ret);
  virtual const char* what() const throw();

private:
  string w;
};

#endif // !_IDX_OUT_OF_BOUNDS_EXCEPTION_HPP_
