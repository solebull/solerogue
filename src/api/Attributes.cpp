#include "Attributes.hpp"
#include <map>
#include <string>

Attributes::Attributes()
{
  intMap["health"] = 100;
  intMap["health-max"] = 100;

  lvlMap["general"] = Level();
}


void
Attributes::intInc(const string& key, int val)
{
  intMap[key] += val;
  
  string keymax = key + "-max";
  if (intMap.find(keymax) != intMap.end())
    if (getInt(key) > getInt(keymax))
      intMap[key] = getInt(keymax);
  
}

void
Attributes::intDec(const string& key, int val)
{
  intMap[key] -= val;

}


int
Attributes::getInt(const string& key)
{
  return intMap[key];
}

void
Attributes::setInt(const string& key, int val)
{
  intMap[key] = val;
}

/** Get the Level object identified by the given key
  *
  * \param the level map key.
  *
  */
Level&
Attributes::getLevel(const string& key)
{
  return lvlMap[key];
}
