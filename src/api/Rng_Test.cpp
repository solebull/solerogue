#include "Rng.hpp"
#include "Map.hpp"
#include <gtest/gtest.h>

TEST( Rng, random_pos )
{
  int x=-1, y=-1;
  Map m(10, 15);
  rng::get_random_pos(&m, &x, &y);
  ASSERT_NE( x, -1);
  ASSERT_NE( y, -1);
}

TEST( Rng, random_floor )
{
  int x=-1, y=-1;
  Map m(10, 15);
  rng::get_random_floor(&m, &x, &y);
  ASSERT_EQ( m.getTile(x, y), 0);
}

