#ifndef _MAP_SYSTEM_HPP_
#define _MAP_SYSTEM_HPP_

#include <vector>
#include <cstdint> // USES uint8_t
#include <iostream>
#include <cassert> // USES assert()

#include "Position.hpp"
#include "EntityList.hpp"
#include "Logger.hpp"

#include "IdxOutOfBoundsException.hpp"
#include "config.h" // USES __FILENAME_ on win
using namespace std;

//int map_idx(const int x, const int y);

#define idx(X,Y) idx_impl(__FILENAME__, __LINE__, X,Y)

// A future full Node implementation
typedef int Node;

/** A Map object
  *
  * This is not a system so we can have a global static object.
  *
  */
class Map : public EntityList
{
public:
  Map(int w=100, int h=100, int mapIdx=0);
  ~Map();
  
  void reset();
  void generate(int dungeonLevel=0);

  /** Return a flat index (usable in vector) from X,Y values
   *
   */
  inline size_t idx_impl(const char* file, int line,
			 const int x, const int y) const
  {
    size_t ret = (y * this->width) + x;
    if (ret > tiles.size())
      {
	LOG(LogLevel::Debug, "Calling idx_impl() from %s:%d with %d,%d params"
	    " throws IdxOutOfBoundsException exception...",
	    file, line, x, y);
	throw IdxOutOfBoundsException(x, y, tiles.size(), this->width, ret);
      }
    return ret;
  }
  
  size_t getWidth(void) const;
  size_t getHeight(void) const;
  double getArea(void) const;

  double getRevealedPercent() const;
  const vector<Position>& getDownStairs()const;
  const vector<Position>& getHatches()const;

  bool isRevealedBonusAdded(void) const;
  void setRevealedBonusAdded(void);

  const Node& getTile(size_t idx)const;
  const Node& getTile(int x, int y)const;
  void        setTile(int x, int y, Node n);

  const uint8_t getVisible(size_t idx)const;
  const uint8_t getVisible(int x, int y)const;
  void          setVisible(int x, int y, uint8_t n);
  void          resetVisible();

  const vector<bool>& getRevealed() const;
  const bool          getRevealed(size_t idx)const;
  const bool          getRevealed(int x, int y)const;
  void                setRevealed(int x, int y, bool b);

  
protected:
  void copyDownStairs(const Map*);
  void checkHatches  (int idx);
  
private:
  int width;
  int height;
  vector<Node>     tiles;      // The Map tiles/nodes
  vector<uint8_t>  visible;
  vector<bool>     revealed;

  vector<Position> downStairs;
  vector<Position> hatches;
  bool revealedBonusAdded;
};

#endif // !_MAP_SYSTEM_HPP_
