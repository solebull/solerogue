
#include "IdxOutOfBoundsException.hpp"

IdxOutOfBoundsException::IdxOutOfBoundsException(int x, int y,
						 int vectorSize,
						 int width, int ret)
{
  w = "Map::idx return out of bounds value: " + to_string(x) + "x" +
    to_string(y)+ ">" + to_string(vectorSize )+ "( with width " +
    to_string(width) + " returns " + to_string(ret);
}
  
const char*
IdxOutOfBoundsException::what() const throw()
{
  return w.c_str();
}
