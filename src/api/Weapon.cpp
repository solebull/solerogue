#include "Weapon.hpp"

#include "Rng.hpp"

#include <stdexcept>

using namespace std;

Weapon::Weapon(const std::string& name):
  Entity(name),
  injuryName(name),
  damageString(),
  onDamage(nullptr)
{

}

Weapon::~Weapon()
{

}


int
Weapon::getDamage(void) const
{
  // return rng::rdice(1, 4);
  if (onDamage)
    return onDamage();
  else
    throw runtime_error("Weapon::onDamage callback not set for weapon '" +
			getName() + "'");
  
}

void
Weapon::setInjuryName(const std::string& in)
{
  injuryName = in;
}

const std::string&
Weapon::getInjuryName(void) const
{
  return injuryName;
}

void
Weapon::setOnDamage(OnDamageFn od)
{
  onDamage = od;
}

void
Weapon::setDamageString(const std::string& ds)
{
  damageString = ds;
}

const std::string&
Weapon::getDamageString(void) const
{
  return damageString;
}

/** Return a list of possible actions for this weapon
  *
  * \return A list a string containing actions
  *
  */
list<string>
Weapon::possibleActions(void) const
{
  list<string> ls;

  ls.push_back("Left hand equip");
  ls.push_back("Right hand equip");
  
  for (auto& a : actions)
    ls.push_back(a.first);

  return ls;
}

