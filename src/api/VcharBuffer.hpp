#ifndef __VCHAR_BUFFER_HPP__
#define __VCHAR_BUFFER_HPP__

#include "rltk.hpp" // Needed vchar

/** A buffer used by mod to draw and modify map just before rendering in
  * rltk's virtual_terminal
  *
  * It doesn't implement its own dirty flag. Use virtual_terminal's one instead.
  * 
  * It also keeps the MapCenter value (player position).
  *
  */
class VcharBuffer
{
public:
  VcharBuffer();

  void clear();
  void resize(int, int);
  inline size_t at(const int x, const int y) noexcept
  {
    return ( y * term_width) + x;
  }

  void               set_char(int, int, const rltk::vchar&);
  const rltk::vchar& getChar(int, int);

  void setMapCenter(int, int);
  int  getMapCenterX(void);
  int  getMapCenterY(void);

  int    getWidth() const;
  int    getHeight() const;
  size_t getSize() const;

  void   draw(rltk::virtual_terminal*);
  
private:
  std::vector<rltk::vchar> buffer;

  int term_width;
  int term_height;
  
  int map_center_x;
  int map_center_y;
};

#endif // !__VCHAR_BUFFER_HPP__
