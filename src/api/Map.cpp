#include "Map.hpp"

#include "rltk.hpp"

#include "MapManager.hpp"
#include "ScriptManager.hpp"

using namespace rltk;

#define RZ (w * h)

Map::Map(int w, int h,int  mapIdx):
  EntityList(),
  width(w),
  height(h),
  tiles(RZ),
  visible(RZ),
  revealed(RZ),
  downStairs(),
  hatches(),
  revealedBonusAdded(false)
{
  reset();
  generate(mapIdx);
}

Map::~Map()
{
  tiles.clear();
  visible.clear();
  revealed.clear();
  downStairs.clear();
  hatches.clear();
}


// Reset the actual map to default values
void
Map::reset()
{
  // Zero the map other than the edges
  std::fill(tiles.begin(), tiles.end(), 0);
  std::fill(visible.begin(), visible.end(), false);
  std::fill(revealed.begin(), revealed.end(), false);

  downStairs.clear();
  hatches.clear();
}

void
Map::generate(int dungeonLevel)
{
  random_number_generator rng;

  // Generate walls
  for (int i=0; i<width; ++i)
    {
      tiles[this->idx(i, 0)] = 1;
      tiles[this->idx(i, height-1)] = 1;
    }
  
  for (int i=0; i<height; ++i)
    {
      tiles[this->idx(0, i)]       = 1;
      tiles[this->idx(width-1, i)] = 1;
    }

  // Random walls
  for (int y=1; y<height-1; ++y) {
    for (int x=1; x<width-1; ++x) {
      if (rng.roll_dice(1,4)==1 && (x!=width/2 || y!=height/2))
	tiles[this->idx(x,y)]=1;
    }
  }

  // 2 for level down
  int dx = rng.roll_dice(1, this->width - 2) + 1;
  int dy = rng.roll_dice(1, this->height - 2) + 1;
  tiles[this->idx(dx,dy)]=2;
  downStairs.emplace_back(Position(dx, dy));
  
  if(dungeonLevel == 0)
  {
    // 3 for level up
    int ux = rng.roll_dice(1,width - 2) + 1;
    int uy = rng.roll_dice(1,height - 2) + 1;
    tiles[this->idx(ux,uy)]=3;
  }
  else
    {
      copyDownStairs(MapManager::getInstance()
		     .getMapByIndex(dungeonLevel - 1));
    }
  
  // 4 for hatch
  int nbhatch = rng.roll_dice(1, 6);
  for (int ihatch = 0; ihatch < nbhatch; ++ihatch)
    {
      int ux = rng.roll_dice(1,width - 2) + 1;
      int uy = rng.roll_dice(1,height - 2) + 1;
      tiles[this->idx(ux,uy)]=4;
      hatches.push_back(Position(ux, uy));
    }
  
  if (dungeonLevel > 0)
    checkHatches(dungeonLevel - 1);
  
  ScriptManager::getInstance().call_OnGenerate(this);
}

size_t
Map::getWidth(void) const
{
  return this->width;
}

size_t
Map::getHeight(void) const
{
  return this->height;
}


double Map::getRevealedPercent() const
{
  int nb_revealed = 0;
  for (auto it: revealed)
    if (it) ++nb_revealed;
  
  return (nb_revealed * 100) / (double)revealed.size();
}

/** Get map area in ha
  *
  */
double
Map::getArea(void) const
{
  return (width * height) / 10000.0;

}

const vector<Position>&
Map::getDownStairs()const
{
  return downStairs;
}

/** Copy down stairs from given map
 *
 * Used to add coherence between down stairs from a level and up stairs
 * from another.
 *
 */
void
Map::copyDownStairs(const Map* m)
{
  auto ds = m->getDownStairs();
  for (auto pos : ds)
    tiles[this->idx(pos.getX(),pos.getY())]=3;
}

const vector<Position>&
Map::getHatches()const
{
  return hatches;
}

/** Check if the current map tiles, just under precedent map's hatches
  * aren't walls
  *
  *
  */
void
Map::checkHatches(int mapIdx)
{
  auto ds = MapManager::getInstance().getMapByIndex(mapIdx)->getHatches();
  for (auto pos : ds)
    if (tiles[idx(pos.getX(),pos.getY())] == 1)
      tiles[idx(pos.getX(),pos.getY())] = 0;
}

bool
Map::isRevealedBonusAdded(void) const
{
  return this->revealedBonusAdded;
}

void
Map::setRevealedBonusAdded()
{
  this->revealedBonusAdded = true;
}

const Node&
Map::getTile(size_t idx)const
{
  return this->tiles[idx];
}

const Node&
Map::getTile(int x, int y)const
{
  Position p(x, y);
  p.bounds_check(width, height);
  return getTile(idx(p.getX(), p.getY()));
}

void
Map::setTile(int x, int y, Node n)
{
  tiles[idx(x, y)] = n;
}
  
const uint8_t
Map::getVisible(size_t idx)const
{
  return visible[idx];
}

const uint8_t
Map::getVisible(int x, int y)const
{
  Position p(x, y);
  p.bounds_check(width, height);
  return getVisible(idx(p.getX(), p.getY()));
}

void
Map::setVisible(int x, int y, uint8_t n)
{
  visible[idx(x, y)] = n;
}

void
Map::resetVisible()
{
  std::fill(visible.begin(), visible.end(), 0);
}

void
Map::setRevealed(int x, int y, bool b)
{
  revealed[idx(x, y)] = b;
}

const vector<bool>&
Map::getRevealed() const
{
  return revealed;
}

const bool
Map::getRevealed(size_t idx)const
{
  return revealed[idx];
}

const bool
Map::getRevealed(int x, int y)const
{
  Position p(x, y);
  p.bounds_check();
  return getRevealed(idx(p.getX(), p.getY()));
}

