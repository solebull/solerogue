#include "Map.hpp"
#include <gtest/gtest.h>

class MapTest : public Map
{
public:
  MapTest(int w=100, int h=100):
    Map(w, h)
  {
    
  }

  void copyDownStairsImpl(const Map* m)
  {
    copyDownStairs(m);
  }

};

TEST( Map, ctor )
{
  Map m(10, 15);
  ASSERT_EQ( m.getWidth(),  10);
  ASSERT_EQ( m.getHeight(), 15);
}

// Test that inner vector are resized within the constructor
// Without, it can lead to a -nan revealedPercent with is a bug
//
// To fix it, please call reset() from within the constructor
TEST( Map, ctor_reset )
{
  Map m(10, 15);
  ASSERT_NE( m.getRevealed().size(), 0);
}

TEST( Map, idx )
{
  Map m(10, 15);
  ASSERT_EQ( m.idx(2, 3),  32);

  Map m2(20, 30);
  ASSERT_EQ( m2.idx(2, 3),  62);
}

TEST( Map, idx_ne )
{
  Map m(10, 15);
  Map m2(20, 30);
  ASSERT_NE(m.idx(2, 3), m2.idx(2, 3));
}

TEST( Map, revealedPercent )
{
  Map m(10, 10);
  ASSERT_EQ(m.getRevealedPercent(), 0.0);
  m.setRevealed(0, 0, true);
  ASSERT_EQ(m.getRevealedPercent(), 1.0);
}

TEST( Map, getArea )
{
  Map m(100, 100);
  ASSERT_EQ(m.getArea(), 1.0);

  Map m2(50, 100);
  ASSERT_EQ(m2.getArea(), 0.5);

  Map m3(50, 50);
  ASSERT_EQ(m3.getArea(), 0.25);
}

// Test that getting downStairs returns a vector
TEST( Map, getDownStairs )
{
  Map m(100, 100);
  m.generate(0);

  MapTest m2(100, 100);
  m2.copyDownStairsImpl(&m);
  
  // Should only generate a down stair
  auto ds = m.getDownStairs();
  for (auto pos: ds)
    {
      ASSERT_EQ(m2.getTile(pos.x,pos.y), 3);
    }
}

// Test that getting downStairs returns a vector
TEST( Map, create100maps )
{

  for (int i=0; i<100; ++i)
    {
      Map m(100, 100);
      m.generate(0);
    }
}

// Test that getting downStairs returns a vector
TEST( Map, badidx_throw )
{
  Map m(100, 100);
  EXPECT_THROW(m.idx(150, 120), IdxOutOfBoundsException);
}

// Test that getting downStairs returns a vector
TEST( Map, idx_x0 )
{
  Map m(100, 100);
  EXPECT_THROW(m.idx(0, 120), IdxOutOfBoundsException);
}

// Test that getting downStairs returns a vector
TEST( Map, get_tile)
{
  Map m(100, 100);
  EXPECT_NO_THROW(m.getTile(10, 10));
}


