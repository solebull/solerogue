#include "Attributes.hpp"
#include <gtest/gtest.h>

#include <iostream>

using namespace std;

TEST( Attributes, ctor )
{
  Attributes a;
  ASSERT_EQ( a.getInt("health"),  100);
  ASSERT_EQ( a.getInt("health-max"),  100);
}


TEST( Attributes, intdec )
{
  Attributes a;
  a.intDec("health", 20);
  ASSERT_EQ( a.getInt("health"),  80);
}

// Should not be greater than max attribute
TEST( Attributes, intinc_max )
{
  Attributes a;
  a.intInc("health", 20);
  ASSERT_EQ( a.getInt("health"), a.getInt("health-max"));
}

// Should not be greater than max attribute
TEST( Attributes, intinc_max2 )
{
  Attributes a;
  a.intDec("health", 10);
  a.intInc("health", 20);
  ASSERT_EQ( a.getInt("health"), a.getInt("health-max"));
}

