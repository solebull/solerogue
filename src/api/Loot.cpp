#include "Loot.hpp"

#include "api/Rng.hpp"
#include "api/Entity.hpp"

/** Chance is exprimed on a base 100;
 */

Loot::Loot(Entity* e, int c):
  entity(e),
  chance(c)
{

}

bool
Loot::get(void)
{
  int n = rng::rdice(1, 100);
  if (n <= chance)
    return true;

  return false;
}

Entity*
Loot::getEntity(void)
{
  return entity;
}
