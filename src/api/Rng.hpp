#ifndef __RNG_HPP__
#define __RNG_HPP__

#include "rltk.hpp"

// Forward declarations
class Map;
// End of forward declarations


namespace rng
{

  int rdice(const int &n, const int &d);
  void get_random_pos(Map*, int*, int*);
  void get_random_floor(Map*, int*, int*);
}

#endif // !__RNG_HPP__
