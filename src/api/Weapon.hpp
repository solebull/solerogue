#ifndef __WEAPON_HPP__
#define __WEAPON_HPP__

#include "Entity.hpp"

#include <string>
#include <functional>   // USES std::function

class Weapon : public Entity
{
public:
  using OnDamageFn = std::function<int(void)>;

  Weapon(const std::string& name);
  virtual ~Weapon();

  int getDamage(void) const;
  void setOnDamage(OnDamageFn);
  
  void               setInjuryName(const std::string&);
  const std::string& getInjuryName(void) const;

  void               setDamageString(const std::string&);
  const std::string& getDamageString(void) const;
  
  virtual bool isEquipable(void) const override { return true; }
  virtual std::list<std::string> possibleActions(void) const override;

  
private:
  std::string injuryName;   //!< The weapon causes a ???
  std::string damageString; //!< The damage explanation string (1d6) etc...
  OnDamageFn onDamage;      //!< The onDamage callback
};

#endif // !__WEAPON_HPP__
