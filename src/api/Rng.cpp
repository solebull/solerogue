#include "Rng.hpp"

#include "Map.hpp"

rltk::random_number_generator rltk_rng;

int
rng::rdice(const int &n, const int &d)
{
  return rltk_rng.roll_dice(n, d);
}

/** Change x and y to be a random in-map position
  *
  * Without any node check.
  *
  */
void
rng::get_random_pos(Map* m, int* x, int* y)
{
  *x = rng::rdice(1, m->getWidth() - 1);
  *y = rng::rdice(1, m->getHeight() - 1);
}

void
rng::get_random_floor(Map* m, int* x, int* y)
{

  Node n = 1;
  while (n != 0)
    {
      rng::get_random_pos(m, x, y);
      n = m->getTile(m->idx(*x , *y));
    }
  
}

