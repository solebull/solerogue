#include "Level.hpp"
#include <gtest/gtest.h>

#include <iostream>

using namespace std;

TEST( Level, ctor )
{
  Level lvl;
  ASSERT_EQ( lvl.getLevel(),  1);
  ASSERT_EQ( lvl.getExp(),    0);
  ASSERT_EQ( lvl.getNext(), 100);
}

TEST( Level, ctor_next )
{
  Level lvl(200);
  ASSERT_EQ( lvl.getNext(), 200);
}


TEST( Level, ctor_inc )
{
    Level lvl(200);
    lvl.inc(50);
    ASSERT_EQ( lvl.getLevel(),  1);
    ASSERT_EQ( lvl.getExp(),   50);

    lvl.inc(175);
    ASSERT_EQ( lvl.getLevel(),  2);
    ASSERT_EQ( lvl.getExp(),   25);

    // We increase next level 'next
    ASSERT_EQ( lvl.getNext(), 240);
}

TEST( Level, ctor_inc_returns_true )
{
   Level lvl(200);
   ASSERT_EQ(lvl.inc(50),  false);
   ASSERT_EQ(lvl.inc(175), true); // Increase level
}
