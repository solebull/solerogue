#include "Logger.hpp"
#include <gtest/gtest.h>

#include <string>

using namespace std;

class TestedLogger: public Logger
{
public:
  
  TestedLogger(): Logger("logfile-test.log", LogLevel::Warning,
			 LOG_MEDIA_FILE){};
  TestedLogger(const LogLevel ll): Logger("logfile-test.log", ll,
					  LOG_MEDIA_FILE){};
  
  std::string _levelToStr(const LogLevel lvl) const {
    return Logger::levelToStr(lvl);
  }

  bool _shouldShow(const LogLevel lvl) const {
    return Logger::shouldShow(lvl);
  }

};

TEST( Logger, levelToStr )
{
  TestedLogger l;
  ASSERT_EQ(l._levelToStr(LogLevel::Warning), "WW");
  ASSERT_EQ(l._levelToStr(LogLevel::Debug),   "DD");
  ASSERT_EQ(l._levelToStr(LogLevel::Error),   "EE");
}

TEST( Logger, ShouldShow )
{
  TestedLogger l;
  ASSERT_EQ(l._shouldShow(LogLevel::Info),    false);
  ASSERT_EQ(l._shouldShow(LogLevel::Warning), true);
  
  TestedLogger l2(LogLevel::Critical);
  ASSERT_EQ(l2._shouldShow(LogLevel::Warning), false);
  
}

// With a brand new logger, return an empty last part
TEST( Logger, lastPart_empty )
{
  TestedLogger l;
  string s = l.lastPart();
  ASSERT_EQ(s.empty(),    true);
}

TEST( Logger, lastPart_one )
{
  TestedLogger l;
  l.push("aze");
  ASSERT_EQ(l.lastPart(), "aze");
}

TEST( Logger, lastPart_two )
{
  TestedLogger l;
  l.push("aze");
  l.push("zer");
  ASSERT_EQ(l.lastPart(), "zer");
}

TEST( Logger, lastPart_two_then_pop )
{
  TestedLogger l;
  l.push("aze");
  l.push("zer");
  l.pop();
  
  ASSERT_EQ(l.lastPart(), "aze");
}

TEST( Logger, lastPart_one_then_pop )
{
  TestedLogger l;
  l.push("aze");
  l.pop();
  
  ASSERT_EQ(l.lastPart().empty(), true);
}

TEST( Logger, get_threshold )
{
  TestedLogger l;
  auto t = l.getThreshold();
    
  ASSERT_EQ(t, LogLevel::Warning);
}

TEST( Logger, set_threshold )
{
  TestedLogger l;
  l.setThreshold(LogLevel::Error);
    
  auto t = l.getThreshold();
  ASSERT_EQ(t, LogLevel::Error);
}

TEST( Logger, log_media_type )
{
  LogMediaType lmt = LOG_MEDIA_FILE;
  ASSERT_TRUE(lmt & LOG_MEDIA_FILE);
  ASSERT_FALSE(lmt & LOG_MEDIA_CERR);

  lmt = LOG_MEDIA_FILE | LOG_MEDIA_CERR;
  ASSERT_TRUE(lmt & LOG_MEDIA_FILE);
  ASSERT_TRUE(lmt & LOG_MEDIA_CERR);

  lmt = 0;
  ASSERT_FALSE(lmt & LOG_MEDIA_FILE);
  ASSERT_FALSE(lmt & LOG_MEDIA_CERR);
}

TEST( Logger, get_media_type )
{
  TestedLogger l;
  auto t = l.getMediaType();
    
  ASSERT_TRUE(t & LOG_MEDIA_FILE);
  ASSERT_FALSE(t & LOG_MEDIA_CERR);
}

TEST( Logger, set_media_type )
{
  TestedLogger l;
  l.setMediaType(LOG_MEDIA_CERR|LOG_MEDIA_FILE);
    
  auto t = l.getMediaType();
  ASSERT_TRUE(t & LOG_MEDIA_CERR);
  ASSERT_TRUE(t & LOG_MEDIA_FILE);
}

