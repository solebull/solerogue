#include "File.hpp"

#include <gtest/gtest.h>
#include <iostream>

using namespace std;

TEST( File, glob )
{
  auto fl = File::fglob("../");
  ASSERT_NE( fl.size(), 0);
}

// Should handle '..' (i.e. relative paths)
TEST( File, glob_relative )
{
  auto fl = File::fglob("../*");
  bool src_found = false;

  // Should be called from build/ directory
  for (auto const& f : fl)
    if (f.find("src") != std::string::npos)
      src_found = true;

  ASSERT_TRUE( src_found);
}
