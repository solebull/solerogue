#include "EntityList.hpp"

#include <cassert>
#include <stdexcept>

#include "Entity.hpp"
#include "Logger.hpp"

using namespace std;

bool shown = false;

EntityList::EntityList():
  entities(0)
{

}

void
EntityList::add(Entity* e)
{
  assert(e->getWeight() > 0.0);
  
  LPUSH("Elist");
  if (find(entities.begin(), entities.end(), e) == entities.end())
    {
      LOG(LogLevel::Info, "Adding entity '%p' (%s)",
	  e, e->getName().c_str());

      entities.push_back(e);
    }
  else
    {
      LOG(LogLevel::Info, "Trying to ad uplicate entity %p", e);
    }
  LPOP();
}

const list<Entity*>
EntityList::getEntities() const
{
  if (!shown)
    {
      LOG(LogLevel::Info, "Getting entities from '%p' ", this);
    }
  shown = true;
  return entities;
}

void
EntityList::remove(Entity* e)
{

  auto it = std::find(entities.begin(), entities.end(), e);
  if (it != entities.end())
    {
      LOG(LogLevel::Info, "Removing entity %s", e->getName().c_str());
      entities.remove(e);
    }
  else
    throw std::runtime_error("Trying to remove a non-present entity");
}
