#include "Entity.hpp"

#include <utility> // USES make_pair()

#include "PlayerId.hpp"   // USES player_id

#include "MapManager.hpp"
#include "i18n.hpp"

#include "api/Map.hpp"
#include "api/Rng.hpp"
#include "api/Weapon.hpp"
#include "api/Loot.hpp"

using namespace std;

/** Named constructor
  *
  * \param n The name.
  *
  */
Entity::Entity(const string& n):
  name(n),
  glyph(0),
  x(0),
  y(0),
  weight(0.0f),
  walkable(true),
  deathby(n),
  deathbyValue(0),
  enemy(false),
  onAttackedFn(nullptr),
  species(),
  speciesIdx(0),
  expWhenKilled(0)
{

}

/** Return the current entity glyph 
  *
  * \return The glyph code
  *
  */
uint8_t
Entity::getGlyph(void)
{
  if (glyph == 0)
    glyph = TranslationManager::getInstance().getGlyph(this->glyphid);

  return glyph;
}

/** Set the glyph identifier
  *
  * \param gid The new glyph identifier.
  *
  */
void
Entity::setGlyphId(const string& gid)
{
  this->glyphid = gid;
}


void
Entity::setForegroundColor(int r, int g, int b)
{
  fg = color_t(r, g, b);
  darkfg = color_t(r/2, g/2, b/2);
}

void
Entity::setBackgroundColor(int r, int g, int b)
{
  bg = color_t(r, g, b);
}

void
Entity::move(int vX, int vY)
{
  this->x = vX;
  this->y = vY;
}

const vchar
Entity::getVChar()
{
  return vchar(getGlyph(), fg, bg);
}
const vchar
Entity::getDarkVChar()
{
  return vchar(getGlyph(), darkfg, bg);
}

void
Entity::setName(const string& n)
{
  name = n;
}

void
Entity::setDescription(const string& d)
{
  description = d;
}

void
Entity::setSpecies(const std::string& s, int i)
{
  species = s;
  speciesIdx = i;
}

/** Returns a non-translated version of the entity species */
const std::string&
Entity::getSpecies(void)
{
  return species;
}

string
Entity::getName(void) const
{
  if (species.empty())
    return TranslationManager::getInstance().get(name);

  if (speciesIdx == -1)
    return name + " (" + species + ")";

  string sp = TranslationManager::getInstance().get(species);
  return name + " (" + sp + " " + to_string(speciesIdx) + ")";
}

const string&
Entity::getDescription(void) const
{
  return TranslationManager::getInstance().get(description);
}

const color_t&
Entity::getForegroundColor(void) const
{
  return fg;
}

const color_t&
Entity::getBackgroundColor(void) const
{
  return bg;
}

void
Entity::setWeight(float w)
{
  weight = w;
}

float
Entity::getWeight(void) const
{
  return weight;
}

void
Entity::add(const string& verb, ActionFn action)
{
  actions.insert(make_pair(verb, action));
}

/** Fire the given action
  *
  * No error occurs if the action can't be found. Only a console message.
  *
  * \param verb The action to be fired.
  *
  */
void
Entity::act(const string& verb)
{
  if (actions[verb])
    actions[verb](entity(player_id), MapManager::getInstance().getCurrentMap());
  else
    cout << "Entity doesn't know how to " << verb << endl;

}

/** Does this entity have this actions ?
  *
  * \return True if the actions map contains the given verb.
  *
  */
bool
Entity::can(const string& verb)
{
  return actions[verb] ? true : false;
}


/** Returns a list of possible actions on this entity 
 *  
 * \return A string list.
 *
 */
list<string>
Entity::possibleActions(void) const
{
  list<string> ls;
  for (auto& a : actions)
    ls.push_back(a.first);

  return ls;
}

/** Is this entity walkable
  *
  * \param w a boolean.
  *
  */
void
Entity::setWalkable(bool w)
{
  walkable = w;
}

/** Is this entity walkable ?
  *
  * \return a boolean.
  *
  */
bool
Entity::isWalkable(void) const
{
  return walkable;
}

/** Set deathby and value in one call
  *
  * \param db The deathBy string.
  * \param v  The integer value.
  *
  */
void
Entity::setDeathBy(const std::string& db, int v)
{
  deathby = db;
  deathbyValue = v;
}

/** Get the deathby string value
  *
  * \return The string
  * 
  */
const std::string&
Entity::getDeathBy(void) const
{
  return deathby;
}

/** Get the deathbyValue value
  *
  * \return An integer.
  * 
  */
int
Entity::getDeathByValue(void) const
{
  return deathbyValue;
}

/** Is this entity an enemy ?
  *
  * It defines if it must be shown in the enemy layer.
  *
  * \param b New value.
  * 
  */
bool
Entity::isEnemy(void)
{
  return enemy;
}

/** Is this entity an enemy
  *
  * It defines if it must be shown in the enemy layer.
  *
  * \param b New value.
  * 
  */
void
Entity::setEnemy(bool b)
{
  enemy = b;
}

/** Change the onAttacked callback
  *
  * \param cb The new callback (function pointer).
  *
  */
void
Entity::onAttacked(OnAttackedFn cb)
{
  onAttackedFn = cb;
}

/** Applys the given amount of damage
  *
  * \param damage The hit points.
  *
  * \return true if dead 
  *
  */
bool
Entity::attack(int damage)
{
  att.intDec("health", damage);

  if (onAttackedFn)
    onAttackedFn(damage);

  if (att.getInt("health") <= 0)
    return true;

  return false;
}

/** Return entity's attributes
  * 
  * \return A pointer to the Attributes object.
  *
  */
Attributes*
Entity::getAttributes(void)
{
  return &att;
}

/** Add a weapon to the weaponList
  *
  * \param w The weapon to be added.
  *
  */
void
Entity::addWeapon(Weapon* w)
{
  weaponList.push_back(w);
}


/** Return a random weapon or nullptr
  *
  * Warning: may return a nullptr weapon pointer if weaponList is empty.
  *
  */
Weapon*
Entity::getRandomWeapon(void)
{
  auto s = weaponList.size();
  if (s == 0)
    return nullptr;
  else
    {
      auto r = rng::rdice(1, s);
      return weaponList[r - 1];
    }
  
}

/** Add the given loot to the lootList
  *
  * \param l The loot pointer to be added.
  *
  */
void
Entity::addLoot(Loot* l)
{
  lootList.push_back(l);
}

/** Returns the list of Loot objects
 *  
 * \return The list of loot objects for this entity
 *
 */
const list<Loot*>&
Entity::getLootList(void) const
{
  return lootList;
}

/** Generate the loot of this entity
  *
  * Add the loot defined in lootList to the given map.
  *
  * \param m The given map.
  *
  */
void
Entity::generateLoot(Map* m)
{
  cout << "Handling " << getName() << " loot" << endl;
  for (auto l : lootList)
    {
      if (l->get())
	{
	  auto e = l->getEntity();
	  cout << "Looting " << e->getName() << endl;
	  e->move(getX(), getY());
	  m->add(e);
	}
    }

  cout << endl;
}

/** Set the Exp geven by this entity when killed 
  *
  * \param ewk A positive, non-null integer.
  */
void
Entity::setExpWhenKilled(int ewk)
{
  expWhenKilled = ewk;
}

/** Get the number of Exp this entity gives when killed 
  *
  * \return An integer. Default to 0.
  */
int
Entity::getExpWhenKilled() const
{
  return expWhenKilled;
}

/** Does this entity can be equipped in inventory */
bool
Entity::isEquipable(void) const
{
  return false;
}

/** Remove the given action verb
  *
  * \param verb The action to be removed from actions map.
  *
  */
void
Entity::removeAction(const std::string& verb)
{
  actions.erase(verb);
}

