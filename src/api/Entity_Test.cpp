#include "Entity.hpp"
#include <gtest/gtest.h>

#include <iostream>

using namespace std;

// Test that we correctly handle name/species
TEST( Entity, get_name )
{
  Entity e("aze");
  ASSERT_EQ(e.getName(), "aze");
}

TEST( Entity, get_name_with_species )
{
  Entity e("aze");
  e.setSpecies("cat", 1);
  ASSERT_EQ(e.getName(), "aze (cat 1)");
}

TEST( Entity, add_action )
{
  Entity e("aze");
  e.add("eat", [&](rltk::entity_t* pl, Map*) {
      //
    });
  ASSERT_EQ(e.can("eat"), true);
}

TEST( Entity, remove_action )
{
  Entity e("aze");
  e.add("eat", [&](rltk::entity_t* pl, Map*) {    });
  e.removeAction("eat");
  ASSERT_EQ(e.can("eat"), false);
}
