#include "VcharBuffer.hpp"

#include "Logger.hpp"

#include <iostream>

using namespace std;
using namespace rltk;

VcharBuffer::VcharBuffer():
  term_width(0),
  term_height(0),
  map_center_x(0),
  map_center_y(0)
{

}

size_t
VcharBuffer::getSize() const
{
  return buffer.size();
}

void
VcharBuffer::resize(int width, int height)
{
  buffer.resize(width * (height + 1));
  term_width = width;
  term_height = height;
}

void
VcharBuffer::clear()
{
  std::fill(buffer.begin(), buffer.end(), vchar{ 32, {255,255,255}, {0,0,0} });
}

void
VcharBuffer::set_char(int x, int y, const rltk::vchar& c)
{
  auto idx = at(x, y);
  if (idx > getSize())
    {
      /*      LOG(LogLevel::Error, "set_char(%d) exeed buffer size (%d). "
	      "Game will crash", idx, getSize()); */
      idx = getSize() - 1;
    }
  buffer[idx] = c;
}

void
VcharBuffer::draw(virtual_terminal* t)
{
  t->clear();
  int i = 0;
  for (auto c : buffer)
    t->set_char(i++, c);
}

const rltk::vchar&
VcharBuffer::getChar(int x, int y)
{
  return buffer[at(x, y)];
}

void
VcharBuffer::setMapCenter(int x, int y)
{
  map_center_x = x;
  map_center_y = y;
}

int
VcharBuffer::getMapCenterX(void)
{
  return map_center_x;
}

int
VcharBuffer::getMapCenterY(void)
{
  return map_center_y;
}

int
VcharBuffer::getWidth() const
{
  return term_width;
}

int
VcharBuffer::getHeight() const
{
  return term_height;
}
