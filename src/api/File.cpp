#include "File.hpp"

#include <glob.h>   // USES glob(), globfree()
#include <string.h> // USES memset()
#include <vector>
#include <stdexcept>
#include <string>
#include <sstream>


using namespace std;

/** Return a list of files from the given pattern
  *
  * Note : relative directpry is supported (i.e. '..').
  * Note : use a '*' wildcard character.
  *
  */
std::vector<std::string>
File::fglob(const std::string& pattern)
{
  // Based on https://stackoverflow.com/a/8615450
  
  using namespace std;

  // glob struct resides on the stack
  glob_t glob_result;
  memset(&glob_result, 0, sizeof(glob_result));
  
  // do the glob operation
  int return_value = glob(pattern.c_str(), GLOB_TILDE, NULL, &glob_result);
  if (return_value != 0 && return_value != GLOB_NOMATCH)
    {
      globfree(&glob_result);
      stringstream ss;
      ss << "glob() failed with return_value " << return_value
	 << "(" << globErrorToStr(return_value) << ")"<< endl;
      throw std::runtime_error(ss.str());
    }
  
  // collect all the filenames into a std::list<std::string>
  vector<string> filenames;
  for(size_t i = 0; i < glob_result.gl_pathc; ++i)
    filenames.push_back(string(glob_result.gl_pathv[i]));
  
  // cleanup
  globfree(&glob_result);
  
  // done
  return filenames;
}

std::string
File::globErrorToStr(int v)
{
  string s;
  switch (v)
    {
    case GLOB_NOSPACE:
      s = "Out of memory";
      break;
    case GLOB_ABORTED:
      s = "Read error";
    case GLOB_NOMATCH:
      s = "No found matches";
    }

  return s;
}
