#include "Logger.hpp"

#include <stdarg.h>

#include <iostream>
#include <sstream>
#include <ctime>       // USES time()
#include <vector>

#include "config.h" // USES VERSION_STRING

using namespace std;

Logger* static_logger; // Declared extern in Logger.hpp, instanciated in main()

/**Logger constructor
  * 
  * \param vFilename  The file relative name.
  * \param vThreshold The log level we start to print at.
  *
  */
Logger::Logger(const string& vFilename, const LogLevel vThreshold,
	       LogMediaType vType = LOG_MEDIA_CERR | LOG_MEDIA_FILE):
  filename(vFilename),
  threshold(vThreshold),
  type(vType)
{
  file = fopen(vFilename.c_str(), "wa");
  if (file == NULL)
    cout << "Error while opening log file '" << vFilename << "'" << endl;
}

/** The logger destructor
  *
  */
Logger::~Logger()
{
  fclose(file);
}

void
Logger::intro()
{
  
  vector<string> sl
    {
     "\n"
     "Welcome to " VERSION_STRING "\n",
     "Feel free to report bug at " REPORT_URL "\n",
     "\n",
     "Compiled for " OS " (" NBITS " bits) on " __DATE__ " " __TIME__ ".\n",
     "\n"
    };
  
  for (auto s : sl)
    {
      if (type & LOG_MEDIA_CERR)
	fprintf(stdout, s.c_str());
      
      if (type & LOG_MEDIA_FILE)
	fprintf(file, s.c_str());
    }
}


void
Logger::log(const std::string& filename, int line,LogLevel lvl,
	    const char* fmt...)
{
  if (shouldShow(lvl))
    {
      va_list args;
      char buf[80];
      auto epoch = std::time(nullptr); // NOW
      struct tm ts = *localtime(&epoch);
      strftime(buf, sizeof(buf), "%H:%M:%S", &ts);

      string level = levelToStr(lvl);

#ifndef _WIN32      

      if (type & LOG_MEDIA_CERR)
	{
	  // See https://stackoverflow.com/a/9309341
	  // The second time we use args after vfprintf use, it content
	  // is undefined
	  va_start(args, fmt);
	  // Write to console
	  fprintf(stderr, "%s %s:%d %s [%s]  ", vt100(34, buf).c_str(),
		  filename.c_str(), line,
		  level.c_str(), vt100(31, lastPart(), 39).c_str()); 
	  vfprintf(stderr, fmt, args);
	  fprintf(stderr, "\n"); 
	  va_end(args);
	}
#endif      

      if (type & LOG_MEDIA_FILE)
	{
	  // Write to file
	  va_start(args, fmt);
	  strftime(buf, sizeof(buf), "%Y.%m.%d-%H:%M:%S", &ts);
	  
	  fprintf(file, "%s %s:%d %s [%s] ", buf,  filename.c_str(), line,
		  level.c_str(), lastPart().c_str()); 
	  vfprintf(file, fmt, args);
	  fprintf(file, "\n");
	  va_end(args);
	  
	  va_end(args);
	}
    }
}

void logw(const std::string&, int, LogLevel, const char*...)
{

}


string
Logger::levelToStr(const LogLevel ll) const
{
  switch (ll)
    {
    case Debug:
      return "DD";
      break;
    case Info:
      return "II";
      break;
    case Warning:
      return "WW";
      break;
    case Error:
      return "EE";
      break;
    case Critical:
      return "CC";
      break;
    default:
      return "Undefined";
     }
  
}

/** Should we show this message ? */
bool
Logger::shouldShow(const LogLevel lvl) const
{
  return lvl >= threshold;
}

/** Push the given string to the parogram parts stack
  *
  */
void
Logger::push(const std::string& part)
{
  parts.push(part);
}

void
Logger::pop()
{
  parts.pop();
}

/** Because if parts stack is empty, printing parts.top() cause a segfault 
  *
  */
string
Logger::lastPart()
{
  if (!parts.empty())
    return parts.top().c_str();
  else
    return "";
}

// For color code, see https://misc.flogisoft.com/bash/tip_colors_and_formatting
// out should mostly be 39 (default for foreground)
string
Logger::vt100(int i, std::string str , int o)
{
  ostringstream oss;
  //  oss << "\033[" << i << "m " << str <<  "\033[" << o << "m";
  oss << "\033[" << i << "m" << str <<  "\033[" << o << "m";
  return oss.str();
}

void
Logger::setThreshold(const LogLevel t)
{
  this->threshold = t;
}

const LogLevel
Logger::getThreshold(void) const
{
  return this->threshold;
}

void
Logger::setMediaType(LogMediaType lmt)
{
  type = lmt;
}

LogMediaType
Logger::getMediaType(void) const
{
  return type;
}
