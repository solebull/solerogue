#ifndef __ENTITY_MANAGER_HPP__
#define __ENTITY_MANAGER_HPP__

#include "Singleton.hpp"

#include <list>
#include <cstdint>

// Forward declarations
class Entity;
// End of forward declarations

using namespace std;

class EntityList
{
public:
  EntityList();
  
  void add(Entity*);
  const list<Entity*> getEntities() const;

  void remove(Entity*);
  
private:
  list<Entity*> entities;
  
};

#endif // !__ENTITY_MANAGER_HPP__
