#ifndef _LOOT_HPP__
#define _LOOT_HPP__

// Forward declaration
class Entity;
// End of forward declaration

class Loot
{
public:
  Loot(Entity*, int);

  bool get(void);
  Entity* getEntity(void);
  
private:
  Entity* entity;
  int     chance;
  
};

#endif // !_LOOT_HPP__
