#ifndef _ATTRIBUTES_HPP_
#define _ATTRIBUTES_HPP_

#include <map>
#include <string>

#include "Level.hpp"
using namespace std;

/** Handle named attributes
  *
  * If the an attribute can be found with the same name + '-max', it will
  * never be greater than this one :
  *   health
  *   health-max
  *
  */
class Attributes
{
public:
  Attributes();

  void intInc(const string&, int);
  void intDec(const string&, int);

  int getInt(const string&);

  void setInt(const string&, int);
  Level& getLevel(const string&);
  
private:
  map<string, int> intMap;
  map<string, Level> lvlMap;
};

#endif // !_ATTRIBUTES_HPP_
