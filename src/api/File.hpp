#ifndef _FILE_HPP_
#define _FILE_HPP_

#include <string>
#include <vector>

/** A global mode-usable class with standard file/filesystem related actions
  *
  */
class File
{
public:
  static std::vector<std::string> fglob(const std::string& pattern);

protected:
  static std::string globErrorToStr(int);

};

#endif // !_FILE_HPP_
