
#include "Level.hpp"

Level::Level(int vNext):
  exp(0),
  next(vNext),
  level(1)
{

}

int
Level::getLevel(void) const
{
  return level;
}

int
Level::getExp(void) const
{
  return exp;
}

int
Level::getNext(void) const
{
  return next;
}

/** Increment the current level by vExp experience points
  *
  */
bool
Level::inc(int vExp)
{
  exp+=vExp;
  if (exp > next)
    {
      level++;
      exp -= next;
      next *= 1.2;
      return true;
    }

  return false;
}

