#ifndef _LOGGER_HPP_
#define _LOGGER_HPP_

#include <string>
#include <stdio.h> // USES FILE
#include <stack>

#include "config.h" // USES __FILENAME__ on win64

#define LOG(LVL, ...) static_logger->log(__FILENAME__, __LINE__, LVL, __VA_ARGS__)

#define LPUSH(PART) static_logger->push(PART)
#define LPOP(PART) static_logger->pop()

// Forward declarations
class Logger;
// End of forward declarations

extern Logger* static_logger; // Really defined in main.cpp

/** The level of a message and default level to print 
  *
  */
enum LogLevel
  {
   Debug = 0,
   Info, 
   Warning,
   Error,
   Critical
  };

/** The media we want to print log message in
  *
  * These values will be 
  *
  */
// Output on console : 2^0, bit 0
#define LOG_MEDIA_CERR 1
// Output in logfile .2^1, bit 1. Next will be 4, 8 ...
#define LOG_MEDIA_FILE 2 

typedef unsigned char LogMediaType; 
  
/** The logger class 
  *
  * Logger implements a program parts stack that only show the latest added.
  * Use push and pop to checnge last one.
  *
  * If you get really strange characters while printing std::string from STL
  * in printf-like chars, please call .c_str().
  *
  */
class Logger
{
public:
  Logger(const std::string&, const LogLevel, LogMediaType);
  virtual ~Logger();

  void log (const std::string&, int, LogLevel, const char*...);
  void logw(const std::string&, int, LogLevel, const char*...);

  void push(const std::string&);
  void pop();

  std::string lastPart();
  void        intro();

  void           setThreshold(const LogLevel);
  const LogLevel getThreshold(void) const;

  void         setMediaType(LogMediaType);
  LogMediaType getMediaType(void) const;
  
protected:
  std::string levelToStr(const LogLevel) const;
  bool        shouldShow(const LogLevel lvl) const ;
  std::string vt100(int, std::string, int o = 39);
  
private:
  std::string  filename;
  LogLevel     threshold;
  LogMediaType type;
  FILE*        file;
  std::stack<std::string> parts;  //!< Program parts
};

#endif //! _LOGGER_HPP_
