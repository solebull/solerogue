#ifndef __ENTITY_HPP__
#define __ENTITY_HPP__


#include <string>
#include <functional>   // USES std::function
#include <map>
#include <list>
#include <vector>

#include "rltk.hpp"
#include "Attributes.hpp"

// Forward declarations
class Map;
class Loot;
class Weapon;
// End of forward declarations

using namespace rltk;

class Entity
{
public:
  using ActionFn   = std::function<void(rltk::entity_t*, Map*)>;
  using OnAttackedFn = std::function<void(int)>;
  //  using OnAttackFn = std::function<void(entity*)>;
  
  Entity(const std::string &);

  void setName(const std::string&);
  void setDescription(const std::string&);
  void setSpecies(const std::string&, int);
  const std::string& getSpecies(void);
  
  std::string        getName(void) const;
  const std::string& getDescription(void) const;
    
  void    move(int x, int y);
  uint8_t getGlyph(void);
  void    setGlyphId(const std::string&);
  void    setForegroundColor(int r, int g, int b);
  void    setBackgroundColor(int r, int g, int b);

  const color_t& getForegroundColor(void) const;
  const color_t& getBackgroundColor(void) const;
  
  const vchar getVChar();
  const vchar getDarkVChar();
  int getX(){ return x;}
  int getY(){ return y;}

  void  setWeight(float);
  float getWeight(void) const;

  // verb/action handling
  void add(const std::string& verb, ActionFn action);
  void act(const std::string& verb);
  bool can(const std::string& verb);
  virtual std::list<std::string> possibleActions(void) const;
  void removeAction(const std::string& verb);

  // Standard callbacks
  void onAttacked(OnAttackedFn);

  bool attack(int);
  
  // walkable/deathBy
  void               setWalkable(bool);
  bool               isWalkable(void) const;
  void               setDeathBy(const std::string&, int);
  const std::string& getDeathBy(void) const;
  int                getDeathByValue(void) const;

  bool isEnemy(void);
  void setEnemy(bool);

  Attributes* getAttributes(void);

  void    addWeapon(Weapon*);
  Weapon* getRandomWeapon(void);

  void               addLoot(Loot*);
  const list<Loot*>& getLootList(void) const;
  void               generateLoot(Map*);

  virtual bool isEquipable(void) const;

  void setExpWhenKilled(int);
  int  getExpWhenKilled(void) const;
  
protected:
  std::string name;        //!< The name of the entity
  std::string description; //!< Textual description
  
  uint8_t glyph;          //!< Maybe uint32_t
  std::string glyphid;    //!< Key to the TranslationManager glyph map
  color_t fg;             //!< Foreground color
  color_t darkfg;         //!< Dark foreground color
  color_t bg;             //!< Background color

  int x;  //!< X position of the entity
  int y;  //!< Y position of the entity

  float weight;                  //!< Weight of the Entity in Kg
  std::map<std::string, ActionFn> actions; //!< verb/action map

  bool        walkable;           //!< Can the player land on this entity
  std::string deathby;            //!< If injured by this entity
  int         deathbyValue;       //!< Removed health each time we touch it

  bool enemy;          //!< Is this shown in the EnemyInfoSystem layer

  OnAttackedFn onAttackedFn; //!< The callback executed when attacked

  Attributes att;  //!< Attributes of the entity
  /** Will not use Character here. Too much human-orinted */
  vector<Weapon*> weaponList;

  list<Loot*> lootList;    //!< A List a Loot objects

  std::string species;     //!< The species text (translatable)
  int         speciesIdx;  //!< Index into the species

  int expWhenKilled; //!< How many Exp given when this entity is killed ?
};

#endif // !__ENTITY_HPP__
