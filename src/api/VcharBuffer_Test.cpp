#include "VcharBuffer.hpp"
#include <gtest/gtest.h>

#include <iostream>

using namespace std;

TEST( VcharBuffer, getSize )
{
  VcharBuffer vb;
  ASSERT_EQ(vb.getSize(), 0);
}

TEST( VcharBuffer, resize )
{
  VcharBuffer vb;
  vb.resize(10, 10);
  ASSERT_EQ(vb.getSize(), 110);
}

TEST( VcharBuffer, at )
{
  VcharBuffer vb;
  vb.resize(10, 10);
  ASSERT_EQ(vb.at(2, 3), 32);
}

TEST( VcharBuffer, map_center )
{
  VcharBuffer vb;
  vb.setMapCenter(10, 20);
  ASSERT_EQ(vb.getMapCenterX(), 10);
  ASSERT_EQ(vb.getMapCenterY(), 20);
}

TEST( VcharBuffer, get_width )
{
  VcharBuffer vb;
  vb.resize(10, 12);
  ASSERT_EQ(vb.getWidth(), 10);
  vb.resize(20, 12);
  ASSERT_EQ(vb.getWidth(), 20);
}

TEST( VcharBuffer, get_height)
{
  VcharBuffer vb;
  vb.resize(17, 12);
  ASSERT_EQ(vb.getWidth(), 17);
  vb.resize(20, 22);
  ASSERT_EQ(vb.getHeight(), 22);
}
