#ifndef __LEVEL_HPP__
#define __LEVEL_HPP__

/** Define a level of experience, can be increased and when next level
  * theresold is reached, raise level
  * 
  */
class Level
{
public:
  Level(int next=100);

  int getLevel(void) const;
  int getExp(void) const;
  int getNext(void) const;

  bool inc(int);
  
private:
  int exp;
  int next;
  int level;
};

#endif // !__LEVEL_HPP__
