#include "Layers.hpp"

#include "rltk.hpp"

using namespace rltk;

constexpr int info_width = 240;
constexpr int log_height = 120;

// This is called when the screen resizes, to allow the GUI to redefine itself.
void resize_main(layer_t * l, int w, int h) {
  // Simply set the width to the whole window width, and the whole window minus 16 pixels (for the heading)
  l->w = w - info_width;
  l->h = h - 32 - log_height; 
  if (l->w < 0) l->w = 160; // If the width is too small with the log window, display anyway.
}

// This is called when the screen resizes, to allow the GUI to redefine itself.
void resize_info(layer_t * l, int w, int h) {
  // Simply set the width to the whole window width, and the whole window minus 16 pixels (for the heading)
  l->w = w - info_width;
  l->h = h - 32 - log_height;
  
  // If the log window would take up the whole screen, hide it
  if (l->w < 0) {
    l->console->visible = false;
  } else {
    l->console->visible = true;
  }
  l->x = w - info_width;
}

