#include "i18n.hpp"

#include "rltk.hpp" // USES sf::font

#include <cstring> // USES strlen

#include <iostream>
#include <sstream>
#include <cwchar>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

#include "api/Logger.hpp"

using namespace std;
namespace pt = boost::property_tree;

sf::Font default_font;
sf::Text default_text;
int default_pixels_size;
static sf::RenderWindow* default_window=nullptr;

sf::Font
get_default_font(void)
{
  return default_font;
}

// Mandatory call, sets default_window
void
set_default_font(const std::string& filename, int pixels_size)
{
  if (!default_font.loadFromFile(filename))
    throw std::runtime_error("Cannot load font file " + filename);
    
  default_text.setFont(default_font);
  default_text.setCharacterSize(pixels_size);
  default_pixels_size = pixels_size;
  
  default_window = rltk::get_window();
}

void
setTempFontSize(int ps)
{
  default_text.setCharacterSize(ps);
}

void
removeTempFontSize()
{
  default_text.setCharacterSize(default_pixels_size);
}


// Dummy overload
std::wstring
get_wstring(const std::wstring & s)
{
  return s;
}

// Real worker
std::wstring
get_wstring(const std::string & s)
{
  const char * cs = s.c_str();
  const size_t wn = std::mbsrtowcs(NULL, &cs, 0, NULL);

  if (wn == size_t(-1))
  {
    std::cout << "Error in mbsrtowcs(): " << errno << std::endl;
    return L"";
  }

  std::vector<wchar_t> buf(wn + 1);
  const size_t wn_again = std::mbsrtowcs(buf.data(), &cs, wn + 1, NULL);

  if (wn_again == size_t(-1))
  {
    std::cout << "Error in mbsrtowcs(): " << errno << std::endl;
    return L"";
  }

  assert(cs == NULL); // successful conversion

  return std::wstring(buf.data(), wn);
}

void print(rltk::virtual_terminal* t, int lx, int ly, int x, int y, string s,
	   const sf::Color& f, const sf::Color& b)
{
  if (!default_window)
    throw std::runtime_error("default window pointer is NULL. "
			     "Did you call set_default_font()");
  
  default_text.setString(get_wstring(s));
  default_text.setFillColor(f);
  default_text.setOutlineColor(b);
  //  default_text.setStyle(sf::Text::Bold | sf::Text::Underlined);

  sf::Vector2i vec(lx + (x*8), ly + (y*16));
  default_text.setPosition(default_window->mapPixelToCoords(vec));
  default_window->draw(default_text);

  //  auto default_window.getSize
}



TranslationManager::TranslationManager(token):
  country_code("en")
{

}

void
TranslationManager::parse(const std::string& path)
{
  string filename = path + "/" + country_code + ".ini";

  LPUSH("i18n");
  pt::ptree tree;
  try
    {
      pt::read_ini(filename, tree);
      for (auto it: tree.get_child("messages"))
	{
	  if (strings.find(it.first.data()) != strings.end())
	    LOG(LogLevel::Warning,
		"Adding duplicate translation key '%s' from file '%s'",
		it.first.data(), filename.c_str());
	  
	  strings[it.first.data()] = it.second.data();
	}
      for (auto it: tree.get_child("glyphs"))
	{
	  if (glyphs.find(it.first.data()) != glyphs.end())
	    LOG(LogLevel::Warning,
		"Adding duplicate glyph key '%s' from file '%s'",
		it.first.data(), filename.c_str());

	  if (it.second.data().size() > 1)
	    LOG(LogLevel::Warning,
		"WARNING: Glyph key '%s' from file '%s' "
		"is not a char. Using '%s' instead", 
		it.first.data(), filename, it.second.data()[0]);

	  glyphs[it.first.data()] = it.second.data()[0];
	}

    }
  catch(exception& e)
    {
      LOG(LogLevel::Error, "Error parsing file '%s' : %s", filename.c_str(),
	  e.what());
    }
  LPOP();
}

void
TranslationManager::setCountryCode(const std::string& lc)
{
  if (lc.size() > 2)
    LOG(LogLevel::Warning, "Only 2 chars country_code code are supported");

  country_code = lc;

  if (lc == "fr")
    {
      strings["Bare hand"] = "Main nue";
    }
}

const std::string&
TranslationManager::getCountryCode(void)
{
  return country_code;
}

const string&
TranslationManager::get(const string& key)
{
  if (country_code == "en" || key.empty())
    return key;

  if (strings.find(key) != strings.end())
    return strings[key];

  if (key != lastError)
    {
      LPUSH("i18n");
      LOG(LogLevel::Info, "Can't find translation for key '%s'", key.c_str());
      LPOP();
      lastError = key;
    }

  
  return key;
}

uint8_t
TranslationManager::getGlyph(const std::string& key)
{
  if (glyphs.find(key) != glyphs.end())
    return glyphs[key];

  if (key != lastError)
    {
      LPUSH("i18n");
      LOG(LogLevel::Info, "Can't find translation for glyph '%s'", key.c_str());
      LPOP();
      lastError = key;
    }
  return key[0];
}

std::string
TranslationManager::getHelpDir(void)
{
  return "help/" + country_code + "/";
}
