#include "rltk.hpp"

#include "api/Rng.hpp"
#include "api/Map.hpp"
#include "api/Logger.hpp"
#include "api/VcharBuffer.hpp"

#include <iostream>     // std::cout
#include <vector>     // std::cout
#include <algorithm>    // USES std::max


extern "C"
{
  void on_init();
  void on_generate(Map*);
  void on_move(Map* m, rltk::entity_t* pl){};
  void on_frame(double, VcharBuffer*);
}

// Mod vars
bool running = false;

double run_time = 300;
double remaining_time;

int from_x = 0;


void
on_init()
{
}

void
on_generate(Map* m)
{

}

/** Change saturation value of the given tile's foreground color
  *
  * \param vb The needed vertex buffer object
  *
  */
void
saturate(VcharBuffer* vb, int x, int y, float delta)
{
  // Try to access rltk map rendering (char/color from here)
  auto ch = vb->getChar(x, y);
  float h, s, v;
  std::tie(h, s, v) = rltk::color_to_hsv(ch.foreground);
  s += delta;
  if (s > 1.0) s = 1.0;
  if (s < 0.0) s = 0.0;

  rltk::color_t c2(h, s, v);
  ch.foreground = c2;
  vb->set_char(x, y, ch);
  
  // background
}

/** Got a very little chance to fire this
  *
  */
bool
fire()
{
  if (running) return true;
  
  if (rng::rdice(1, 500000 ) <5) // always
    {
      LOG(LogLevel::Info, "Firing saturate mod effect");
      running = true;
      remaining_time = run_time;
      from_x = 0;
      return true;
    }

  return false;
}

/** Called after each frame, with a modifiable rendered map in VcharBuffer */
void on_frame(double duration, VcharBuffer* vb)
{
  if (!fire())
    return;
  
  float s;
  
  for (int x = 0 + from_x; x<10 + from_x; ++x)
    for (int y = 0; y<vb->getHeight() - 1; ++y)
      {
	s = (float)y/100.0f;
	saturate(vb, x, y, 0.5f); // s
      }

  ++from_x;
  if (from_x > vb->getWidth()) running = false;

  // Count down
  remaining_time -= duration;
  //  LOG(LogLevel::Info, "Saturate mod remaining_time : %f", remaining_time);
  if (remaining_time <= 0)
    {
      running = false;
    }
}
