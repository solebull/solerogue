#include <iostream>

using namespace std;

#include "api/Rng.hpp"
#include "api/Map.hpp"
#include "api/Entity.hpp"
#include "api/Attributes.hpp"
#include "api/VcharBuffer.hpp"

#include "components/Inventory.hpp"
#include "systems/Player.hpp"


extern "C"
{
  void on_init(){}
  void on_generate(Map*);
  void on_move(Map* m, rltk::entity_t* pl){}
  void on_frame(double, VcharBuffer*){}
}

void
on_generate(Map* m)
{
  Entity* e = new Entity("Cactus");
  e->setDescription("A dangerous mexican object");
  e->setGlyphId("cactus");
  e->setForegroundColor(55, 255, 51);
  e->setBackgroundColor(80, 80, 80);
  e->setWeight(0.3);
  e->setWalkable(false);
  e->setDeathBy("cactus", 5);

  int x, y;
  rng::get_random_floor(m, &x, &y);
  e->move(x, y);
  m->add(e);
}
