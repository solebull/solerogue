#include <iostream>

using namespace std;

#include "api/Rng.hpp"
#include "api/Map.hpp"
#include "api/Loot.hpp"
#include "api/Entity.hpp"
#include "api/Weapon.hpp"
#include "api/Attributes.hpp"

#include "components/Inventory.hpp"
#include "systems/Player.hpp"

#include "namegen.h"

#include <list>
#include <cmath>

using namespace std;

extern "C"
{
  void on_init();
  void on_generate(Map*);
  void on_move(Map* m, rltk::entity_t* pl);
}

int dognb = 1;

list<Entity*> es;
Weapon w1("jaw");
Weapon w2("claws");


void
on_init()
{
  cout << "From 01-so module" << endl;
}

Entity* get_dog(const string& name)
{
  Entity* e = new Entity(name);
  e->setDescription("A non agressive pet");
  e->setGlyphId("dog");
  e->setForegroundColor(255, 40, 0);
  e->setBackgroundColor(60, 60, 0);
  e->setWeight(0.8);
  e->setWalkable(false);
  e->setDeathBy("dog", 0);

  auto att = e->getAttributes();
  att->setInt("health", 40);
  att->setInt("health-max", 40);

  e->setExpWhenKilled(55);
  
  w1.setInjuryName("bite");
  w1.setOnDamage([]() -> int
    {
      return 2 + rng::rdice(1, 4);
    });
  e->addWeapon(&w1);

  w2.setInjuryName("scratches");
  w2.setOnDamage([]() -> int
    {
      return rng::rdice(1, 4);
    });
  e->addWeapon(&w2);

  Entity* corpse = new Entity("Dog "+name+" corpse");
  corpse->setGlyphId("dog-corpse");
  corpse->setDescription("The corpse of "+name+", a dead dog.");
  corpse->setBackgroundColor(255, 40, 0);
  corpse->setForegroundColor(60, 60, 0);
  corpse->setWeight(0.8);
  corpse->setWalkable(true);
  e->addLoot(new Loot(corpse, 100));

  Entity* peek = new Entity("peek");
  peek->setDescription("Thanks for 200 twitch followers");
  peek->setGlyphId("peek");
  peek->setBackgroundColor(60, 40, 0);
  peek->setForegroundColor(00, 150, 0);
  peek->setWeight(0.01);
  peek->setWalkable(true);
  peek->add("eat", [peek](rltk::entity_t* pl, Map* ma)
    {
      pl->component<Inventory>()->consume(peek);
      auto att = pl->component<Attributes>();
      att->intInc("health", 40);
    });
  e->addLoot(new Loot(peek, 1));
  
  return e;
}

void
on_generate(Map* m)
{
  // Get a random name
  string name;
  auto namedice = rng::rdice(1, 100);
  if (namedice > 2)
    {
      NameGen::Generator tgen("ss(luy|ily|ash|dg)");
      name = tgen.toString();
      name[0] = toupper(name[0]);
      LOG(LogLevel::Info, "Dog has a random fantasy name : '%s'", name.c_str());
    }

  if (rng::rdice(1, 10) == 1) // 10%
    {
      Entity* e = get_dog(name);
      e->setSpecies("dog", dognb++);
      
      int x, y;
      rng::get_random_floor(m, &x, &y);
      e->move(x, y);
      
      e->add("release", [e](rltk::entity_t* pl, Map* ma)
			{
			  pl->component<Inventory>()->consume(e);
			  auto pos = pl->component<Position>();
			  e->move(pos->getX(), pos->getY());
			  // Should get current map
			  ma->add(e);
			});
      
      e->onAttacked([e](int damage)
		    {
		      e->setEnemy(true);
		    });
      
      m->add(e);
      es.push_back(e);
    }
}


void on_frame(double duration)
{
  
}

void
move_randomly(Entity* e)
{
  // Move randomly !
  int x = e->getX();
  int y = e->getX();
  int d = rng::rdice(1, 5);
  switch (d)
    {
    case 1:
      --x;
      break;
    case 2:
      ++x;
      break;
    case 3:
      --y;
      break;
    case 4:
      ++y;
      break;
    }
  e->move(x, y);
}

void on_move(Map* m, rltk::entity_t* pl)
{
  
  for (auto e : es)
    {

      auto pos = pl->component<Position>();
      
      // https://fr.wikipedia.org/wiki/Distance_entre_deux_points_sur_le_plan_cart%C3%A9sien
      double d = sqrt(pow( pos->getX() - e->getX(), 2) +
		      pow( pos->getY() - e->getY(), 2));

      if (!e->isEnemy())
	{
	  move_randomly(e);
	}
      else
	{
	  if (d > 2)
	    move_randomly(e);
	}
    }
}
