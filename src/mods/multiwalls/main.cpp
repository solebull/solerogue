#include <iostream>

/** MultiWalls mod :
  *
  * Draw walls using multiple tiles.
  *
  */

using namespace std;

#include "api/Rng.hpp"
#include "api/Map.hpp"
#include "api/Entity.hpp"
#include "api/Attributes.hpp"

#include "components/Inventory.hpp"
#include "systems/Player.hpp"

#include <vector>

#include "namegen.h"

using namespace std;

extern "C"
{
  void on_init();
  void on_generate(Map*);
  int on_getTile(int, int, int, bool);
}

/** Keep the idex of each wall in the current map */
vector<int> walls;
int map_width;

void
on_init()
{

}

int
random_wall(void)
{
  int d = rng::rdice(1, 6);
  if (d == 4) return 177;
  if (d > 4)  return 178;
  return 176;
}

void
on_generate(Map* m)
{
  // Must create a array of the Map size and add a inde in this
  map_width = m->getWidth();
  int nb = map_width * m->getHeight();
  walls.resize(nb);
  LPUSH("[mod]multiwall");
  LOG(LogLevel::Info, "Generating walls map of size '%d'", nb);
  LPOP();
  for (int i=0; i < nb; ++i)
    {
      walls[i] = random_wall();
    }
}

int
on_getTile(int depth, int x, int y, bool isWall)
{
  if (!isWall)
    return -1;
  
  int w = walls[(y * map_width) + x]; 
  //  cout << "Returning wall " << w << endl;
  LPUSH("[mod]multiwall");
  //LOG(LogLevel::Debug, "Returning wall tile #%d", w);
  LPOP();
  return w;
}
