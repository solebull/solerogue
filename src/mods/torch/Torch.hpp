#ifndef __TORCH_HPP__
#define __TORCH_HPP__

#include "api/Entity.hpp"

// Forward declarations
class VcharBuffer;
// End of forward declarations

extern int colors_vec[6][3];

class Torch : public Entity
{
public:
  Torch(double duration_s);
  void onFrame(double duration_ms, VcharBuffer* vb);
  void brighten(VcharBuffer* vb, int x, int y, int v, int col_idx);

  void setOn(bool);
  double getTorchDuration(void);
  double getRemainingDuration(void);

  void setPercent(int);
  int computePercent(double);
  
protected:
  void draw(VcharBuffer* vb);
  
private:
  double torch_duration_ms; // Light duration in milliseconds
  int    color_index;       // The current color index
  bool   on;                // Is the torch lighting ?
  double change_color;      //!< change color every x milliseconds
  double color_delta;       //!< Change color when > change_color
  double remaining_duration; // Seconds to milliseconds
};

#endif // __TORCH_HPP__
