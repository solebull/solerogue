#include "Torch.hpp"

#include <gtest/gtest.h>


TEST( Torch, init )
{
  Torch t(10);
  
  ASSERT_EQ( t.getName(), "Torch" );
  ASSERT_EQ( t.getTorchDuration(), 10000 );
  ASSERT_EQ( t.getRemainingDuration(), 10000 );
}


TEST( Torch, setPercent )
{
  Torch t(10);
  t.setPercent(40);
  ASSERT_EQ( t.getName(), "Torch (40%)" );
  t.setPercent(80);
  ASSERT_EQ( t.getName(), "Torch (80%)" );
}

TEST( Torch, computePercent )
{
  Torch t(10);
  ASSERT_EQ( t.computePercent(0),   100 );
  ASSERT_EQ( t.computePercent(1000), 90 );
}
