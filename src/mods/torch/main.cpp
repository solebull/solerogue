#include <iostream>

using namespace std;

#include "api/Rng.hpp"
#include "api/Map.hpp"
#include "api/Loot.hpp"
//#include "api/Entity.hpp"
#include "api/Weapon.hpp"
#include "api/Attributes.hpp"

#include "components/Inventory.hpp"
#include "systems/Player.hpp"

#include <list>
#include <cmath>

#include "Torch.hpp"

using namespace std;

list<Torch*> torch_list;

extern "C"
{
  void on_init();
  void on_generate(Map*);
  void on_move(Map* m, rltk::entity_t* pl){};
  void on_frame(double, VcharBuffer*);
}

void add_TOn(Torch* e);
void add_TOff(Torch* e);


void
on_init()
{
  cout << "From 01-so module" << endl;
}

/** Turn off and reset actions of the given Enttyi (a torch)
  *
  */
void
turn_off(Torch* e)
{
  e->removeAction("Turn off");
  e->setOn(false);
  add_TOn(e);
}

void
add_TOff(Torch* e)
{
  e->add("Turn off", [e](rltk::entity_t* pl, Map*)
    {
      turn_off(e);
    });
}

void
add_TOn(Torch* e)
{
  e->add("Turn on", [e](rltk::entity_t* pl, Map*)
    {
      e->removeAction("Turn on");
      e->setOn(true);
      add_TOff(e);
    });
}

Torch* get_torch()
{
  Torch* e = new Torch(180);
  e->setDescription("Light up your steps");
  e->setGlyphId("torch");
  e->setSpecies("100%", -1);
  auto col = 0;
  e->setForegroundColor(colors_vec[col][0],
			colors_vec[col][1],
			colors_vec[col][2]);
  e->setBackgroundColor(15, 15, 15);
  e->setWeight(0.8);
  e->setWalkable(true);
  add_TOn(e);

  e->add("drop", [e](rltk::entity_t* pl, Map* ma)
    {
      pl->component<Inventory>()->consume(e);
      auto pos = pl->component<Position>();
      e->move(pos->getX(), pos->getY());
      turn_off(e);
      ma->add(e);
    });

  return e;
}

void
on_generate(Map* m)
{
  //  if (rng::rdice(1, 10) == 1) // 10%
    {
      Torch* torch = get_torch();
      torch_list.push_back(torch);
      int x, y;
      x = 10; y = 10;
      //rng::get_random_floor(m, &x, &y);
      torch->move(x, y);
      m->add(torch);
    }
}

/** Called after each frame, with a modifiable rendered map in VcharBuffer */
void on_frame(double duration, VcharBuffer* vb)
{
  for (auto torch: torch_list)
    torch->onFrame(duration, vb);
}
