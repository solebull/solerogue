#include "Torch.hpp"

#include "api/VcharBuffer.hpp"
#include "api/Rng.hpp"

// Colors from https://www.schemecolor.com/fire-color-scheme.php
int colors_vec[6][3] =
  {
   {128, 17, 0},   // Barn Red
   {182, 34, 3},   // International Orange (Engineering)
   {215, 53, 2},   // Sinopia
   {252, 100, 0},  // Orange
   {255, 117, 0},  // Philippine Orange
   {250, 192, 0},  // Golden Poppy
  };


Torch::Torch(double duration_s):
  Entity("Torch"),
  torch_duration_ms(duration_s * 1000),
  color_index(0),
  on(false),
  change_color(30),
  color_delta(change_color + 1),
  remaining_duration(torch_duration_ms)
{
  
}

void
Torch::onFrame(double duration_ms, VcharBuffer* vb)
{
  if (!on || remaining_duration <= 0)
    return;
  
  if (color_delta > change_color)
    {
      color_index = rng::rdice(1, 6) - 1;
      color_delta = 0;
    }
  else
    color_delta += duration_ms;
    
  setForegroundColor(colors_vec[color_index][0],
		     colors_vec[color_index][1],
		     colors_vec[color_index][2]);
  
  //  cout << duration << " " << remaining_duration << endl;
    setPercent(computePercent(duration_ms));
  draw(vb);
}

int
Torch::computePercent(double duration_ms)
{
  remaining_duration -= duration_ms;
  //  double pc = (torch_duration_ms * 100) / remaining_duration;
  double pc = (remaining_duration * 100) / torch_duration_ms;
  return (int)pc;
}

void
Torch::draw(VcharBuffer* vb)
{
  int cx=  vb->getMapCenterX();
  int cy=  vb->getMapCenterY();

  int range = 12 + rng::rdice(1, 6);
  
  for (int x = vb->getMapCenterX()-range; x <= vb->getMapCenterX()+range; ++x)
    {
      for (int y = vb->getMapCenterY() - range;
	   y <= vb->getMapCenterY() + range; ++y)
	{

	  // Distance with map center
	  double d = sqrt(pow( x - cx, 2) + pow( y - cy, 2));

	  if (d < range)
	    brighten(vb, x, y, (range-d) * (20-range), color_index);
	}
    }
}

/// col_idx is an index from the colors_vec
void
Torch::brighten(VcharBuffer* vb, int x, int y, int v, int col_idx)
{
  auto vc = vb->getChar(x, y);

  color_t m(30, 30, 30);
  /*
  cout << vc.glyph << ": '" << (char)vc.glyph << "'" << endl;
  if (vc.glyph != 46) // '.'
    m = color_t(15, 15, 15);
  
  if (vc.glyph == 35) // '#'
    return;
  */
      
  color_t c(v, v, v);
  color_t fc(colors_vec[col_idx][0] / 10,
	     colors_vec[col_idx][1] / 10,
	     colors_vec[col_idx][2] / 10);
  vc.background = ((vc.background + fc) + c) - m;
  vb->set_char(x, y, vc);
}

void
Torch::setOn(bool b)
{
  this->on = b;
}

/** Get the total light duration in milliseconds
  *
  */
double
Torch::getTorchDuration()
{
  return torch_duration_ms;
}

double
Torch::getRemainingDuration(void)
{
  return remaining_duration;
}


void
Torch::setPercent(int percent)
{
  setSpecies(to_string(percent) + "%", -1);
}

