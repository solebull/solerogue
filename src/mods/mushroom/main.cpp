#include <iostream>

using namespace std;

#include "api/Rng.hpp"
#include "api/Map.hpp"
#include "api/Entity.hpp"
#include "api/Attributes.hpp"
#include "api/VcharBuffer.hpp"

#include "components/Inventory.hpp"
#include "systems/Player.hpp"

#include "messages/AddLogEntry.hpp"

extern "C"
{
  void on_init();
  void on_generate(Map*);
  void on_frame(double, VcharBuffer*);
  int on_getTile(int, int, int, bool);
}

const string msg = "Ouch! This mushroom tasted terrible!"; //!< Eat message
bool poisoned    = false; //!< Are we currently poisonned
double cooldown  = 0;     //!< The current cooldown in ms

void
on_init()
{
  cout << "From standard-food module" << endl;
}

Entity*
get_mushroom()
{
  Entity* e = new Entity("Mushroom");
  e->setDescription("A fungus speciality. Not necessarily eatable.");
  e->setGlyphId("mushroom");

  // Random color (mostly red/yellowish)
  auto d=rng::rdice(1, 4);
  if      (d==1) e->setForegroundColor(199, 0, 57);
  else if (d==2) e->setForegroundColor(255, 87, 51);
  else if (d==3) e->setForegroundColor(255, 195, 0);
  else           e->setForegroundColor(144, 12, 63);
  
  e->setBackgroundColor(200, 200, 200);
  e->setWeight(0.3);
  e->add("eat", [e](rltk::entity_t* pl, Map*)
    {
      pl->component<Inventory>()->consume(e);
      auto att = pl->component<Attributes>();
      att->intDec("health", 3);
      emit(AddLogEntryMessage(msg));
      poisoned = true;
      cooldown += 800;
      cooldown += (rng::rdice(1, 6) * 100);
      LOG(LogLevel::Info, "You're poisonned for %f ms", cooldown);
    });
  return e;
}


/** On map generation, randomly place some mushrooms 
  *
  * \param m The map to be mushroomed!.
  *
  */
void
on_generate(Map* m)
{
  auto chance = rng::rdice(1, 6) - 4;
  if (chance < 1)
    return;
  
  for (int i = 0; i < chance; ++i)
    {
      auto e = get_mushroom();
      int x, y;
      rng::get_random_floor(m, &x, &y);
      e->move(x, y);
      m->add(e);
    }
}


void
on_frame(double duration, VcharBuffer*)
{
  if (poisoned)
    {
      cooldown -=  duration;
      if (poisoned && cooldown<0)
	{
	  LOG(LogLevel::Info, "You're not poisonned anymore");
	  poisoned=false;
	  cooldown=6000;
	}
    }
  
}

int
on_getTile(int depth, int x, int y, bool isWall)
{

  if (poisoned)
    {

      int d = rng::rdice(1, 6);
      if (d == 4) return 177;
      if (d > 4)  return 178;
      return 176;
    }
  else
    return 176;
}
