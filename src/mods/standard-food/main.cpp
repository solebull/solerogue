#include <iostream>

using namespace std;

#include "api/Rng.hpp"
#include "api/Map.hpp"
#include "api/Entity.hpp"
#include "api/Attributes.hpp"

#include "components/Inventory.hpp"
#include "systems/Player.hpp"

extern "C"
{
  void on_init();
  void on_generate(Map*);
  void on_move(Map* m, rltk::entity_t* pl);
}
  
void
on_init()
{
  cout << "From standard-food module" << endl;
}

Entity* get_apple()
{
  Entity* e = new Entity("Apple");
  e->setDescription("A delicious apple");
  e->setGlyphId("apple");
  e->setForegroundColor(255, 223, 0);
  e->setBackgroundColor(20, 20, 20);
  e->setWeight(0.3);
  e->add("eat", [e](rltk::entity_t* pl, Map*)
    {
      pl->component<Inventory>()->consume(e);
      auto att = pl->component<Attributes>();
      att->intInc("health", 3);
    });
  return e;
}

Entity*
get_chicken()
{
  Entity* e = new Entity("Chicken");
  e->setDescription("A delicious chicken meat");
  e->setGlyphId("chicken");
  e->setForegroundColor(255, 155, 51);
  e->setBackgroundColor(80, 80, 80);
  e->setWeight(0.3);
  e->add("eat", [e](rltk::entity_t* pl, Map*)
    {
      pl->component<Inventory>()->consume(e);
      auto att = pl->component<Attributes>();
      att->intInc("health", 5);
    });
  e->add("throw", [e](rltk::entity_t* pl, Map*)
    {
      pl->component<Inventory>()->consume(e);
      cout << "You just loose your leg" << endl;
    });
  return e;
}


void
on_generate(Map* m)
{
  for (int i = 0; i < rng::rdice(1, 3); ++i)
    {
      auto e = get_chicken();
      int x, y;
      rng::get_random_floor(m, &x, &y);
      e->move(x, y);
      m->add(e);
    }

  for (int i = 0; i < rng::rdice(1, 3); ++i)
    {
      auto e = get_apple();
      int x, y;
      rng::get_random_floor(m, &x, &y);
      e->move(x, y);
      m->add(e);
    }

}


void on_frame(double duration)
{
  
}

void on_move(Map* m, rltk::entity_t* pl)
{
  
}
