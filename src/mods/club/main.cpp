#include <iostream>

using namespace std;

#include "api/Rng.hpp"
#include "api/Map.hpp"
#include "api/Loot.hpp"
#include "api/Entity.hpp"
#include "api/Weapon.hpp"
#include "api/Attributes.hpp"

#include "components/Inventory.hpp"
#include "systems/Player.hpp"

#include <list>
#include <cmath>

using namespace std;

extern "C"
{
  void on_init();
  void on_generate(Map*);
  void on_move(Map* m, rltk::entity_t* pl);
}

int catnb = 1;

list<Entity*> es;
Weapon w1("jaw");
Weapon w2("claws");

void
on_init()
{
  cout << "From 01-so module" << endl;
}

Weapon* get_club()
{
  Weapon* e = new Weapon("Club");
  e->setDescription("A good basic melee weapon.");
  e->setGlyphId("club");
  e->setForegroundColor(40, 250, 0);
  e->setBackgroundColor(60, 30, 30);
  e->setWeight(2.4);
  e->setWalkable(true);
  e->setDeathBy(e->getName(), 0);

  e->setInjuryName("contusion");
  e->setDamageString("1d6 + 5%x2");
  e->setOnDamage([]() -> int
   {
     if (rng::rdice(1, 20) == 20) // critical attack
       return 12;
       
      return rng::rdice(1, 6);
   });
  
  /*
  auto att = e->getAttributes();
  att->setInt("health", 40);
  att->setInt("health-max", 40);
  */
  
  return e;
}

void
on_generate(Map* m)
{
  if (rng::rdice(1, 5) == 1) // 20%
    {
      Weapon* w = get_club();
      int x, y;
      rng::get_random_floor(m, &x, &y);
      w->move(x, y);
      m->add(w);
    }
}


void on_frame(double duration)
{
  
}

void
move_randomly(Entity* e)
{
  // Move randomly !
  int x = e->getX();
  int y = e->getX();
  int d = rng::rdice(1, 5);
  switch (d)
    {
    case 1:
      --x;
      break;
    case 2:
      ++x;
      break;
    case 3:
      --y;
      break;
    case 4:
      ++y;
      break;
    }
  e->move(x, y);
}

void on_move(Map* m, rltk::entity_t* pl)
{
  
  for (auto e : es)
    {

      auto pos = pl->component<Position>();
      
      // https://fr.wikipedia.org/wiki/Distance_entre_deux_points_sur_le_plan_cart%C3%A9sien
      double d = sqrt(pow( pos->getX() - e->getX(), 2) +
		      pow( pos->getY() - e->getY(), 2));

      if (!e->isEnemy())
	{
	  move_randomly(e);
	}
      else
	{
	  if (d > 2)
	    move_randomly(e);
	}
    }
}
