#include <iostream>

using namespace std;

#include "api/Rng.hpp"
#include "api/Map.hpp"
#include "api/Entity.hpp"
#include "api/Attributes.hpp"

#include "components/Inventory.hpp"
#include "systems/Player.hpp"

#include <list>

#include "namegen.h"

using namespace std;

extern "C"
{
  void on_init();
  void on_generate(Map*);
  void on_move(Map* m, rltk::entity_t* pl);
}

int catnb = 1;


list<Entity*> es;

void
on_init()
{

}

Entity* get_cat(const string& name)
{
  Entity* e = new Entity(name);
  e->setDescription("A little tiger");
  e->setGlyphId("cat");
  e->setForegroundColor(255, 165, 0);
  e->setBackgroundColor(60, 60, 0);
  e->setWeight(0.3);
  e->setWalkable(true);
  e->setDeathBy(e->getName(), 0);
  return e;
}

void
on_generate(Map* m)
{
  string name = "Felix";
  
  // Get a random name
  auto namedice = rng::rdice(1, 100);
  if (namedice > 2)
    {
      NameGen::Generator tgen("ss(ly|ily|ish|ing)");
      name = tgen.toString();
      name[0] = toupper(name[0]);
      LOG(LogLevel::Info, "Cat has a random fantasy name : '%s'", name.c_str());
    }
  //  Entity* e = new Entity("Papuche (cat)");
  Entity* e = get_cat(name);
  e->setSpecies("cat", catnb);
  
  int x, y;
  rng::get_random_floor(m, &x, &y);
  e->move(x, y);

  e->add("release", [e](rltk::entity_t* pl, Map* ma)
    {
      pl->component<Inventory>()->consume(e);
      auto pos = pl->component<Position>();
      e->move(pos->getX(), pos->getY());
      // Should get current map
      ma->add(e);
    });

  m->add(e);
  es.push_back(e);
}


void on_frame(double duration)
{
  
}

void on_move(Map* m, rltk::entity_t* pl)
{
  for (auto e : es)
    {
      // Stay calm if in player's inventory
      auto ent = pl->component<Inventory>()->getEntities();
      if (ent.find(e) == ent.end())
	{      
	  // Move randomly !
	  int x = e->getX();
	  int y = e->getX();
	  int d = rng::rdice(1, 5);
	  switch (d)
	    {
	    case 1:
	      --x;
	      break;
	    case 2:
	      ++x;
	      break;
	    case 3:
	      --y;
	      break;
	    case 4:
	      ++y;
	      break;
	    }
	  e->move(x, y);
	}
    }
}
