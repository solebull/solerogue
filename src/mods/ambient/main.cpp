#include "rltk.hpp"

#include "api/Rng.hpp"
#include "api/Map.hpp"
#include "api/Logger.hpp"
#include "api/VcharBuffer.hpp"
#include "api/File.hpp"

#include "messages/PlaySfx.hpp"

#include <iostream>     // std::cout
#include <vector>
#include <algorithm>    // USES std::max
#include <random>

extern "C"
{
  void on_init();
  void on_generate(Map*);
  void on_move(Map* m, rltk::entity_t* pl){};
  void on_frame(double, VcharBuffer*);
}

// Mod vars
const string modname = "mod/ambient";
std::vector<std::string> files;

void
on_init()
{
  files = File::fglob("../media/ambient/*");

  // debug
  LPUSH(modname);
  for (auto& f : files)
    LOG(LogLevel::Debug, "Ambient file found : %s", f.c_str());
  LPOP(modname);
  
}

void
on_generate(Map* m)
{

}

/** Change saturation value of the given tile's foreground color
  *
  * \param vb The needed vertex buffer object
  *
  */
void
ambient()
{
  // Get a random filename
  std::random_device random_device;
  std::mt19937 engine{random_device()};
  std::uniform_int_distribution<int> dist(0, files.size() - 1);
  string file = files[dist(engine)];

  // random pitch from 0.15/3
  constexpr float LO = 0.15f;
  constexpr float HI = 3.0f;
  float pitch = LO + static_cast <float> (rand())/
    (static_cast <float> (RAND_MAX/(HI-LO)));
  LPUSH(modname);
  for (auto& f : files)
    LOG(LogLevel::Debug, "Playing ambient sound : %s", f.c_str());
  LPOP(modname);

  emit(PlaySfxMessage(file,pitch, 80.0f));
}

/** Got a very little chance to fire this
  *
  */
bool
fire()
{
  if (rng::rdice(1, 20'500'000 ) <5)
    {
      LPUSH(modname);
      LOG(LogLevel::Info, "Firing ambient mod effect");
      ambient();
      LPOP(modname);
      return true;
    }

  return false;
}

/** Called after each frame, with a modifiable rendered map in VcharBuffer */
void on_frame(double duration, VcharBuffer* vb)
{
  if (!fire())
    return;
}
