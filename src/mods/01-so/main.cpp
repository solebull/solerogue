#include <iostream>

#include "api/Rng.hpp"
#include "api/Map.hpp"
#include "api/Loot.hpp"
#include "api/Logger.hpp"
#include "api/Entity.hpp"
#include "api/Weapon.hpp"
#include "api/Attributes.hpp"
#include "api/VcharBuffer.hpp"

#include "components/Inventory.hpp"
#include "systems/Player.hpp"

#include <list>
#include <cmath>

using namespace std;
extern "C"
{
  void on_init();
  void on_generate(Map*);
  void on_move(Map* m, rltk::entity_t* pl){};
  void on_frame(double, VcharBuffer*);
}

void
on_init()
{
}


void
on_generate(Map* m)
{

}

/** Called after each frame, with a modifiable rendered map in VcharBuffer */
void on_frame(double duration, VcharBuffer* vb)
{
  //  LOG(LogLevel::Info, "From 01-so and logger!");
}
