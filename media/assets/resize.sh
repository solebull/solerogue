#!/bin/bash

# terminal-32x32 is 512x512 = 16 images per row

echo "Please verify these lines are pesent in 'fonts.txt'"
for ((i=30; i>=2; i-=2))
do
    # Unix command here #
    # 30 x 16 =
    let w=$i*16
    convert terminal32x32.png -resize "$w"x"$w" terminal"$i"x"$i".png
    echo "$i"x"$i",terminal"$i"x"$i".png,"$i","$i"
done

